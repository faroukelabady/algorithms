package competitveProg3.chapter2.excersises;

import java.util.Arrays;

public class E2_2_1 {
	public static void main(String[] args) {
		int[] array = new int[100];
		Arrays.sort(array);
		for(int i = 1; i < array.length; i++) {
			if(array[i-1] ==array[i]) {
				// there is a duplicate
				System.out.println("duplicate found");
			}
		}

		// brute force solution with two for loops O(N^2)
		int v  = 10000;
		for(int i =0 ; i < array.length; i++) {
			for(int j =i ; j < array.length; j++) {
				if(array[i] + array[j] == v) {
					// match found
				}
			}
		}

		// another solution is to sort the array
		//

		//
		// get a and b
		// sort the array
		// binary search a and b and get the index
		// get the data between a index and b index

		// max variable
		// when there is a integer less than the pervious one restart the length
		// and check the max
		int max = 0;
		int length = 0;
		for(int i = 1; i < array.length; i++) {
			if(array[i] > array[i - 1]) {
				length++;
			} else {
				max = Math.max(max, length);
				length = 0;
			}
		}


	}
}
