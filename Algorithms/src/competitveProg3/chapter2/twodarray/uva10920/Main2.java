package competitveProg3.chapter2.twodarray.uva10920;

import java.io.InputStream;
import java.io.PrintWriter;
import java.util.Scanner;

public class Main2 {

	public static void main(String[] args) {
		DataReader in = new DataReader(System.in);
		int size, pos;
		do {
			size = in.nextInt();
			pos = in.nextInt();
			if (pos == 0 && size == 0) {
				break;
			}
			int baseCoord = (int) Math.ceil(size / 2.0);
			int baseSqrt = (int) Math.sqrt(pos);
			if (baseSqrt % 2 == 0) {
				baseSqrt -= 1;
			}
			int nextSqrt = baseSqrt + 2;
			int nextSq = (int) Math.pow(nextSqrt, 2);
			int baseSq = (int) Math.pow(baseSqrt, 2);
			baseCoord += (baseSqrt - 1) / 2;
			int maxCoord = baseCoord + 1;
			int minCoord = baseCoord - baseSqrt;
			int x, y, secSegMarker, thirdSegMarker, fourthSegMarker;
			int segLength = (nextSq - baseSq) / 4;
			secSegMarker = baseSq + segLength + 1;
			thirdSegMarker = baseSq + 2 * segLength + 1;
			fourthSegMarker = baseSq + 3 * segLength + 1;

			if (pos > baseSq && pos < secSegMarker) {
				x = maxCoord;
				y = (maxCoord - 1) - (pos - (baseSq + 1));
			} else if (pos >= secSegMarker && pos < thirdSegMarker) {
				y = minCoord;
				x = (maxCoord - 1) - (pos - secSegMarker);
			} else if (pos >= thirdSegMarker && pos < fourthSegMarker) {
				x = minCoord;
				y = minCoord + 1 + (pos - thirdSegMarker);
			} else if (pos >= fourthSegMarker && pos <= nextSq) {
				y = maxCoord;
				x = minCoord + 1 + (pos - fourthSegMarker);
			} else {
				x = baseCoord;
				y = baseCoord;
			}
			System.out.printf("Line = %d, column = %d.\n", x, y);
		} while (true);

	}

	static class DataReader {
		private InputStream in = null;
		private int pos, count;
		private byte[] buf = new byte[64 * 1024];

		public DataReader(InputStream in) {
			this.in = in;
			pos = 0;
			count = 0;
		}

		public int nextInt() {
			int c, sign = 1;
			while (Character.isWhitespace(c = read()))
				;
			if (c == '-') {
				sign = -1;
				c = read();
			}
			int n = c - '0';
			while ((c = read() - '0') >= 0)
				n = n * 10 + c;
			return n * sign;
		}

		public String next() {
			StringBuilder builder = new StringBuilder();
			int c;
			while (!Character.isSpaceChar(c = read()))
				builder.append(c);
			return builder.toString();
		}

		public String nextLine() {
			StringBuilder builder = new StringBuilder();
			int c;
			while ((c = read()) != '\n')
				builder.append(c);
			return builder.toString();
		}

		public int read() {
			if (pos == count)
				fillBuffer();
			return buf[pos++];
		}

		private void fillBuffer() {
			try {
				count = in.read(buf, pos = 0, buf.length);
				if (count == -1)
					buf[0] = -1;
			} catch (Exception ignore) {
			}
		}
	}

}
