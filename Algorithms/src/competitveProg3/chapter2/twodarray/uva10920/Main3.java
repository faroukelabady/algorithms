package competitveProg3.chapter2.twodarray.uva10920;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main3 {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader br = new BufferedReader(new InputStreamReader(System.in));
		String line;
		StringTokenizer st;
		while ((line = br.readLine()) != null) {
			st = new StringTokenizer(line);
			int S = Integer.parseInt(st.nextToken());
			long P = Long.parseLong(st.nextToken());
			if (S == 0 && P == 0) {
				break;
			}
			int ind = 1, cnt = 0, row = 0, col = 0;
			while (ind < P) {
				cnt++;
				if (ind + cnt >= P) {
					row += P - ind;
					break;
				}
				ind += cnt;
				row += cnt;
				if (ind + cnt >= P) {
					col -= P - ind;
					break;
				}
				ind += cnt;
				col -= cnt;
				cnt++;
				if (ind + cnt >= P) {
					row -= P - ind;
					break;
				}
				ind += cnt;
				row -= cnt;
				if (ind + cnt >= P) {
					col += P - ind;
					break;
				}
				ind += cnt;
				col += cnt;
			}
			S >>= 1;
			S++;
			System.out.println("Line = " + (S + row) + ", column = " + (S + col) + ".");

		}
	}

}
