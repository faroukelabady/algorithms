package competitveProg3.chapter2.twodarray.uva10920;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int sz = 0;
		long p = 0;
		while (true) {
			sz = sc.nextInt();
			p = sc.nextLong();
			if (sz == 0 & p == 0) {
				break;
			}
			sc.nextLine();
			long count = 1;
			int startPos = (sz / 2);
			int x = startPos, y = startPos;
			long step = 1;
			boolean xspirFlag = true;
			boolean yspirFlag = true;
			boolean xYFlag = true;
			int stepcount = 0;
			outer:
			while((x < sz || y < sz) && x >= 0 && y >= 0) {
				if(count == p) {
					break outer;
				}
				if(xYFlag) {
					for(int i = 1; i <= step; i++) {
						if(xspirFlag) {
							x--;
						} else {
							x++;
						}
						if(x >= sz || x < 0 || y >= sz || y < 0) {
							break;
						}
						count++;
						if(count == p) {
							break outer;
						}
					}
					xspirFlag = !xspirFlag;
					stepcount++;
				} else {
					for(int i = 1; i <= step; i++) {
						if(yspirFlag) {
							y--;
						} else {
							y++;
						}
						if(y >= sz || y < 0 || x >= sz || x < 0) {
							break;
						}
						count++;
						if(count == p) {
							break outer;
						}

					}
					yspirFlag = !yspirFlag;
					stepcount++;
				}
				xYFlag = !xYFlag;


				if(stepcount == 2) {
					step++;
					stepcount = 0;
				}
			}
			x = sz - x;
			y = y + 1;
			System.out.println("Line = "+ x +", column = " + y + ".");


		}
	}

}
