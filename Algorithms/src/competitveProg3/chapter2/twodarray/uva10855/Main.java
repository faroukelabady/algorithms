package competitveProg3.chapter2.twodarray.uva10855;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int N = 0;
		int n = 0;
		char[][] big = null;
		char[][] small = null;
		while (true) {
			N = sc.nextInt();
			n = sc.nextInt();
			if (N == 0 & n == 0) {
				break;
			}
			sc.nextLine();
			big = new char[N][N];
			small = new char[n][n];
			for (int i = 0; i < N; i++) {
				big[i] = sc.nextLine().toCharArray();
			}

			for (int i = 0; i < n; i++) {
				small[i] = sc.nextLine().toCharArray();
			}
			int count = findMatch(big, small);
			System.out.print(count + " ");
			small = rotate90(small);
			count = findMatch(big, small);
			System.out.print(count + " ");
			small = rotate90(small);
			count = findMatch(big, small);
			System.out.print (count + " ");
			small = rotate90(small);
			count = findMatch(big, small);
			System.out.println(count);
		}

	}

	private static int findMatch(char[][] big, char[][] small) {
		int counter = 0;
		int N = big.length;
		int n = small.length;
		boolean match = true;
		int hStep = 0;
		int vStep = 0;
		while (true) {
			outer:
			for (int i = 0; i < n; i++) {
				for (int j = 0; j < n; j++) {
					if (big[i + hStep][j + vStep] != small[i][j]) {
						match = false;
						break outer;
					}
				}
			}

			if (match)
				counter++;

			if (hStep + n >= N && vStep + n >= N) {
				break;
			}
			if (vStep + n >= N) {
				vStep = 0;
				hStep++;
			} else {
				vStep++;
			}
			match = true;
		}

		return counter;
	}

	private static char[][] rotate90(char[][] small) {
		int n = small.length;
		char[][] rotatedSmall = new char[n][n];
		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				rotatedSmall[0 + j][n - 1 - i] = small[i][j];
			}
		}

		return rotatedSmall;

	}

}
