package competitveProg3.chapter2.onedimensionarray.uva11340;

import java.io.IOException;
import java.math.BigDecimal;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		String s = Main.readLine(500);
		int testCases = new Integer(s);
		while (testCases > 0) {
			int paidChar = new Integer(Main.readLine(500));
			double totalPrice = 0.0;
			Map<Character, Double> value = new HashMap<>();
			for (int i = 0; i < paidChar; i++) {
				String[] data = Main.readLine(500).split(" ");
				value.put(new Character(data[0].toCharArray()[0]), Double.parseDouble(data[1]) / 100);
			}
			int lines = new Integer(Main.readLine(500));
			for (int i = 0; i < lines; i++) {
				totalPrice += Main.readLine(10000, value);
			}
			BigDecimal result = new BigDecimal(totalPrice);
			result = result.setScale(2, BigDecimal.ROUND_HALF_EVEN);

			//  DecimalFormat myFormatter = new DecimalFormat(pattern);
			// pattern = ###.##
			System.out.println("" + result+ "$");
			testCases--;
		}

	}

	public static String readLine(int maxLen) {
		byte[] b = new byte[maxLen];
		int lg =0, c=0;
		while(lg < maxLen) {
			try {
				c = System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(c < 0 || c == '\n')
				break;
			b[lg++] += c;
		}
		if(c < 0 && lg == 0)
			return null;
		return new String(b, 0 , lg);

	}

	public static double readLine(int maxLen, Map<Character, Double> value) {
		byte[] b = new byte[maxLen];
		int lg =0, c=0;
		double result = 0.0;
		while(lg < maxLen) {
			try {
				c = System.in.read();
				if (value.containsKey((char)c)) {
					result += value.get((char)c);
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(c < 0 || c == '\n')
				break;
			b[lg++] += c;
		}

		return result;

	}

}
