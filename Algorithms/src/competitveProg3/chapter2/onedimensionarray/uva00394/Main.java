package competitveProg3.chapter2.onedimensionarray.uva00394;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	static class Array {
		String name;
		int baseAddr;
		int byteSize;
		int dimension;
		int bounds[][];
		int c[];

		void claculateC() {
			c = new int[dimension + 1];
			c[dimension] = byteSize;
			for(int i = c.length - 2; i > 0; i--) {
				c[i] = c[i+1] * (bounds[i + 1][1] - bounds[i + 1][0] + 1);
			}
			c[0] = baseAddr;
			for(int i = 1; i < c.length; i++) {
				c[0] -= c[i] * bounds[i][0];
			}
		}

	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int arrayNumbers = sc.nextInt();
		int data = sc.nextInt();
		ArrayList<Array> arrays = new ArrayList<>();
		sc.nextLine();
		for (int i = 1; i <= arrayNumbers; i++) {
			String line = sc.nextLine();
			StringTokenizer token = new StringTokenizer(line);
			Array a = new Array();
			a.name = token.nextToken();
			a.baseAddr = new Integer(token.nextToken());
			a.byteSize = new Integer(token.nextToken());
			a.dimension = new Integer(token.nextToken());
			a.bounds = new int[a.dimension + 1][2];
			for (int j = 1; j <= a.dimension; j++) {
				a.bounds[j][0] = new Integer(token.nextToken());
				a.bounds[j][1] = new Integer(token.nextToken());
			}
			a.claculateC();
			arrays.add(a);
		}

		for (int i = 1; i <= data; i++) {
			String line = sc.nextLine();
			StringTokenizer token = new StringTokenizer(line);
			String arrayName = token.nextToken();
			for(Array a: arrays) {
				if(a.name.equals(arrayName)) {
					int sum = a.c[0];
					int size[] = new int[a.dimension];
					for(int j =1 ; j < a.c.length; j++) {
						size[j - 1] = new Integer(token.nextToken());
						sum += a.c[j] *  size[j - 1];
					}
					System.out.println(arrayName + Arrays.toString(size) + " = " + sum);
				}
			}
		}
		sc.close();

	}

}
