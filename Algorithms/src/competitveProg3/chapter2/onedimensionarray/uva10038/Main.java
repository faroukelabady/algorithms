package competitveProg3.chapter2.onedimensionarray.uva10038;

import java.util.Scanner;
import java.util.regex.Pattern;

public class Main {

	static Pattern matcher = Pattern.compile(" ");
	static int max = Integer.MIN_VALUE;

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (sc.hasNextLine()) {
			String data = sc.nextLine();
			if (data == null || data.isEmpty())
				break;
			boolean[] flags = new boolean[30005];
			int[] input = matcher.splitAsStream(data).mapToInt(Integer::parseInt).toArray();
			int n = input[0];
			boolean jolly = true;
			for(int i = 1; i < n; i++) {
				int current = input[i];
				int next = input[i+1];
				int abs = Math.abs(next - current);
				if(abs <= 0 || abs > n - 1) {
					jolly = false;
				} else if(flags[abs]) {
					jolly = false;
				}
				flags[abs] = true;
			}

			if(jolly) {
				System.out.println("Jolly");
			} else {
				System.out.println("Not jolly");
			}

		}
	}

}
