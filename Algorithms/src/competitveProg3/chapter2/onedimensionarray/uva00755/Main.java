package competitveProg3.chapter2.onedimensionarray.uva00755;

import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

public class Main {

	static class Number {
		public static String convertNumber(String number) {
			String converted = "";
			char[] array = number.toCharArray();
			for(int i = 0 ; i < array.length; i++) {
				if(array[i] == 'A' || array[i] == 'B' || array[i] == 'C' ) {
					array[i]= '2';
				} else if(array[i] == 'A' || array[i] == 'B' || array[i] == 'C' ) {
					array[i]= '2';
				} else if(array[i] == 'D' || array[i] == 'E' || array[i] == 'F' ) {
					array[i]= '3';
				} else if(array[i] == 'G' || array[i] == 'H' || array[i] == 'I' ) {
					array[i]= '4';
				} else if(array[i] == 'J' || array[i] == 'K' || array[i] == 'L' ) {
					array[i]= '5';
				} else if(array[i] == 'M' || array[i] == 'N' || array[i] == 'O' ) {
					array[i]= '6';
				} else if(array[i] == 'P' || array[i] == 'R' || array[i] == 'S' ) {
					array[i]= '7';
				} else if(array[i] == 'T' || array[i] == 'U' || array[i] == 'V' ) {
					array[i]= '8';
				} else if(array[i] == 'W' || array[i] == 'X' || array[i] == 'Y' ) {
					array[i]= '9';
				}
			}
			converted = new String(array);
			converted = converted.replaceAll("[-]", "");
			return new StringBuilder(converted).insert(3, '-').toString();
		}
	}

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		sc.nextLine();
		while (testCases > 0) {
			sc.nextLine();
			SortedMap<String, Integer> map = new TreeMap<>();
			int dataset = sc.nextInt();
			sc.nextLine();
			for (int i = 1; i <= dataset; i++) {
				String number = sc.nextLine();
				String converted = Number.convertNumber(number).trim();
				if(map.containsKey(converted)) {
					map.put(converted, map.get(converted) + 1);
				} else {
					map.put(converted, 1);
				}
			}

			map.entrySet().removeIf( e -> e.getValue() == 1);

			if(map.isEmpty()) {
				System.out.println("No duplicates.");
			} else {
				map.forEach((k , v) -> System.out.println(k + " " + v));
			}
			if(testCases > 1) {
				System.out.println();
			}
			testCases--;
		}
		sc.close();

	}

}
