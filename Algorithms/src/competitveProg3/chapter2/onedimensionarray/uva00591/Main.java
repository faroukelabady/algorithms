package competitveProg3.chapter2.onedimensionarray.uva00591;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int p = 0;
		while(true) {
			int n = sc.nextInt();
			if(n == 0)
				break;

			p++;
			sc.nextLine();
			int total = 0;
			int[] h = new int[n];
			for(int i = 0; i < n; i++) {
				int val = sc.nextInt();
				total+= val;
				h[i] = val;
			}
			int med = total / n;
			int count = 0;
			for(int i : h ) {
				if(i > med) {
					count += i - med;
				}
			}
			System.out.println("Set #" + p);
			System.out.println("The minimum number of moves is " + count+ ".");
			System.out.println();

		}

	}

}
