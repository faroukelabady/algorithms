package competitveProg3.chapter2.onedimensionarray.uva00230;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Deque;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Scanner;
import java.util.Set;
import java.util.SortedSet;
import java.util.Stack;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.function.Consumer;
import java.util.function.Predicate;

public class Main {

	class BookData implements Comparable<BookData> {

		String bookName;
		String author;

		BookData(String bookName, String author) {
			this.bookName = bookName;
			this.author = author;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + ((author == null) ? 0 : author.hashCode());
			result = prime * result + ((bookName == null) ? 0 : bookName.hashCode());
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			BookData other = (BookData) obj;
			if (author == null) {
				if (other.author != null)
					return false;
			} else if (!author.equals(other.author))
				return false;
			if (bookName == null) {
				if (other.bookName != null)
					return false;
			} else if (!bookName.equals(other.bookName))
				return false;
			return true;
		}

		@Override
		public int compareTo(BookData o) {
			int result = this.author.compareTo(o.author);
			if (result == 0) {
				result = this.bookName.compareTo(o.bookName);
			}
			return result;
		}

	}

	public static void main(String[] args) {
		Main m = new Main();
		Scanner sc = new Scanner(System.in);
		List<BookData> allBooks = new ArrayList<>();
		SortedSet<BookData> remainingBooks = new TreeSet<>();

		while(true) {
			String book = sc.nextLine();
			if(book.equals("END")) {
				break;
			}
			String title1 = book.substring(book.indexOf('"'), book.lastIndexOf('"') + 1);
			String author1 = book.substring(book.indexOf("by "));
			BookData data = m.new BookData(title1, author1);
			allBooks.add(data);
			remainingBooks.add(data);
		}

		List<String> borrowedBooks = new ArrayList<>();
		Deque<String> returnedBooks = new ArrayDeque<>();
		while(true) {
			String state = sc.nextLine();
			if(state.equals("END")) {
				break;
			}
			String[] data = state.split(" ", 2);
			if(data[0].equals("BORROW")) {
				borrowedBooks.add(data[1]);
				remainingBooks.removeIf(new Predicate<BookData>() {
					@Override
					public boolean test(BookData t) {
						return t.bookName.equals(data[1]);
					}
				});
			} else if(data[0].equals("RETURN")) {
				returnedBooks.add(data[1]);
				borrowedBooks.removeIf(new Predicate<String>() {
					@Override
					public boolean test(String t) {

						return data[1].equals(t);
					}
				});
			} else if(data[0].equals("SHELVE")) {
				Iterator<String> it = returnedBooks.iterator();
				SortedSet<BookData> returnedBooksSet = new TreeSet<>();
				while(it.hasNext()) {
					String bookName = it.next();
					BookData original = null;
					for(BookData book: allBooks) {
						if(book.bookName.equals(bookName)){
							returnedBooksSet.add(book);
							break;
						}
					}
				}
				Iterator<BookData> origin = returnedBooksSet.iterator();
				while(origin.hasNext()) {
					BookData original = origin.next();
					if(remainingBooks.isEmpty()) {
						System.out.println("Put " + original.bookName + " first");
					} else {
						BookData previous = null;
						boolean put = false;
						for(BookData book: remainingBooks) {
							int result  = original.compareTo(book);
							if(result < 1) {
								if(previous == null) {
									System.out.println("Put " + original.bookName + " first");
									put = true;
									previous = book;
									break;
								} else {
									System.out.println("Put " + original.bookName + " after " + previous.bookName);
									put = true;
									break;
								}

							}
							previous = book;
						}
						if(!put) {
							System.out.println("Put " + original.bookName + " after " + previous.bookName);
						}
					}
					returnedBooks.remove(original.bookName);
					remainingBooks.add(original);
				}
				System.out.println("END");
			}

		}

	}


}
