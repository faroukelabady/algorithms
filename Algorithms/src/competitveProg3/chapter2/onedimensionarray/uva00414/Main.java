package competitveProg3.chapter2.onedimensionarray.uva00414;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(true) {
			int n = sc.nextInt();
			if(n == 0)
				break;
			sc.nextLine();
			int minSpace = Integer.MAX_VALUE;
			int[] maxSpace = new int[n];
			for(int i = 1 ; i <= n ; i++) {
				String line = sc.nextLine();
				int start = line.indexOf(" ");
				int counter = 0;
				if(start != -1) {
					for(int j = start ; j < line.length(); j++ )
						if(line.charAt(j) == ' ')
							counter++;

				}
				minSpace = Math.min(minSpace, counter);
				maxSpace[i - 1] = counter;
			}
			int diff = 0;
			for(int space : maxSpace) {
				diff += space - minSpace;
			}
			System.out.println(diff);
		}
	}

}
