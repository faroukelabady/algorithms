package competitveProg3.chapter2.onedimensionarray.uva00665;

import java.util.HashSet;
import java.util.Iterator;
import java.util.Scanner;
import java.util.Set;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		sc.nextLine();
		while (testCases > 0) {
			sc.nextLine();
			int coinNums = sc.nextInt();
			int trials = sc.nextInt();
			sc.nextLine();
			boolean[] greaterThan = new boolean[coinNums + 1];
			boolean[] lessThan = new boolean[coinNums + 1];
			for (int i = 1; i <= coinNums; i++) {
				greaterThan[i] = true;
				lessThan[i] = true;
			}
			for (int i = 1; i < trials * 2; i += 2) {
				int weightedCoinNum = sc.nextInt();
				boolean leftSide[] = new boolean[coinNums + 1];
				boolean rightSide[] = new boolean[coinNums + 1];
				char compare = '=';
				for (int j = 0; j < weightedCoinNum; j++) {
					leftSide[sc.nextInt()] = true ;
				}
				for (int j = 0; j < weightedCoinNum; j++) {
					rightSide[sc.nextInt()] = true;
				}
				sc.nextLine();
				compare = sc.next().charAt(0);
				if (compare == '<') {
					for (int j = 1 ; j <= coinNums; j++) {
						greaterThan[j] = greaterThan[j] & rightSide[j];
					}

					for (int j = 1 ; j <= coinNums; j++) {
						lessThan[j] = lessThan[j] & leftSide[j];
					}

				} else if (compare == '>') {
					for (int j = 1 ; j <= coinNums; j++) {
						greaterThan[j] = greaterThan[j] & leftSide[j];
					}

					for (int j = 1 ; j <= coinNums; j++) {
						lessThan[j] = lessThan[j] & rightSide[j];
					}

				} else if (compare == '=') {
					for (int j = 1 ; j <= coinNums; j++) {
						greaterThan[j] = greaterThan[j] & !leftSide[j];
						greaterThan[j] = greaterThan[j] & !rightSide[j];
						lessThan[j] = lessThan[j] & !leftSide[j];
						lessThan[j] = lessThan[j] & !rightSide[j];
					}

				}
			}
			int rightIndex = 0;
			int leftIndex = 0;
			int rightcount = 0;
			int leftcount = 0;
			for (int j = 1 ; j <= coinNums; j++) {
				if(greaterThan[j]) {
					rightIndex = j;
					rightcount++;
				}
				if(lessThan[j]) {
					leftIndex = j;
					leftcount++;
				}
			}

			if (rightcount == 1 && leftcount == 0) {
				System.out.println(rightIndex);
			} else if(rightcount == 0 && leftcount == 1) {
				System.out.println(leftIndex);
			} else if(rightcount == 1 && leftcount == 1 && rightIndex == leftIndex) {
				System.out.println(leftIndex);
			}else {
				System.out.println("0");
			}
			if(testCases > 1) {
				System.out.println();
			}

			testCases--;
		}

	}

}
