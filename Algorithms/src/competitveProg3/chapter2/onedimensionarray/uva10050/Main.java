package competitveProg3.chapter2.onedimensionarray.uva10050;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		sc.nextLine();
		while(testCases > 0) {
			int days = sc.nextInt();
			sc.nextLine();
			int parties = sc.nextInt();
			sc.nextLine();
			int[] pi = new int[parties];
			for(int i = 0; i < parties; i++) {
				pi[i] = sc.nextInt();
				sc.nextLine();
			}

			boolean[] vac = new boolean[3655];
			for(int i = 0; i < parties; i++) {
				int counter = 1;
				while(true) {
					int n = pi[i] * counter;
					if(n > days) {
						break;
					}
					if( n % 7 == 0 || (n - 6) % 7 == 0) {
						vac[n] = false;
					} else {
						vac[n] = true;
					}
					counter++;
				}
			}
			int total = 0;
			for(boolean v: vac) {
				if(v)
					total++;
			}
			System.out.println(total);
			testCases--;
		}

	}

}
