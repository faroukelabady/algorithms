package competitveProg3.chapter2.onedimensionarray.uva00482;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		sc.nextLine();
		while (testCases > 0) {
			sc.nextLine();
			String[] indecies = sc.nextLine().split(" ");
			String[] data = sc.nextLine().split(" ");
			String[] array = new String[indecies.length];

			for(int i =0; i < indecies.length; i++) {
				int val = new Integer(indecies[i]);
				array[val - 1]= data[i];
			}

			for(String a : array) {
				System.out.println(a);
			}
			if(testCases > 1) {
				System.out.println();
			}
			testCases--;
		}
		sc.close();
	}

}
