package competitveProg3.chapter1.veryeasy.uva12250;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input;
		StringTokenizer tokenizer;
		int i = 1;
		while (!(input = Main.ReadLn(255)).equals("#")) {
			if (input.equals("HELLO")) {
				System.out.println("Case " + i + ": ENGLISH");
			} else if (input.equals("HOLA")) {
				System.out.println("Case " + i + ": SPANISH");
			} else if (input.equals("HALLO")) {
				System.out.println("Case " + i + ": GERMAN");
			} else if (input.equals("BONJOUR")) {
				System.out.println("Case " + i + ": FRENCH");
			} else if (input.equals("CIAO")) {
				System.out.println("Case " + i + ": ITALIAN");
			} else if (input.equals("ZDRAVSTVUJTE")) {
				System.out.println("Case " + i + ": RUSSIAN");
			} else {
				System.out.println("Case " + i + ": UNKNOWN");
			}
			i++;
		}
	}
}
