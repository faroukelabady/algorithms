package competitveProg3.chapter1.veryeasy.uva12403;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {


	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = Main.ReadLn(255);
		StringTokenizer tokenizer;
		int testCases;
		String operation; 
		long num = 0;
		long result = 0;
		tokenizer = new StringTokenizer(input);
		testCases = Integer.parseInt(tokenizer.nextToken());
		for (int i = 1; i <= testCases; i++) {
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			operation = tokenizer.nextToken();
			if(operation.equals("donate")) {
				num = Long.parseLong(tokenizer.nextToken());
				result += num;
			} else {
				System.out.println(result);
			}
		}
	}
}
