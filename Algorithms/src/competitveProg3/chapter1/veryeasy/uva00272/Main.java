package competitveProg3.chapter1.veryeasy.uva00272;

import java.io.*;
import java.util.*;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input;
		boolean flipConvert = true;
		String output = "";
		while ((input = Main.ReadLn(255)) != null) {
			while (input.indexOf('"') > -1) {
				if (flipConvert) {
					input = input.replaceFirst("\"", "``");
					flipConvert = false;
				} else {
					input = input.replaceFirst("\"", "''");
					flipConvert = true;
				}
			}
			System.out.println(input);
		}
	}
}
