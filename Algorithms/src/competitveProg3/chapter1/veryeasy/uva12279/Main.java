package competitveProg3.chapter1.veryeasy.uva12279;

import java.io.IOException;
import java.util.StringTokenizer;


public class Main {
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = "";
		StringTokenizer tokenizer;
		int testCases;
		int i = 1;
		while (!(input = Main.ReadLn(3000)).equals("0")) {
			tokenizer = new StringTokenizer(input);
			testCases = Integer.parseInt(tokenizer.nextToken());
			input = Main.ReadLn(3000);
			tokenizer = new StringTokenizer(input);
			int treat = 0 , noTreat = 0;
			for(;testCases > 0; testCases--) {
				int test = Integer.parseInt(tokenizer.nextToken());
				if(test > 0)
					treat++;
				else
					noTreat++;
			}
			int result = treat - noTreat;
			System.out.println("Case " + i + ": " + result);
			i++;
		}
	}
}
