package competitveProg3.chapter1.veryeasy.uva11172;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = Main.ReadLn(255);
		StringTokenizer tokenizer;
		int testCases, a, b;
		tokenizer = new StringTokenizer(input);
		testCases = Integer.parseInt(tokenizer.nextToken());
		for (; testCases > 0; testCases--) {
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			a = Integer.parseInt(tokenizer.nextToken());
			b = Integer.parseInt(tokenizer.nextToken());
			System.out.println(a>b?">":a<b?"<":"=");
		}
	}
}