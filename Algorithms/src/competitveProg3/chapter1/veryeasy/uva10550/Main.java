package competitveProg3.chapter1.veryeasy.uva10550;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input;
		StringTokenizer tokenizer;
		int start, firstNum, secondNum, thirdNum;
		// we calculat after the first 2 turns
		while ((input = Main.ReadLn(255)) != null) {
			int result = 1080;
			tokenizer = new StringTokenizer(input);
			start = Integer.parseInt(tokenizer.nextToken());
			firstNum = Integer.parseInt(tokenizer.nextToken());
			secondNum = Integer.parseInt(tokenizer.nextToken());
			thirdNum = Integer.parseInt(tokenizer.nextToken());
			if (start == 0 && firstNum == 0 && secondNum == 0 && thirdNum == 0) {
				break;
			}
			
			int temp = firstNum - start;
			int res1 = temp > 0 ? (40 - temp): (-temp);
			temp = firstNum - secondNum;
			int res2 = temp > 0? (40 - (temp)) : -temp ;
			temp = thirdNum - secondNum;
			int res3 = temp > 0? (40 - (temp)) : -temp ;
			result += 9 * res1 + 9 * res2 + 9 * res3;
			System.out.println(result);

		}
	}
}
