package competitveProg3.chapter1.veryeasy.uva11498;

import java.io.IOException;
import java.util.StringTokenizer;
import java.util.TreeSet;

public class Main {
	
	
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input;
		StringTokenizer tokenizer;
		int testCases, divX , divY , x, y;
		while(!(input = Main.ReadLn(255)).equals("0")) {
			tokenizer = new StringTokenizer(input);
			testCases = Integer.parseInt(tokenizer.nextToken()) ;
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			divX = Integer.parseInt(tokenizer.nextToken());
			divY = Integer.parseInt(tokenizer.nextToken());
			for (; testCases > 0; testCases--) {
				input = Main.ReadLn(255);
				tokenizer = new StringTokenizer(input);
				x = Integer.parseInt(tokenizer.nextToken());
				y = Integer.parseInt(tokenizer.nextToken());
				StringBuilder strBuild = new StringBuilder();
				if(divX == x || divY == y) {
					strBuild.append("divisa");
				} else {
					strBuild.append(y > divY?"N":"S");
					strBuild.append(x > divX?"E":"O");
				}
				System.out.println(strBuild);
			}
		}
	}
}
