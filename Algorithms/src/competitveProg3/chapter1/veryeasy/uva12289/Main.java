package competitveProg3.chapter1.veryeasy.uva12289;

import java.io.IOException;
import java.util.StringTokenizer;


public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = Main.ReadLn(255);
		StringTokenizer tokenizer;
		int testCases;
		tokenizer = new StringTokenizer(input);
		testCases = Integer.parseInt(tokenizer.nextToken());
		String one = "one", two = "two", three = "three";
		for (; testCases > 0; testCases--) {
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			if(compare(input.toCharArray(), one.toCharArray()) <= 1) {
				System.out.println(1);
			} else if (compare(input.toCharArray(), two.toCharArray()) <= 1) {
				System.out.println(2);
			} else if (compare(input.toCharArray(), three.toCharArray()) <= 1) {
				System.out.println(3);
			}
			
		}
	}
	
	int compare(char[] in, char[] origin) {
		int diff = 0;
		int leastLength = in.length < origin.length?in.length :origin.length;
		for(int i = 0; i < leastLength; i++) {
			if(in[i] != origin[i])
				diff++;
		}
		return diff;
	}
}
