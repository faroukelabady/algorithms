package competitveProg3.chapter1.medium.uva00661;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
	
	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		boolean first = true;
		int count = 1;
		while(true) {
			StringTokenizer line = new StringTokenizer(in.readLine());
			int n = Integer.parseInt(line.nextToken());
			int m = Integer.parseInt(line.nextToken());
			int c = Integer.parseInt(line.nextToken());
			
			if(n == 0) 
				break;
			
			int[][] devices = new int[n][2];
			for(int i = 0 ; i < n;i++) {
				line = new StringTokenizer(in.readLine());
				devices[i][0] = Integer.parseInt(line.nextToken());
			}
			int sum = 0;
			int max = 0;
			boolean flag = true;
			for(int i =0; i < m; i++) {
				line = new StringTokenizer(in.readLine());
				int u = Integer.parseInt(line.nextToken()) - 1;
				if(devices[u][1] == 0 ) {
					devices[u][1] = 1;
					sum += devices[u][0]; 
				} else {
					devices[u][1] = 0;
					sum -= devices[u][0];
				}
				max = Math.max(sum, max);
				if(sum > c && flag == true) {
					System.out.println("Sequence " + count);
					System.out.println("Fuse was blown.");
					System.out.println();
					flag = false;
				}
			}
			
			if(max <= c) {
				System.out.println("Sequence " + count);
				System.out.println("Fuse was not blown.");
				System.out.printf("Maximal power consumption was %d amperes.\n", max);
				System.out.println();
			}
			count++;
		}
	}
}
