package competitveProg3.chapter1.medium.uva10919;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException {
		Main main = new Main();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			String input = in.readLine();
			if(input.equals("0"))
				break;
			StringTokenizer token = new StringTokenizer(input);
			int k = Integer.parseInt(token.nextToken());
			int m = Integer.parseInt(token.nextToken());
			String[] str = in.readLine().split(" ");
			String right = "yes";
			while(m > 0) {
				input = in.readLine();
				token = new StringTokenizer(input);
				int cNum = Integer.parseInt(token.nextToken());
				int cMin = Integer.parseInt(token.nextToken());
				List<String> t = new ArrayList<>();
				for(int i = 1;i <= cNum;i++) {
					t.add(token.nextToken());
				}
				int count = 0;
				for(String u: str) {
					if(t.contains(u))
						count++;
				}
				if(count < cMin)  {
					right = "no";
				}
				m--;
			}
			System.out.println(right);
		}
	}
}
