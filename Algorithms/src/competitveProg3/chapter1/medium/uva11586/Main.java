package competitveProg3.chapter1.medium.uva11586;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	static boolean ReadLn(String str) // utility function to read from stdin
	{
		int lg = 0;
		String line = "";
		int m = 0;
		int f = 0;
		for (char car : str.toCharArray()) {
			if ((car < 0) || (car == '\n'))
				break;
			if ((car == 'M' || car == 'm')) {
				m++;
			} else if (car == 'F' || car == 'f') {
				f++;
			}
		}
		return m == f && (m+f) > 2;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		StringTokenizer token = new StringTokenizer(in.readLine());
		int testCases = Integer.parseInt(token.nextToken());
		while (testCases > 0) {
			if (ReadLn(in.readLine())) {
				System.out.println("LOOP");
			} else {
				System.out.println("NO LOOP");
			}
			testCases--;
		}
	}

}
