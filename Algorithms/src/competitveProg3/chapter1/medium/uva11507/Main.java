package competitveProg3.chapter1.medium.uva11507;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String input = in.readLine();
			if (input.equals("0"))
				break;
			
			StringTokenizer token = new StringTokenizer(input);
			int l = Integer.parseInt(token.nextToken());
			String current = "+x";
			String[] str = in.readLine().split(" ");
			for(String v:str) {
				if(v.equals("No")) {
					continue;
				} else if(v.equals("+y")) {
					if(current.equals("+x"))
						current = "+y";
					else if(current.equals("-x"))
						current = "-y";
					else if(current.equals("-y"))
						current = "+x";
					else if(current.equals("+y"))
						current = "-x";
				} else if(v.equals("-y")) {
					if(current.equals("+x"))
						current = "-y";
					else if(current.equals("-x"))
						current = "+y";
					else if(current.equals("+y"))
						current = "+x";
					else if(current.equals("-y"))
						current = "-x";
				} else if(v.equals("+z")) {
					if(current.equals("+x"))
						current = "+z";
					else if(current.equals("-x"))
						current = "-z";
					else if(current.equals("-z"))
						current = "+x";
					else if(current.equals("+z"))
						current = "-x";
				} else if(v.equals("-z")) {
					if(current.equals("+x"))
						current = "-z";
					else if(current.equals("-x"))
						current = "+z";
					else if(current.equals("+z"))
						current = "+x";
					else if(current.equals("-z"))
						current = "-x";
				}
			}
			System.out.println(current);
		}
	}

}
