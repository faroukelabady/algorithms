package competitveProg3.chapter1.medium.uva00573;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.NumberFormat;
import java.util.StringTokenizer;

public class Main {

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		try {
			myWork.Begin();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	void Begin() throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while(true) {
			String line = in.readLine();
			StringTokenizer tokenizer = new StringTokenizer(line);
			double h = Integer.parseInt(tokenizer.nextToken());
			double u = Integer.parseInt(tokenizer.nextToken());
			double d = Integer.parseInt(tokenizer.nextToken());
			double f = Integer.parseInt(tokenizer.nextToken());
			f = u * f / 100;
			if(h == 0 )
				break;
			
			double sum = 0;
			int count = 0;
			while(sum >= 0) {
				count++;
				sum += u;
				if(sum > h) {
					break;
				}
				sum -= d;
				u = Math.max(u-f, 0);
				
			}
			
			if(sum >= h) 
				System.out.printf("success on day %d\n" , count);
			else
				System.out.printf("failure on day %d\n" , count);
			
		}
		
		in.close();
		System.exit(0);
	}

}
