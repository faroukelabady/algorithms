package competitveProg3.chapter1.medium.uva10141;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.StringTokenizer;

public class Main {

	public class proposal {
		String name;
		float price;
		int metNum;
		int order;
	}

	public static void main(String[] args) throws IOException {
		Main main = new Main();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int count = 1;
		boolean first = true;
		while (true) {
			StringTokenizer line = new StringTokenizer(in.readLine());
			int reqNum = Integer.parseInt(line.nextToken());
			int propNum = Integer.parseInt(line.nextToken());

			if (reqNum == 0 && propNum == 0)
				break;
			for (int i = 0; i < reqNum; i++)
				in.readLine();

			List<proposal> l = new ArrayList<>();
			int counter = 0;
			for (int i = 0; i < propNum; i++) {
				proposal p = main.new proposal();
				p.order = ++counter;
				p.name = in.readLine();
				line = new StringTokenizer(in.readLine());
				p.price = Float.parseFloat(line.nextToken());
				p.metNum = Integer.parseInt(line.nextToken());
				for (int j = 0; j < p.metNum; j++)
					in.readLine();
				l.add(p);
			}
			l.sort(new Comparator<Main.proposal>() {
				public int compare(Main.proposal o1, Main.proposal o2) {
					if (o1.metNum > o2.metNum) {
						return -1;
					} else if (o1.metNum < o2.metNum) {
						return 1;
					} else {
						if (o1.price < o2.price) {
							return -1;
						} else if (o1.price > o2.price) {
							return 1;
						} else {
							if (o1.order < o2.order) {
								return -1;
							} else {
								return 1;
							}
						}
					}
				};
			});
			if (first)
				first = false;
			else
				System.out.println();
			System.out.println("RFP #" + count);
			System.out.println(l.get(0).name);
			count++;
		}
	}
}
