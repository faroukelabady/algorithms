package competitveProg3.chapter1.medium.uva00119;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.StringTokenizer;

public class Main {
	
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		try {
			myWork.Begin();
		} catch (NumberFormatException | IOException e) {
			e.printStackTrace();
		}
	}

	void Begin() throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		String line;
		StringTokenizer tokenizer;
		boolean first = true;
		while((line = in.readLine()) != null) {
			tokenizer = new StringTokenizer(line);
			int n = Integer.parseInt(tokenizer.nextToken());
			line = in.readLine();
			tokenizer = new StringTokenizer(line);
			Map<String, Integer> map = new LinkedHashMap<>();
			for(int i = 0; i< n; i++) {
				map.put(tokenizer.nextToken(), 0);
			}
			
			for(int i = 0; i< n; i++) {
				line = in.readLine();
				tokenizer = new StringTokenizer(line);
				String name = tokenizer.nextToken();
				int gift = Integer.parseInt(tokenizer.nextToken());

				int p = Integer.parseInt(tokenizer.nextToken());
				if(p == 0) continue;
				int avg = gift / p;
				map.put(name , map.get(name) - (avg * p));
				for(int j = 0; j < p; j++) {
					String rName = tokenizer.nextToken();
					map.put(rName , map.get(rName) + avg);
					
				}
			}
			
			if (first)
				first = false;
			else
				System.out.println();
			
			for(Entry<String,Integer> e : map.entrySet()) {
				System.out.printf("%s %d\n", e.getKey() , e.getValue());
			}
		}
		in.close();
		System.exit(0);
	}
}
