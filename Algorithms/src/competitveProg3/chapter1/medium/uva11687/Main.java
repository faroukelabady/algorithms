package competitveProg3.chapter1.medium.uva11687;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String x0 = in.readLine();
			if (x0.equals("END"))
				break;
			String xi = Integer.toString(x0.length());
			String xi_1 = Integer.toString(xi.length());
			int i = 1;
			while(true) {
				if(x0.equals(xi)) {
					break;
				}
				if(!xi.equals(xi_1)) {
					i++;
					xi = Integer.toString(xi_1.length());
					if(!xi.equals(xi_1)) {
						i++;
					}
					xi_1 = Integer.toString(xi.length());
				} else {
					i++;
					break;
				}
			}
			System.out.println(i);

		}
	}

}

/*
 * 		while ((s=br.readLine())!=null && !s.equals("END")) {
			String x=s;
			String lastX=String.valueOf(s.length());

			if (x.equals(lastX)) {
				System.out.println(1);
			} else {
				int count=0;
				while (!x.equals(lastX)) {
					lastX=x;
					x=String.valueOf(x.length());
					count++;
				}
				System.out.println(count);
			}
		}
		*/
