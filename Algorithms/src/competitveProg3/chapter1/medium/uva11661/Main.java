package competitveProg3.chapter1.medium.uva11661;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	static boolean ReadLn(String str) // utility function to read from stdin
	{
		int lg = 0;
		String line = "";
		int m = 0;
		int f = 0;
		for (char car : str.toCharArray()) {
			if ((car < 0) || (car == '\n'))
				break;
			if ((car == 'M' || car == 'm')) {
				m++;
			} else if (car == 'F' || car == 'f') {
				f++;
			}
		}
		return m == f && (m+f) > 2;
	}

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String input = in.readLine();
			if (input.equals("0"))
				break;

			StringTokenizer token = new StringTokenizer(input);
			int l = Integer.parseInt(token.nextToken());
			String highway = in.readLine();
			char[] data = highway.toCharArray();
			int resturantIndex = -1;
			int drugStoreIndex = -1;
			int minDistance = Integer.MAX_VALUE;
			for(int i =0; i < data.length; ++i) {
				if(data[i] == 'Z') {
					minDistance = 0;
					break;
				} else if(data[i] == 'R') {
					resturantIndex = i;
					if(drugStoreIndex != -1) {
						minDistance = Math.min(minDistance, Math.abs(drugStoreIndex - resturantIndex));
					}
				} else if(data[i] == 'D') {
					drugStoreIndex = i;
					if(resturantIndex != -1) {
						minDistance = Math.min(minDistance, Math.abs(drugStoreIndex - resturantIndex));
					}

				}
			}

			System.out.println(Math.abs(minDistance));
		}

	}


}
