package competitveProg3.chapter1.medium.uva11956;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader d = new BufferedReader(new InputStreamReader(System.in));
		int testCases = Integer.valueOf(d.readLine());
		int counter  = 1;
		while(testCases > 0) {
			char[] commands = d.readLine().toCharArray();
			byte[] memory = new byte[100];
			int index = 0;
			for(char c : commands) {
				if(c == '>') {
					if(index != 99) {
						index++;
					} else {
						index = 0;
					}
				} else if(c == '<') {
					if(index != 0) {
						index--;
					} else {
						index=99;
					}

				} else if(c == '+') {
					memory[index]++;
				} else if(c == '-') {
					memory[index]--;
				}
			}
			System.out.printf("Case %d: ", counter);
			counter++;
			for(int i =0 ; i < memory.length;i++) {
				System.out.printf("%02X", memory[i]);
				if(i < memory.length - 1) {
					System.out.printf(" ");
				}
			}
			System.out.println();
			testCases--;
		}
	}

}
