package competitveProg3.chapter1.medium.uva10424;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		int count = 0;
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				if ((car >= 'a' && car <= 'z')) {
					count+= car - 'a' + 1;
				} else if (car >= 'A' && car <= 'Z') {
					count+= car-'A' + 1;
				}
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return Integer.toString(count);
	}

	public static void main(String[] args) throws IOException {
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		while (true) {
			String firsCount = Main.ReadLn(255);
			if (firsCount == null) {
				break;
			}
			int a = 0;
			int b = 0;
			String secCount = Main.ReadLn(255);
			if(firsCount.equals("0") && secCount.equals("0")) {
				System.out.println();
				continue;
			}
			while(true) {
				int sum = 0;
				for(char c: firsCount.toCharArray()) {
					sum+= c - '1' + 1;
				}
				firsCount = Integer.toString(sum);
				if(sum < 10) {
					a = sum;
					break;
				}
			}
			while(true) {
				int sum = 0;
				for(char c: secCount.toCharArray()) {
					sum+= c - '1' + 1;
				}
				secCount = Integer.toString(sum);
				if(sum < 10) {
					b = sum;
					break;
				}
			}
			if(a < b) {
				float f = (float) a / (float) b;
				System.out.printf("%.2f %%\n", f * 100);
			} else {
				float f = (float) b / (float) a;
				System.out.printf("%.2f %%\n", f* 100);
			}

		}
	}

}
