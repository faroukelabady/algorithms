package competitveProg3.chapter1.medium.uva11683;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) throws IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		while (true) {
			String input = in.readLine();
			if (input.equals("0 0"))
				break;

			StringTokenizer token = new StringTokenizer(input);
			int height = Integer.parseInt(token.nextToken());
			int length = Integer.parseInt(token.nextToken());
			token = new StringTokenizer(in.readLine());
			int first = Integer.parseInt(token.nextToken());
			int start = height - first;
			int rounds = start;
			for (int i = 2; i <= length; ++i) {
				int next = Integer.parseInt(token.nextToken());
				if (next < first) {
					rounds += first - next;
				}
				first = next;

			}
			System.out.println(rounds);
		}
	}

}
