package competitveProg3.chapter1.medium.uva10324;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.StringTokenizer;

public class Main {
	
	public static void main(String[] args) throws IOException {
		Main main = new Main();
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int count = 1;
		while(true) {
			String value = in.readLine();
			if(value == null || value.isEmpty() || value.equals("0 0")) 
				break;
			StringTokenizer line = new StringTokenizer(in.readLine());
			int cases = Integer.parseInt(line.nextToken());
			System.out.println("Case " + count + ":");
			while(cases > 0) {
				line = new StringTokenizer(in.readLine());
				int i = Integer.parseInt(line.nextToken());
				int j = -1;
				if(line.hasMoreTokens())
					j = Integer.parseInt(line.nextToken());
				
				if(j == -1) j =i;
				
				int start = Math.min(i, j);
				int end = Math.max(i, j);
				char c;
				c = value.charAt(start);
				String Match = "Yes";
				for(int k = start + 1; k <= end; k++) {
					if(value.charAt(k) != c) {
						Match = "No";
						break;
					}
				}
				System.out.println(Match);
				cases--;
				
			}
			count++;
		}

	}

}
