package competitveProg3.chapter1.easy.uva12015;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;
import java.util.TreeMap;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringTokenizer token = new StringTokenizer(sc.nextLine());
		int testCases = Integer.parseInt(token.nextToken());
		int j = 1;
		while (testCases > 0) {
			TreeMap<Integer, List<String>> map = new TreeMap<Integer, List<String>>();
			for (int i = 1; i <= 10; i++) {
				String s = sc.nextLine();
				token = new StringTokenizer(s);
				String url = token.nextToken();
				int r = Integer.parseInt(token.nextToken());
				if (map.get(r) == null) {
					map.put(r, new ArrayList<String>());
				}
				map.get(r).add(url);
			}
			System.out.println("Case #" + j + ":");
			for (String url : map.lastEntry().getValue()) {
				System.out.println(url);
			}
			testCases--;
			j++;
		}
	}
}
