package competitveProg3.chapter1.easy.uva10114;

import java.io.IOException;
import java.util.LinkedList;
import java.util.Queue;
import java.util.StringTokenizer;

public class Main {
	
	private class tracker {
		
		public tracker(int monNum, double percent) {
			super();
			this.monNum = monNum;
			this.percent = percent;
		}
		int monNum;
		double percent;
	}
	
	public static String readLine(int maxLen) {
		byte[] b = new byte[maxLen];
		int lg =0, c=0;
		while(lg < maxLen) {
			try {
				c = System.in.read();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			if(c < 0 || c == '\n')
				break;
			b[lg++] += c;
		}
		if(c < 0 && lg == 0)
			return null;
		return new String(b, 0 , lg);
		
	}
	
	public static void main(String args[]) {
		Main work = new Main();
		work.Begin();
	}
	
	void Begin() {
		String input = "";
		StringTokenizer tokenizer;
		int months, depNum;
		double downPay, loan, carWorth , monPay;
		while(true) {
			input = Main.readLine(500);
			tokenizer = new StringTokenizer(input);
			months = Integer.parseInt(tokenizer.nextToken());
			downPay= Double.parseDouble(tokenizer.nextToken());
			loan = Double.parseDouble(tokenizer.nextToken());
			depNum = Integer.parseInt(tokenizer.nextToken());
			monPay = loan / (double)months;
			
			carWorth = loan + downPay;
			if(months < 0 ) {
				break;
			}
			
			Queue<tracker> queue = percentage(depNum);
			tracker track = queue.poll();
			tracker track2 = queue.peek();
			carWorth -=  carWorth * track.percent;
			int counter = 0;
			for(int i = 0; i < months; i++) {
				//System.out.println("Car worth : " + carWorth + " Loan : " + loan + " percentage : "  + track.percent);
				
				if(carWorth < loan )
					counter++;
				else 
					break;
				
				
				if(track2 != null  && !(i+1 < track2.monNum)) {
					track = queue.poll();
					track2 = queue.peek();
				} 
				
				loan -= monPay;
				carWorth -=  carWorth * track.percent;
				
			}
			System.out.println( "" + counter  + " month" + (counter != 1 ? "s":""));
		}
	}
	
	private Queue<tracker> percentage(int depNum) {
		Queue<tracker> map =new LinkedList<>();
		String input;
		StringTokenizer tokenizer;
		while(depNum > 0) {
			input = Main.readLine(500);
			tokenizer = new StringTokenizer(input);
			int monNum = Integer.parseInt(tokenizer.nextToken());
			double percent = Double.parseDouble(tokenizer.nextToken());
			map.add(new tracker(monNum, percent));
			depNum--;
		}
		return map;
		
	}
}
