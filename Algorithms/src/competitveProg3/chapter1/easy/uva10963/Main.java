package competitveProg3.chapter1.easy.uva10963;

import java.io.PrintWriter;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);

		int W = sc.nextInt();
		for (int i = 0; i < W; i++) {
			int C = sc.nextInt();
			boolean bad = false;
			int last = Math.abs(sc.nextInt() - sc.nextInt());
			for (int j = 1; j < C; j++) {
				int tmp = Math.abs(sc.nextInt() - sc.nextInt());
				if (last != tmp)
					bad = true;
			}
			if (bad) {
				out.println("no");
			} else {
				out.println("yes");
			}
			if (i < W - 1)
				out.println();
		}

		out.close();
		sc.close();
	}
}
