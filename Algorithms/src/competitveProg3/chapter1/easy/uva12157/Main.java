package competitveProg3.chapter1.easy.uva12157;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scan = new Scanner(System.in);
		int testCases = scan.nextInt();
		int count = 1;
		while(testCases > 0 ) {
			int numCalls = scan.nextInt();
			int mileSum = 0;
			int juiceSum = 0;
			for(int i =1; i <= numCalls; i++) {
				int duration = scan.nextInt();
				mileSum += (duration / 30) * 10 + 10;
				juiceSum += (duration / 60) * 15 + 15;
			}
			if(mileSum > juiceSum) {
				System.out.println("Case " + count +": Juice " + juiceSum);
			} else if (juiceSum > mileSum) {
				System.out.println("Case " + count +": Mile " + mileSum);
			} else {
				System.out.println("Case " + count +": Mile Juice " + mileSum);
			}
			testCases--;
			count++;
		}
	}
}
