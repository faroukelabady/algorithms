package competitveProg3.chapter1.easy.uva11332;

import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		PrintWriter out = new PrintWriter(System.out);
		String line = sc.nextLine();
		while (!line.equals("0")) {
			StringTokenizer token = new StringTokenizer(line);
			int val = Integer.parseInt(token.nextToken());
			while(val > 9) {
				val = recursiveSum(val);
			}
			System.out.println(val);
			line = sc.nextLine();
		}
		sc.close();
		out.close();
	}
	
	public static int recursiveSum(int value) {
		int sum = 0;
		if(value <= 9)
			return value;
		sum += value % 10;
		return sum + recursiveSum(value / 10);
	}
}
