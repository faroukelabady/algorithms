package competitveProg3.chapter1.easy.uva12554;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = Main.ReadLn(255);
		StringTokenizer tokenizer = new StringTokenizer(input);
		int n = Integer.parseInt(tokenizer.nextToken());
		String[] str = new String[n];
		for (int i = 0; i < n; i++) {
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			str[i] = tokenizer.nextToken();
		}
		String[] song = new String[] { "Happy", "birthday", "to", "you", "Happy", "birthday", "to", "you", "Happy",
				"birthday", "to", "Rujia", "Happy", "birthday", "to", "you" };

		int i = 0, j = 0;
		boolean flag1 = false;
		boolean flag2 = false;
		int repeat = 1;
		while ((flag1 == false || flag2 == false) || repeat > 0) {
			System.out.println(str[i] + ": " + song[j]);
			i++;
			j++;
			if (i >= n) {
				i = 0;
				flag1 = true;
			}

			if (j >= song.length) {
				if(i < n && flag1 == false)
					repeat++;
				
				repeat--;
				j = 0;
				flag2 = true;
			}
		}
	}
}
