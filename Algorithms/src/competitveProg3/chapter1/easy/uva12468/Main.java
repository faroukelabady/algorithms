package competitveProg3.chapter1.easy.uva12468;

import java.io.IOException;
import java.util.StringTokenizer;



public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
//		for(int i = 0; i <100; i++  ) {
//			for(int j = 0; j < 100; j++) {
//				System.out.println(i + " " + j );
//			}
//		}
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}
	
	void Begin() {
		String input;
		StringTokenizer tokenizer;
		
		while((input = Main.ReadLn(255)) != null) {
			tokenizer = new StringTokenizer(input);
			int startChannel = Integer.parseInt(tokenizer.nextToken());
			int endChannel = Integer.parseInt(tokenizer.nextToken());
			if(startChannel  < 0 && endChannel < 0)
				break;
//			
//			int result = Math.abs(startChannel - endChannel);
//			if(result > 50) result = 100 - result;
			
			int plusApproach = endChannel - startChannel;
			plusApproach = plusApproach >= 0 ? plusApproach : 100 + plusApproach;
			int minusApproach = startChannel - endChannel;
			minusApproach = minusApproach >= 0 ? minusApproach : 100 + minusApproach;
			System.out.println(Math.min(plusApproach, minusApproach));
		} 
	}
}
