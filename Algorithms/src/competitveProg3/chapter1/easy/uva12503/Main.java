package competitveProg3.chapter1.easy.uva12503;

import java.io.IOException;
import java.util.StringTokenizer;

public class Main {
	
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}
	
	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}
	
	
	void Begin() {
		String input = Main.ReadLn(255);
		StringTokenizer tokenizer = new StringTokenizer(input);
		int testCases = Integer.parseInt(tokenizer.nextToken());
		
		while(testCases > 0) {
			input = Main.ReadLn(255);
			tokenizer = new StringTokenizer(input);
			int r = Integer.parseInt(tokenizer.nextToken());
			String[] str = new String[r];
			int p = 0;
			for(int i = 0 ; i < r; i++) {
				input = Main.ReadLn(255);
				tokenizer = new StringTokenizer(input);
				str[i] = tokenizer.nextToken();
				if(str[i].equalsIgnoreCase("LEFT")) {
					p--;
				} else if(str[i].equalsIgnoreCase("RIGHT")) {
					p++;
				} else {
					tokenizer.nextToken();
					int pos  = Integer.parseInt(tokenizer.nextToken());
					str[i] = str[pos-1];
					if(str[i].equalsIgnoreCase("LEFT")) {
						p--;
					} else if(str[i].equalsIgnoreCase("RIGHT")) {
						p++;
					}
				}
			}
			System.out.println(p);
			testCases--;
		} 
	}
}
