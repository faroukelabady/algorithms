package competitveProg3.chapter1.easy.uva11679;

import java.util.Scanner;

public class Main {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		
		while(sc.hasNextLine()) {
			int b = sc.nextInt();
			int n = sc.nextInt();
			
			if(b == 0 && n == 0)
				break;
			
			int[] r = new int[b+1];
			
			for(int i = 1 ; i <= b; i++) {
				r[i] = sc.nextInt();
			}
			
			for(int i = 1; i <= n; i++) {
				int d = sc.nextInt();
				int c = sc.nextInt();
				int v = sc.nextInt();
				r[d] -= v;
				r[c] += v;
			}
			boolean flag = false;
			for(int i = 1;i<=b;i++) {
				if(r[i]< 0) {
					flag = true;
					break;
				}
			}
			
			if(flag) {
				System.out.println("N");
			} else {
				System.out.println("S");
			}
		}
	}
}
