package competitveProg3.chapter1.easy.uva11942;

import java.util.Arrays;
import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		System.out.println("Lumberjacks:");
		while (testCases > 0) {

			int current = sc.nextInt();
			int next = sc.nextInt();
			boolean sortOrder = next > current;
			boolean isSorted = true;
			for (int i = 3; i <= 10; i++) {
				int afterNext = sc.nextInt();
				if (isSorted) {
					if (sortOrder) {
						if (afterNext < next) {
							System.out.println("Unordered");
							isSorted = false;
						}
					} else {
						if (afterNext > next) {
							System.out.println("Unordered");
							isSorted = false;
						}
					}
				}
				next = afterNext;
			}

			if (isSorted) {
				System.out.println("Ordered");
			}
			testCases--;
		}
		sc.close();

	}

}
