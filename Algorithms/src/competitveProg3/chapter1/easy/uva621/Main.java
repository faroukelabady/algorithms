package competitveProg3.chapter1.easy.uva621;

import java.io.IOException;


public class Main {
	
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input = Main.ReadLn(255);
		int testCases, a ,b ,c;
		testCases = Integer.parseInt(input);
		for (; testCases > 0; testCases--) {
			input = Main.ReadLn(255);
			if(input.endsWith("35")) {
				System.out.println("-");
			} else if(input.startsWith("9") && input.endsWith("4")) {
				System.out.println("*");
			} else if (input.startsWith("190")) {
				System.out.println("?");
			} else {
				System.out.println("+");
			}
		}
	}
}
