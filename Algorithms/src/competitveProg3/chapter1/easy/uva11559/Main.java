package competitveProg3.chapter1.easy.uva11559;

import java.io.PrintWriter;
import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		try {
			while (sc.hasNextLine()) {
				int n = sc.nextInt();
				int b = sc.nextInt();
				int h = sc.nextInt();
				int w = sc.nextInt();
				int minAmount = Integer.MAX_VALUE;
				for (int i = 1; i <= h; i++) {
					int price = sc.nextInt();
					for (int j = 1; j <= w; j++) {
						int bedNum = sc.nextInt();
						if (bedNum < n) {
							continue;
						}
						int finalPrice = price * n;
						if (finalPrice <= b) {
							minAmount = Math.min(minAmount, finalPrice);
						}
					}
				}

				if (minAmount == Integer.MAX_VALUE) {
					System.out.println("stay home");
				} else {
					System.out.println(minAmount);
				}
			}
		} catch (Exception e) {
			//e.printStackTrace();
		} finally {
			sc.close();
		}
	}
}
