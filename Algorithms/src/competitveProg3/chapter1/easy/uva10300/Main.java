package competitveProg3.chapter1.easy.uva10300;

import java.io.IOException;
import java.util.StringTokenizer;


public class Main {
	
	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";
		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}
	
	public static void main(String args[]) // entry point from OS
	{
		Main myWork = new Main(); // create a dinamic instance
		myWork.Begin(); // the true entry point
	}

	void Begin() {
		String input;
		StringTokenizer tokenizer;
		while ((input = Main.ReadLn(255)) != null) {
			int testCases = Integer.parseInt(input);
			for(; testCases > 0 ;testCases--) {
				input = Main.ReadLn(255);
				int farmerNum = Integer.parseInt(input);
				int result = 0;
				for(; farmerNum > 0 ; farmerNum--) {
					tokenizer = new StringTokenizer(Main.ReadLn(255));
					int size = Integer.parseInt(tokenizer.nextToken());
					int animals = Integer.parseInt(tokenizer.nextToken());
					int env = Integer.parseInt(tokenizer.nextToken());
					result += size * env;
				}
				System.out.println(result);
			}
		}
	}
}
