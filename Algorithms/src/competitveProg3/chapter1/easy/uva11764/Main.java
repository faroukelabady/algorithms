package competitveProg3.chapter1.easy.uva11764;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		int j = 1;
		while(testCases > 0) {
			int n = sc.nextInt();
			int high = 0;
			int low = 0;
			int current = sc.nextInt();
			for(int i =1; i < n; i++) {
				int next = sc.nextInt();
				if(current < next) {
					high++;
				} else if(current > next) {
					low++;
				}
				current = next;
			}
			System.out.println("Case " + j + ": " + high + " " + low);
			j++;
			testCases--;
		}
		sc.close();
	}
}
