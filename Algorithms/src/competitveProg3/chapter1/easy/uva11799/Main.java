package competitveProg3.chapter1.easy.uva11799;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int testCases = sc.nextInt();
		int j = 1;
		while(testCases > 0 ) {
			
			int n = sc.nextInt();
			int max = -1;
			for(int  i = 1 ; i <= n; i++) {
				max = Math.max(max, sc.nextInt());
			}
			System.out.println("Case " + j + ": " + max);
			testCases--;
			j++;
		}
		sc.close();
	}
}
