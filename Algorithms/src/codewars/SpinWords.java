package codewars;

import java.util.Arrays;
import java.util.stream.Collectors;

public class SpinWords {

	public String spinWords(String sentence) {
		return Arrays.stream(sentence.split(" "))
				.map(t -> t.length() >= 5 ? new StringBuilder(t).reverse().toString() : t)
				.collect(Collectors.joining(" "));
	}

}
