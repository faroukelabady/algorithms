package codewars;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;

/*
 * Given two arrays of strings a1 and a2 return a sorted array r
 *  in lexicographical order of the strings of a1 which are substrings of strings of a2.

#Example 1: a1 = ["arp", "live", "strong"]

a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

returns ["arp", "live", "strong"]

#Example 2: a1 = ["tarp", "mice", "bull"]

a2 = ["lively", "alive", "harp", "sharp", "armstrong"]

returns []

#Notes: Arrays are written in "general" notation. See "Your Test Cases" for examples in your language.

Beware: r must be without duplicates.
 */
public class WhichAreIn {

	public static String[] inArray(String[] array1, String[] array2) {
		StringBuilder sb = new StringBuilder();
		HashSet<String> r = new HashSet<>();
		for (int i = 0; i < array1.length; i++) {
			for (String str : array2) {
				if (str.contains(array1[i])) {
					r.add(array1[i]);
					break;
				}
			}
		}
		String[] res = {} ;
		res = r.toArray(res);
		Arrays.sort(res);
		return res;
	}


	public static void main(String[] args) {
		String a[] = new String[]{ "tarp", "mice", "bull" };
    	String b[] = new String[] { "lively", "alive", "harp", "sharp", "armstrong" };
    	String r[] = new String[] { "arp", "live", "strong" };
		r = WhichAreIn.inArray(a, b);
		System.out.println(Arrays.toString(r));
	}
}
