package hackerrank;

import java.util.Arrays;
import java.util.function.LongBinaryOperator;

public class Train {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}

	static long aVeryBigSum(int n, long[] ar) {
	        // Complete this function
	        long result = 0;
	        for(long number: ar) {
	            result += number;
	        }
	        return Arrays.stream(ar).reduce((left,right) -> left + right).getAsLong();
	        
	            
	    }

}
