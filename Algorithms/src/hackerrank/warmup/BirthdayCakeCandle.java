package hackerrank.warmup;

import java.util.Scanner;

public class BirthdayCakeCandle {
	
	   static int birthdayCakeCandles(int n, int[] ar) {
	        // Complete this function
		   return 0;
	    }

	    public static void main(String[] args) {
	        Scanner in = new Scanner(System.in);
	        int n = in.nextInt();
	        int count = 0;
	        int tallest = Integer.MIN_VALUE;
	        for(int ar_i = 0; ar_i < n; ar_i++){
	        	int candle = in.nextInt();
	        	if(candle > tallest) {
	        		count = 1;
	        		tallest = candle;
	        	} else if(candle == tallest) {
	        		count++;
	        	} 
	        }
	        System.out.println(count);
	        in.close();
	    }

}
