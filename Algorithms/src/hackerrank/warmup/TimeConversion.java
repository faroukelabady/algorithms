package hackerrank.warmup;

import java.time.LocalTime;
import java.time.format.DateTimeFormatterBuilder;
import java.util.Scanner;

public class TimeConversion {

//	static String timeConversion(String s) {
//		// Complete this function
//		DateTimeFormatterBuilder f = new DateTimeFormatterBuilder().appendPattern("hh:mm:ssa");
//		LocalTime time = LocalTime.parse(s ,  f.toFormatter());
//		f = new DateTimeFormatterBuilder().appendPattern("HH:mm:ss");
//		return time.format(f.toFormatter());
//	}
//	
	static String timeConversion(String s) {
		String[] split = s.split(":");
		boolean pmFlag = false;
		if(split[2].contains("PM")) {
			pmFlag = true;
		}
		int h = Integer.parseInt(split[0]); 
		if(pmFlag && h < 12) {
			split[0] = String.format("%02d", h + 12);
		} else if(h == 12 && !pmFlag) {
			split[0] = String.format("%02d", 0);
		}
		return String.join(":", split).replaceFirst(".M", "");
	}
	
//	static String timeConversion(String time) {
//		 String tArr[] = time.split(":");
//	     String AmPm = tArr[2].substring(2,4);
//	     int hh,mm,ss;
//	     hh = Integer.parseInt(tArr[0]);
//	     mm = Integer.parseInt(tArr[1]);
//	     ss = Integer.parseInt(tArr[2].substring(0,2));
//	     
//	     String checkPM = "PM",checkAM ="AM";
//	     int h = hh;
//	     if(AmPm.equals(checkAM) && hh==12)
//	     	h=0;
//	     else if(AmPm.equals(checkPM)&& hh<12)
//	     	h+=12;
//	     
//	     return String.format("%02d:%02d:%02d",h,mm,ss);
//	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		String s = in.next();
		String result = timeConversion(s);
		System.out.println(result);
	}

}
