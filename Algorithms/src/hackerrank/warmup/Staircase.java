package hackerrank.warmup;

import java.util.Scanner;

public class Staircase {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner sc = new Scanner(System.in);
		int n = sc.nextInt();
		sc.close();

		for (int i = 0; i < n; i++) {
			for (int j = 1; j <= n - i - 1; j++) {
				System.out.print(" ");
			}
			
			for (int j = 0; j <= i; j++) {
				System.out.print("#");
			}
			
			if(i < n - 1)
				System.out.println();	
		}
	}

	/*
	 * 
	 * try (Scanner s = new Scanner(System.in)) {
    int n = s.nextInt();    
    char[] a = new char[n];
    Arrays.fill(a, ' ');
    for (int i = n-1; i >= 0; i--) {
        a[i] = '#';
        System.out.println(new String(a));
    }
}

public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    String str="#";
    for (int i=0;i<n;i++)
        { 
        System.out.printf("%"+n+"s%n",str);
        str=str+"#";
    }
}

  public static void main(String[] args) {
        Scanner scan = new Scanner(System.in);
        int numInputs = scan.nextInt();
        scan.close();
        
        for(int i = 1; i <= numInputs; i++){ // rows
            
            int j = numInputs;
            
            // loop to print (size - i) spaces
            while(j-- > i) {
                System.out.print(" ");
            }
            // loop to print (i) #s
            while(j-- >= 0) {
                System.out.print("#");
            }
            
            System.out.println();
        }
        
    }
	 */
}
