package hackerrank.warmup;

import java.util.Scanner;
import java.util.stream.Stream;

public class DiagonalDifference {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int matrixDimension= sc.nextInt();
		sc.nextLine();
		int mainSum = 0;
		int secSum = 0;
		for(int i = 0; i < matrixDimension; i++) {
			String line = sc.nextLine();
			int[] row = Stream.of(line.split(" ")).mapToInt(Integer::valueOf).toArray();
			mainSum += row[i];
			secSum += row[matrixDimension - i - 1];
		}
		System.out.println(Math.abs(mainSum - secSum));
		sc.close();
		//Stream.of(matrix).flatMap(Stream::of).forEach(it -> System.out.println(Arrays.toString(it)));
	}
	
	/*
	 * for(int j = 0; j < numInputs; j++){
    for(int k = 0; k < numInputs; k++){
        cin >> curInput;
        if(j == k){
            leftD += curInput;
        }
        if(j+k == (numInputs-1)){
            rightD += curInput;
        }
    }
}
ans = abs(leftD-rightD);
	 */

}
