package hackerrank.warmup;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.regex.Pattern;

public class PlusMinus {
	
	int positiveSum = 0;
	int negativeSum = 0;
	int zerosSum = 0;
	
	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader in = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(in.readLine());
		PlusMinus pm = new PlusMinus();
		in.lines().limit(1)
			.flatMap(Pattern.compile(" ")::splitAsStream).mapToInt(Integer::valueOf).forEach(it -> {
				if(it > 0) 
					pm.positiveSum++;
				else if(it < 0)
					pm.negativeSum++;
				else 
					pm.zerosSum++;
			});
		
		System.out.println(String.format("%.6f", (float) pm.positiveSum / n));
		System.out.println(String.format("%.6f", (float) pm.negativeSum / n));
		System.out.println(String.format("%.6f", (float) pm.zerosSum / n));
	}

}
