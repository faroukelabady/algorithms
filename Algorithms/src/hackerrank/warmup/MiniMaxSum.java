package hackerrank.warmup;

import java.util.Arrays;
import java.util.Scanner;

public class MiniMaxSum {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int[] arr = new int[5];
		for (int arr_i = 0; arr_i < 5; arr_i++) {
			arr[arr_i] = in.nextInt();
		}
		
		long min = Long.MAX_VALUE;
		long max = Long.MIN_VALUE;
		long sum = 0;
		for(int a : arr) {
			sum += a;
			if(a < min) {
				min = a;
			}
			
			if(a > max) {
				max = a;
			}
		}
		
//		Arrays.sort(arr);
//		long minSum = 0;
//		long maxSum = 0;
//		int i = 0;
//		while(i <= 3) {
//			minSum += arr[i];
//			maxSum += arr[i+1];
//			i++;
//		}
		
		System.out.println( (sum - max) + " "  + (sum - min));
		
	}
	
	/*
	 * 
	 *  long max, min, sum;
    sum = max = min = in.nextLong();

    for(int i=1; i<5;i++){
        long temp = in.nextLong();
        sum += temp;
        if(max>temp){
            if(min > temp) {
                min = temp;
            }
        } else {
            max = temp;
        }
    }

    System.out.print((sum -max) + " " + (sum - min));
	 * 
	 */

}
