package hackerrank.implementation;

import java.util.Scanner;

public class DayOfTheProgrammer {

	static String solve(int year) {
		// Complete this function
		// 1700 to 1917 julian 
		// we can try to get feb only instead of all the months
		if(year <= 1917) {
			boolean leapYear = (year % 4 == 0);
			if(leapYear)
				return "12.09." + year;
			else
				return "13.09." + year;
			
		} else if(year >= 1919) {
			boolean leapYear = (year % 400 == 0 || (year % 4 == 0 && year % 100 != 0));
			if(leapYear)
				return "12.09." + year;
			else
				return "13.09." + year;
		} else {
			return "27.09.1918";
		}
		
		// 1919 till now gregorian
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int year = in.nextInt();
		String result = solve(year);
		System.out.println(result);
	}

}
