package hackerrank.implementation;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

public class ClimbingTheLeaderboard {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		List<Integer> scores = new ArrayList<>();
		scores.add(Integer.MAX_VALUE);
		int rank = 1;
		for (int scores_i = 1; scores_i < n + 1; scores_i++) {
			int score = in.nextInt();
			if(scores.get(rank - 1) > score) {
				scores.add(score);
				rank++;
			} 
		}
		
		int m = in.nextInt();
		int[] alice = new int[m];
		for (int alice_i = 0; alice_i < m; alice_i++) {
			alice[alice_i] = in.nextInt();
		}
		
		for(int a: alice) {
			System.out.println(indexedBinarySearch(scores, a));
		}
		
		// your code goes here

	}
	
	private static int indexedBinarySearch(List<Integer> list, Integer key) {
        int low = 0;
        int high = list.size()-1;

        while (low <= high) {
            int mid = (low + high) >>> 1;
            int cmp = key.compareTo(list.get(mid));

            if (cmp < 0)
                low = mid + 1;
            else if (cmp > 0)
                high = mid - 1;
            else
                return mid; // key found
        }
        return low;  // key not found
    }

}
