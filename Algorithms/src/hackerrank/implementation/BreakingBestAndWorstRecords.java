package hackerrank.implementation;

import java.util.Scanner;

public class BreakingBestAndWorstRecords {

	static int[] getRecord(int[] s) {
		// Complete this function
		int mostScore = s[0];
		int leastScore = s[0];
		int[] result = new int[2];
		
		for(int score: s) {
			if(score > mostScore) {
				mostScore = score;
				result[0] += 1;
			}
			
			if(score < leastScore) {
				leastScore = score;
				result[1] += 1;
			}
		}
		
		return result;
		
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] s = new int[n];
		for (int s_i = 0; s_i < n; s_i++) {
			s[s_i] = in.nextInt();
		}
		int[] result = getRecord(s);
		String separator = "", delimiter = " ";
		for (Integer val : result) {
			System.out.print(separator + val);
			separator = delimiter;
		}
		System.out.println("");
	}
	
    public class Record {
        private int score;
        public Record left;
        public Record right;
        
        /**
        *    A best or worst score record.
        *    @param The record's score.
        **/
        public Record(int score) {
            this.score = score;
            this.left = null;
            this.right = null;
        }
        
        /**
        *    Checks for and inserts new records.
        *    @param score The score for the (potential) new best or worst record.
        **/
        public void insert(int score) {
            if (score < this.score) {
                if (this.left == null) {
                    this.left = new Record(score);
                }
                else {
                    this.left.insert(score);
                }
                
            }
            else if (score > this.score) {
                if (this.right == null) {
                    this.right = new Record(score);
                }
                else {
                    this.right.insert(score);
                }
            }
        }
        
        /**
        *    @return The number of times the worst record was broken (edges in left branch of tree).
        **/
        public int depthLeft() {
            System.err.println("Worst Record: " + this.score);
            return (this.left == null) ? 0 : (1 + this.left.depthLeft());
        }
        
        /**
        *    @return The number of times the best record was broken (edges in right branch of tree).
        **/
        public int depthRight() {
            System.err.println("Best Record: " + this.score);
            return (this.right == null) ? 0 : (1 + this.right.depthRight());
        }
    }
    
    public String solve(int[] scores) {
        Record root = new Record(scores[0]);
        
        for (int i = 1; i < scores.length; i++) {
            root.insert(scores[i]);
        }
        
        // Return number of broken records in the correct order
        return root.depthRight() + " " + root.depthLeft();
    }


}
