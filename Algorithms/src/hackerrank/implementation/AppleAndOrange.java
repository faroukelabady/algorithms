package hackerrank.implementation;

import java.util.Scanner;

public class AppleAndOrange {
	
	static int houseStart = 0;
	static int houseEnd = 0;
	static int appleTree = 0;
	static int ornageTree = 0;
	
	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		houseStart = in.nextInt();
		houseEnd = in.nextInt();
        appleTree = in.nextInt();
        ornageTree = in.nextInt();
        int noOfApples = in.nextInt();
        int noOfOranges = in.nextInt();
        int appleCounter = 0;
        for(int apple_i=0; apple_i < noOfApples; apple_i++){
        	appleCounter += isWithinRange(appleTree, in.nextInt())? 1: 0;
        }
        
        int orangeCounter = 0;
        for(int orange_i=0; orange_i < noOfOranges; orange_i++){
        	orangeCounter += isWithinRange(ornageTree, in.nextInt())? 1: 0;
        }
        
        // we need to print apples falls within house range first then oranges
        System.out.println(appleCounter);
        System.out.println(orangeCounter);
        in.close();
        
//      long orangeHits = IntStream.generate(()->in.nextInt())
//        .limit(n)
//        .filter(d -> d+b>=s && d+b<=t)
//        .count(); 
	}
	
	
	private static boolean isWithinRange(int treePosition, int distance) {
		int position = treePosition + distance;
		if(position >= houseStart && position <= houseEnd) {
			return true;
		}
		
		return false;
	}
	
	

}
