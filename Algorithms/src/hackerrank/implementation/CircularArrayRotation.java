package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class CircularArrayRotation {

	// Complete the circularArrayRotation function below.
	static int[] circularArrayRotation(int[] a, int k, int[] queries) {

		LinkedList<Integer> list = new LinkedList<>();
    	for(int i: a) {
    		list.add(i);
    	}
    	
    	for(int i = 1; i <= k; i++) {
    		Integer v = list.removeLast();
    		list.addFirst(v);
    	}
    	
    	ArrayList<Integer> arr = new ArrayList<>(list);
    	
		int[] result = new int[queries.length];
		for (int i = 0; i < queries.length; i++) {
			result[i] = arr.get(queries[i]);
		}
		
		return result;

	}
	/*
	 * 
	 * public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int N = in.nextInt();
    int K = in.nextInt();
    int Q = in.nextInt();
    int rot = K % N;
    int[] arr = new int[N];
    for (int i = 0; i < N; i++)
        arr[i] = in.nextInt();
    for (int i = 0; i < Q; i++) {
        int idx = in.nextInt();
        if (idx - rot >= 0)
            System.out.println(arr[idx - rot]);
        else
            System.out.println(arr[idx - rot + arr.length]);
		}
	}
	
	public static void main(String[] args) {
    Scanner in = new Scanner(System.in);
    int n = in.nextInt();
    int k = in.nextInt();
    int q = in.nextInt();
    int[] a = new int[n];
    for(int a_i=0; a_i < n; a_i++){
        a[a_i] = in.nextInt();
    }
    for(int a0 = 0; a0 < q; a0++){
        int m = in.nextInt();
        System.out.println(a[(n - (k % n)+ m) % n]);
    }               
}
	 */

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		String[] nkq = scanner.nextLine().split(" ");

		int n = Integer.parseInt(nkq[0]);

		int k = Integer.parseInt(nkq[1]);

		int q = Integer.parseInt(nkq[2]);

		int[] a = new int[n];

		String[] aItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int aItem = Integer.parseInt(aItems[i]);
			a[i] = aItem;
		}

		int[] queries = new int[q];

		for (int i = 0; i < q; i++) {
			int queriesItem = scanner.nextInt();
			scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
			queries[i] = queriesItem;
		}

		int[] result = circularArrayRotation(a, k, queries);
		
		System.out.println(Arrays.toString(result));

//		for (int i = 0; i < result.length; i++) {
//			bufferedWriter.write(String.valueOf(result[i]));
//
//			if (i != result.length - 1) {
//				bufferedWriter.write("\n");
//			}
//		}
//
//		bufferedWriter.newLine();
//
//		bufferedWriter.close();

		scanner.close();
	}

}
