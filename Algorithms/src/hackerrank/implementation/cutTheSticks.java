package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class cutTheSticks {
	
	 // Complete the cutTheSticks function below.
    static int[] cutTheStickss(int[] arr) {
    	
    	int smallest = Integer.MAX_VALUE;
    	for(int i : arr) {
    		if(i < smallest)
    			smallest = i;
    	}
    	
    	int zeroCount = 0;
    	List<Integer> list = new ArrayList<>();
    	while(zeroCount < arr.length) {
    		int sticks = 0;
    		int nextSmallest = Integer.MAX_VALUE;
    		for(int i = 0; i < arr.length; i++ ) {
    			if(arr[i] > 0) {
    				int v = arr[i] - smallest;
    				if(v == 0) {
        				zeroCount++;
        			} else if(v < nextSmallest) {
        				nextSmallest = v;
        			}
        			
        			arr[i] = v;
        			sticks++;
    			}
    		}
    		smallest = nextSmallest;
    		list.add(sticks);
    	}
    	
    	return list.stream().mapToInt(i->i).toArray();


    }

    /*
     * 
     *  int main(void) {
    int N; 
    
    cin >> N;           // Taking the number of elements from user's input.
    cout << N << endl;  // Printing the number of our elements since our output will start with number of the full list.
    
    int Array[N];
    
    for (int i = 0; i < N; i++) 
        cin >> Array[i];            // Taking all elements in to our Array.
    
    sort(Array, Array + N);         // Sorting list from minimum to maximum.
    
    for (int i = 0; i < N-1; i++)       
        if (Array[i] != Array[i+1])     
            cout << N-(i+1) << endl; // Printing our output by subtracting number of smallest item/items from our lenght of array.                    
    return 0;
}            
     */
    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int[] result = cutTheStickss(arr);

        for (int i = 0; i < result.length; i++) {
           System.out.println(String.valueOf(result[i]));

//            if (i != result.length - 1) {
//                bufferedWriter.write("\n");
//            }
        }

//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

        scanner.close();
    }

}
