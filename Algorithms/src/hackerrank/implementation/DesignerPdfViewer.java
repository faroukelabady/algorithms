package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class DesignerPdfViewer {
	
	 // Complete the designerPdfViewer function below.
    static int designerPdfViewer(int[] h, String word) {
    	
    	int offset = 97;
    	int maxHeight = Integer.MIN_VALUE;
    	for(char c : word.toCharArray()) {
    		maxHeight = Math.max(h[c - offset], maxHeight);
//    		int index = c - offset;
//    		int currentHeight = h[index];
//    		if(currentHeight > maxHeight) {
//    			maxHeight = currentHeight;
//    		}
    	}
    	return maxHeight * word.length();


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
    	
    		int offset = 98;
    		"farouk".chars().forEach(System.out::println);
    		
    		System.out.println('a' - offset);
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
//
//        int[] h = new int[26];
//
//        String[] hItems = scanner.nextLine().split(" ");
//        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//        for (int i = 0; i < 26; i++) {
//            int hItem = Integer.parseInt(hItems[i]);
//            h[i] = hItem;
//        }
//
//        String word = scanner.nextLine();
//
//        int result = designerPdfViewer(h, word);
//
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();
//
//        scanner.close();
    }

}
