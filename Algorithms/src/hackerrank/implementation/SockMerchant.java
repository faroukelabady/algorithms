package hackerrank.implementation;

import java.util.HashSet;
import java.util.PriorityQueue;
import java.util.Scanner;

public class SockMerchant {

	static int sockMerchant(int n, int[] ar) {
		// Complete this function
		HashSet<Integer> s = new HashSet<>();
		int count = 0;
		for(int i = 0; i < n; i++) {
			if(s.contains(ar[i])) {
				count++;
				s.remove(ar[i]);
			} else {
				s.add(ar[i]);
			}
		}
		
		return count;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int ar_i = 0; ar_i < n; ar_i++) {
			ar[ar_i] = in.nextInt();
		}
		int result = sockMerchant(n, ar);
		System.out.println(result);
	}

}
