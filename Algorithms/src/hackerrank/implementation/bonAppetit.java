package hackerrank.implementation;

import java.util.Scanner;

public class bonAppetit {

	static int bonAppetit(int n, int k, int b, int[] ar) {
		// Complete this function
		/*
		 * int max = IntStream.range(0,n).map(x -> x!=k?ar[x]:0).sum()/2;
System.out.println(max==b?"Bon Appetit":(b-max));

int total = (charged << 1);
        
        for (int i : cost) {
            total = total - i;
        }
        
        System.out.println( (total >= 0) ? cost[k] / 2 : "Bon Appetit");
		 */
		int actualSum =0;
		for(int i = 0; i < n; i++) {
			if(i != k) {
				actualSum += ar[i];
			}
		}
		int actualBill = actualSum / 2;
		if(actualBill == b) {
			return 0;
		} else {
			return b - actualBill;
		}
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int k = in.nextInt();
		int[] ar = new int[n];
		for (int ar_i = 0; ar_i < n; ar_i++) {
			ar[ar_i] = in.nextInt();
		}
		int b = in.nextInt();
		int result = bonAppetit(n, k, b, ar);
		System.out.println(result != 0? result: "Bon Appetit");
	}

}
