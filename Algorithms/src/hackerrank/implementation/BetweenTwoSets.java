package hackerrank.implementation;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class BetweenTwoSets {

	public static int gcd(int a, int b) {
		while (a > 0 && b > 0) {

			if (a >= b) {
				a = a % b;
			} else {
				b = b % a;
			}
		}

		return a + b;
	}

	public static int lcm(int a, int b) {
		return (a / gcd(a, b)) * b;
	}

	public static int getTotalX(int[] a, int[] b) {

		int multiple = 0;
		for (int i : b) {
			multiple = gcd(multiple, i);
		}
		// System.err.println("Multiple: " + multiple);

		int factor = 1;
		for (int i : a) {
			factor = lcm(factor, i);
			if (factor > multiple) {
				return 0;
			}
		}

		if (multiple % factor != 0) {
			return 0;
		}
		// System.err.println("Factor: " + factor);

		int value = multiple / factor;

		int max = Math.max(factor, value);
		int totalX = 1;

		for (int i = factor; i < multiple; i++) {
			if (multiple % i == 0 && i % factor == 0) {
				totalX++;
			}
		}

		return totalX;

	}

	static int getTotalX(int[] a, int[] b, int maxA, int minB) {
		// Complete this function
		// greatest common divisor
		// lowest common multiplier
		if (maxA > minB) {
			return 0;
		}
		List<Integer> x_p = new ArrayList<>();

		double start = minB;
		while (start >= maxA) {
			if (start % maxA == 0 && minB % start == 0) {
				x_p.add((int) start);
			}
			start -= 1;
		}

		int result = 0;

		for (int x : x_p) {
			boolean valid = true;
			for (int a_i : a) {
				if (x % a_i != 0) {
					valid = false;
					break;
				}
			}

			if (valid == false) {
				continue;
			}

			for (int b_i : b) {
				if (b_i % x != 0) {
					valid = false;
					break;
				}
			}

			if (valid) {
				result++;
			}

		}

		return result;

	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int m = in.nextInt();
		int[] a = new int[n];
		int maxA = Integer.MIN_VALUE;
		for (int a_i = 0; a_i < n; a_i++) {
			a[a_i] = in.nextInt();
			if (a[a_i] > maxA) {
				maxA = a[a_i];
			}
		}

		int[] b = new int[m];
		int minB = Integer.MAX_VALUE;
		for (int b_i = 0; b_i < m; b_i++) {
			b[b_i] = in.nextInt();
			if (b[b_i] < minB) {
				minB = b[b_i];
			}
		}
		int total = getTotalX(a, b);
		System.out.println(total);
		in.close();
	}

}
