package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class AppendAndDelete {
	
	// Complete the appendAndDelete function below.
    static String appendAndDelete(String s, String t, int k) {
    	
    	int diffIndex = 0;
    	for(int i = 0; i < s.length() && i < t.length(); i++) {
    		if(s.charAt(i) - t.charAt(i) != 0) {
    			break;
    		}
    		diffIndex = i;
    	}

    	int removedChars = s.length() - diffIndex - 1;
    	int addChars = t.length() - diffIndex - 1;
    	
    	int minOp = removedChars + addChars;
    	int remainedOp = k - minOp;
    	
    	if(s.length() >= t.length()) {
    		if(minOp <= k) {
        		return "Yes";
        	} else {
        		return "No";
        	}
    	} else {
    		if(remainedOp % 2 == 0 || (remainedOp == k && k/2 >= s.length())) {
        		return "Yes";
        	} else {
        		return "No";
        	}
    	}
    	
    	

    }
    
    /*
     * 
     *     public static boolean solve(char[] s, char[] t, int k) {
        // We have more operations than we need to delete and rewrite the string
        if (s.length + t.length < k) {
            return true;
        }
        
        // Iterate through string matching characters
        int i = -1;
        while(i++ < Math.min(s.length, t.length) - 1) {
            if (s[i] != t[i]) {
                break;
            }
        }
        
        // The strings are the same
        if (i == s.length && s.length == t.length) {
            // if k is odd, there will always be 1 operation left over
            // else, you can delete and re-append last character to use up k operations
            return ((k & 1) == 1) ? false : true;
        }

        // Else
        // Reduce k by number of necessary deletions and insertions
        k = k - (s.length - i) - (t.length - i);

        // If k < 0 or there is an odd number of operations left over, false
        // else we need exactly k operations or the number of extra ops is even, true
        return (k < 0 || (k & 1) == 1) ? false : true;
        
    }
    
    int commonLength = 0;
        
        for (int i=0; i<java.lang.Math.min(s.length(),t.length());i++){
            if (s.charAt(i)==t.charAt(i))
                commonLength++;
            else
                break;
        }
//CASE A
        if((s.length()+t.length()-2*commonLength)>k){
            System.out.println("No");
        }
//CASE B
        else if((s.length()+t.length()-2*commonLength)%2==k%2){
            System.out.println("Yes");
        }
//CASE C
        else if((s.length()+t.length()-k)<0){
            System.out.println("Yes");
        }
//CASE D
        else{
            System.out.println("No");
        }
     */

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String s = scanner.nextLine();

        String t = scanner.nextLine();

        int k = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        String result = appendAndDelete(s, t, k);
        System.out.println(result);

//        bufferedWriter.write(result);
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

        scanner.close();
    }

}
