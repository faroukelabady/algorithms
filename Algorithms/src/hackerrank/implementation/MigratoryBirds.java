package hackerrank.implementation;

import java.util.Arrays;
import java.util.Scanner;

public class MigratoryBirds {

	static int migratoryBirds(int n, int[] ar) {
		// Complete this function
		int birds[] = new int[6];
		Arrays.stream(ar).forEach(it -> birds[it]++);
		
		int type = 1;
		int sum = birds[1];
		
		for(int i = 1; i < birds.length; i++) {
			if(birds[i] > sum) {
				sum = birds[i];
				type = i;
			}
		}
		
		return type;
		
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] ar = new int[n];
		for (int ar_i = 0; ar_i < n; ar_i++) {
			ar[ar_i] = in.nextInt();
		}
		int result = migratoryBirds(n, ar);
		System.out.println(result);
	}

}
