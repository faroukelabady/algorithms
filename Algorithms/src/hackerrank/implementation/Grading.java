package hackerrank.implementation;

import java.util.Scanner;

public class Grading {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int[] grades = new int[n];
		for (int grades_i = 0; grades_i < n; grades_i++) {
			grades[grades_i] = in.nextInt();
			Solve(grades, grades_i);
		}
		for (int i = 0; i < grades.length; i++) {
			System.out.print(grades[i] + (i != grades.length - 1 ? "\n" : ""));
		}
		System.out.println("");
		
		// System.out.println(grade < 38 || grade % 5 < 3 ? grade : grade + (5 - (grade % 5)));
		// Arrays.stream(new int[n]).map(i-> in.nextInt()).map(g -> (g >= 38 && g % 5 >=3) ? g+5-g%5 : g).forEach(System.out::println);

	}
	
	static void Solve(int[] grades, int index) {
		if (grades[index] >= 38 && grades[index] % 5 != 0) {
			int gradePlusFive = grades[index] + 5; 
			int amountToSub = gradePlusFive % 5;
			int nextMultipleOfFive = gradePlusFive - amountToSub;
			if (nextMultipleOfFive - grades[index] < 3) {
				grades[index] = nextMultipleOfFive;
			}
		}
	}

}
