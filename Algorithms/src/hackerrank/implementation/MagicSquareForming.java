package hackerrank.implementation;

import java.util.Scanner;

public class MagicSquareForming {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

		// 4 9 2 | 2 7 6 | 6 1 8 | 8 3 4
		// 3 5 7 | 9 5 1 | 7 5 3 | 1 5 9
		// 8 1 6 | 4 3 8 | 2 9 4 | 6 7 2

		Scanner in = new Scanner(System.in);
		int[][] s = new int[3][3];
		int[][] m1 = {{4,9,2},
					  {3,5,7},
					  {8,1,6}};
		
		int[][] m2 = {{2,7,6},
					  {9,5,1},
					  {4,3,8}};
		
		int[][] m3 = {{6,1,8},
					  {7,5,3},
					  {2,9,4}};
		
		int[][] m4 = {{8,3,4},
				      {1,5,9},
				      {6,7,2}};
		
		// 4 3 8 | 8 1 6 | 6 7 2 | 2 9 4
		// 9 5 1 | 3 5 7 | 1 5 9 | 7 5 3
		// 2 7 6 | 4 9 2 | 8 3 4 | 6 1 8
		
		int[][] m5 = {{4,3,8},
				      {9,5,1},
				      {2,7,6}};
		
		int[][] m6 = {{8,1,6},
				      {3,5,7},
				      {4,9,2}};
		
		int[][] m7 = {{6,7,2}, 
				      {1,5,9},
				      {8,3,4}};
		
		int[][] m8 = {{2,9,4},
				      {7,5,3},
				      {6,1,8}};
		
		int c1 = 0,c2 = 0,c3 = 0,c4 =0;
		int c5 = 0,c6 = 0,c7 = 0,c8=0;
		for (int s_i = 0; s_i < 3; s_i++) {
			for (int s_j = 0; s_j < 3; s_j++) {
				int value = in.nextInt();
				c1 += Math.abs(m1[s_i][s_j] - value);
				c2 += Math.abs(m2[s_i][s_j] - value);
				c3 += Math.abs(m3[s_i][s_j] - value);
				c4 += Math.abs(m4[s_i][s_j] - value);
				
				c5 += Math.abs(m5[s_i][s_j] - value);
				c6 += Math.abs(m6[s_i][s_j] - value);
				c7 += Math.abs(m7[s_i][s_j] - value);
				c8 += Math.abs(m8[s_i][s_j] - value);
			}
		}
		int min1 = Math.min(Math.min(Math.min(c1, c2), c3), c4);
		int min2 = Math.min(Math.min(Math.min(c5, c6), c7), c8);
		System.out.println(Math.min(min1, min2));

		// Print the minimum cost of converting 's' into a magic square

	}

}
