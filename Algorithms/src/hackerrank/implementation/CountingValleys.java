package hackerrank.implementation;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;

public class CountingValleys {

	public static void main(String[] args) throws NumberFormatException, IOException {
		BufferedReader sc = new BufferedReader(new InputStreamReader(System.in));
		int n = Integer.parseInt(sc.readLine());
		/*
		 *  if(c == 'U') {
                if(altitude == -1) {
                    count++;
                }
                altitude++;
            }
		 */
		int step = 0;
		boolean startVally = false;
		int countValley = 0;
		char[] data = sc.readLine().toCharArray();
		for (int i = 0; i < n; i++) {
			char c = data[i];
			if (c == 'U') {
				step++;
			} else if (c == 'D') {
				step--;
			}

			if (step < 0) {
				startVally = true;
			} else if (step >= 0 && startVally) {
				startVally = false;
				countValley++;
			}

		}
		sc.close();

		System.out.println(countValley);

	}

}
