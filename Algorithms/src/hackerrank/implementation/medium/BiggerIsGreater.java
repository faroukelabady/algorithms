package hackerrank.implementation.medium;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class BiggerIsGreater {
	static String biggerIsGreater(String w) {
		StringBuilder result = new StringBuilder();
		int minValue = Integer.MAX_VALUE;
		char firstOtherChar = '\u0000';
		int otherCharIndex = -1;
		int currentIndex = -1;
		for (int i = 0; i < w.length(); i++) {
			char startChar = w.charAt(i);
			for (int j = i; j < w.length(); j++) {
				char current = w.charAt(j);
				if (current - startChar > 0 && (current - startChar) < minValue) {
					firstOtherChar = current;
					otherCharIndex = j;
					currentIndex = i;
				}
			}

		}

		if (otherCharIndex == -1) {
			return "no answer";
		}
		result.append(w.subSequence(0, currentIndex));
		result.append(firstOtherChar);
		char[] rest = new char[w.length() - currentIndex - 1];
		int j = 0;
		for (int i = currentIndex; i < w.length(); i++) {
			if (i != otherCharIndex) {
				rest[j] = w.charAt(i);
				j++;
			}
		}
		Arrays.sort(rest);
		result.append(rest);

		return result.toString();

	}

	static String biggerIsGreater2(String word) {
		if (word.length() == 1) {
			return "no answer";
		}

		int maxLexoC1 = 0; // The max lexocographical according to condition 1
		int maxLexoC2 = 0; // The max lexocographical according to condition 2

		// Find the largest index char that is weakly increasing such as g in hefg
		for (int j = 1; j < word.length(); j++) {
			boolean condition1 = word.charAt(j) > word.charAt(j - 1);

			if (condition1) {
				maxLexoC1 = (j > maxLexoC1) ? j : maxLexoC1;
			}
		}

		// if our only increasing is at point 0 then we are in the last permuation of
		// our string
		if (maxLexoC1 == 0) {
			return "no answer";
		}

		// maxLexoC2
		// Determine the right most char greater than the pivot in index and in lexo
		for (int j = maxLexoC1; j < word.length(); j++) {
			boolean condition2 = word.charAt(j) > word.charAt(maxLexoC1 - 1);

			if (condition2) {
				maxLexoC2 = j;
			}
		}

		StringBuilder wordSB = new StringBuilder(word);

		// Swap the pivot with maxLexoC2
		char tmp = wordSB.charAt(maxLexoC1 - 1);
		wordSB.setCharAt(maxLexoC1 - 1, wordSB.charAt(maxLexoC2));
		wordSB.setCharAt(maxLexoC2, tmp);

		// Reverse starting at the element to the right of the pivot
		int left = maxLexoC1;
		int right = wordSB.length() - 1;
		while (left < right) {
			// swap left with right
			tmp = wordSB.charAt(left);
			wordSB.setCharAt(left, wordSB.charAt(right));
			wordSB.setCharAt(right, tmp);
			left++;
			right--;
		}

		return wordSB.toString();

	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		int T = Integer.parseInt(bufferedReader.readLine());

		for (int TItr = 0; TItr < T; TItr++) {
			String w = bufferedReader.readLine();

			String result = biggerIsGreater2(w);

			System.out.println(result);
		}

		bufferedReader.close();

	}
}
