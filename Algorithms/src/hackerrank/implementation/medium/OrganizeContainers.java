package hackerrank.implementation.medium;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class OrganizeContainers {

	// Complete the organizingContainers function below.
	static String organizingContainers(int[][] container) {

		int n = container.length;
		int a[] = new int[n];
		int b[] = new int[n];

		for (int i = 0; i < n; i++) {
			for (int j = 0; j < n; j++) {
				a[i] += container[i][j];
				b[j] += container[i][j];
			}
		}

		String result = "Possible";
		for (int i = 0; i < n; i++) {
			int j = 0;
			for (j = i; j < n; j++) {
				if (a[i] == b[j]) {
					int temp = b[j];
					b[j] = b[i];
					b[i] = temp;
					break;
				}
			}
			if (j == n) {
				result = "Impossible";
				break;
			}
		}

		return result;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		int q = Integer.parseInt(bufferedReader.readLine());

		for (int qItr = 0; qItr < q; qItr++) {
			int n = Integer.parseInt(bufferedReader.readLine());

			int[][] container = new int[n][n];

			for (int i = 0; i < n; i++) {
				String[] containerRowItems = bufferedReader.readLine().split(" ");

				for (int j = 0; j < n; j++) {
					int containerItem = Integer.parseInt(containerRowItems[j]);
					container[i][j] = containerItem;
				}
			}

			String result = organizingContainers(container);

			System.out.println(result);
		}

		bufferedReader.close();

		scanner.close();
	}
}
