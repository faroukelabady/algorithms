package hackerrank.implementation.medium;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

public class TimeInWords {

	/**
	 * @param h
	 * @param m
	 * @return
	 */
	static String timeInWords(int h, int m) {
		String[] hours = new String[] {"", "one", "two", "three", "four", "five", "six", "seven", "eight", "nine", "ten", "eleven", "twelve"};
		String[] oneToThirty = new String[] {"", "one minute", "two minutes", "three minutes", "four minutes", 
				"five minutes", "six minutes", "seven minutes", "eight minutes", "nine minutes", 
				"ten minutes", "eleven minutes", "twelve minutes", "thirteen minutes", "fourteen minutes" , "quarter",
				"sixteen minutes", "seventeen minutes", "eighteen minutes", "ninteen minutes", "twenty minutes", 
				"twenty one minutes", "twenty two minutes", "twenty three minutes", "twenty four minutes", "twenty five minutes", 
				"twenty six minutes", "twenty seven minutes", "twenty eight minutes", "twenty nine minutes", "half" };
		
		String[] afterThirty = new String[] {};
		
		if (m == 0) {
			return hours[h] + " o' clock"; 
		} else if (m >= 1 && m <= 30) {
			return oneToThirty[m] + " past " + hours[h];
		} else {
			return oneToThirty[60 - m] + " to " + hours[h + 1 > 12 ? 1 : h + 1];
		}
	}

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		int h = Integer.valueOf(bufferedReader.readLine());

		int m = Integer.valueOf(bufferedReader.readLine());

		String result = timeInWords(h, m);

		System.out.println(result);

		bufferedReader.close();

	}

}
