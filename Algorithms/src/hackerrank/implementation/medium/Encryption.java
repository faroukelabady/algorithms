package hackerrank.implementation.medium;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class Encryption {
	static String encryption(String s) {
		
		int n = s.length();
		String trimmer = s.replaceAll(" ", "");
		int total = trimmer.length();
		int row = (int) Math.floor(Math.sqrt(total));
		int column = (int) Math.ceil(Math.sqrt(total));
		
		if(row * column < total) {
			row += 1;
		}
		char[][] mat = new char[row][column];
		int index = 0;
		for(int i = 0; i < row; i++) {
			for(int j = 0; j< column; j++) {
				mat[i][j] = trimmer.charAt(index);
				index++;
				if(index >= total) {
					break;
				}
			}
		}
		for(int j = 0; j< row; j++) {
			System.out.println(Arrays.toString(mat[j]));
		}
		
		String  result = ""; 
		
		for(int i = 0; i < column; i++) {
			for(int j = 0; j< row; j++) {
				if(mat[j][i] != '\u0000') {
					result = result.concat(mat[j][i] + "");
				}
				
			}
			if( i < column - 1) {
				result = result.concat(" ");
			}
			
		}
		
//		for (int i = 0; i < c; i++) {
//	        for (int j = 0; j < r; j++)
//	            if (j * c + i < len)
//	                cout << str[j * c + i]; 
		
//		 for(int j=0;j<column;++j) {
//		        for(int i=j; i<n;i+=column)cout << s[i];
//		        cout << ' ';
//		    }
		
		
		
		return result;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		String result = encryption(bufferedReader.readLine());

		System.out.println(result);

		bufferedReader.close();

	}
}
