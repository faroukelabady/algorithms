package hackerrank.implementation.medium;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class QueenAttack2 {

	static int queensAttack(int n, int k, int r_q, int c_q, int[][] obstacles) {
		
		int right = Math.abs(n - c_q);
		int left = Math.abs(1 - c_q);
		int up = Math.abs(n - r_q);
		int down = Math.abs(1 - r_q);
		
		int stepsUpRight = Math.min(Math.abs(n - r_q), Math.abs(n - c_q));
		int stepsDownLeft = Math.min(Math.abs(1 - r_q), Math.abs(1 - c_q));
		
		int stepsUpLeft = Math.min(Math.abs(n - r_q), Math.abs(1 - c_q));
		int stepsDownRight = Math.min(Math.abs(1 - r_q), Math.abs(n - c_q));
		
		int rowTotal = right + left;
		int columnTotal = up + down;
		
		for ( int i = 0 ; i < k; i++) {
			int oRow = obstacles[i][0];
			int oColumn = obstacles[i][1];
			if(oRow == r_q) {
				if (oColumn < c_q) {
					rowTotal =Math.min(rowTotal ,  rowTotal -  (Math.abs(oColumn - 1) + 1));
				} else if (oColumn > c_q) {
					rowTotal = Math.min(rowTotal , rowTotal - (Math.abs(oColumn - n) + 1));
				}
			} else if(oColumn == c_q) {
				if (oRow < r_q) {
					columnTotal = Math.min(columnTotal, columnTotal - ( Math.abs(oRow - 1) + 1));
				} else if (oRow > r_q) {
					columnTotal = Math.min(columnTotal, columnTotal - (Math.abs(oRow - n) + 1));
				}
			} else if (Math.abs(oColumn - c_q) == Math.abs(oRow - r_q)) {
				if(oRow > r_q) {
					if (oColumn < c_q) {
						stepsUpLeft =Math.min(stepsUpLeft, (Math.abs(oColumn - c_q) - 1));
					} else if (oColumn > c_q) {
						stepsUpRight = Math.min(stepsUpRight, (Math.abs(oColumn - c_q) - 1)) ;
					}
				} else if (oRow < r_q) {
					if (oColumn < c_q) {
						stepsDownLeft =Math.min(stepsDownLeft,  (Math.abs(oColumn - c_q)  - 1));
					} else if (oColumn > c_q) {
						stepsDownRight =Math.min(stepsDownRight, ( Math.abs(oColumn - c_q) - 1));
					}
				}
			}
		}
		
		int slope1Total = stepsUpRight + stepsDownLeft;
		int slope2Total = stepsUpLeft + stepsDownRight;
		
		return rowTotal + columnTotal + slope1Total + slope2Total;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		String[] nk = bufferedReader.readLine().split(" ");

		int n = Integer.parseInt(nk[0]);

		int k = Integer.parseInt(nk[1]);

		String[] r_qC_q = bufferedReader.readLine().split(" ");

		int r_q = Integer.parseInt(r_qC_q[0]);

		int c_q = Integer.parseInt(r_qC_q[1]);

		int[][] obstacles = new int[k][2];

		for (int i = 0; i < k; i++) {
			String[] obstaclesRowItems = bufferedReader.readLine().split(" ");
			//bufferedReader.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

			for (int j = 0; j < 2; j++) {
				int obstaclesItem = Integer.parseInt(obstaclesRowItems[j]);
				obstacles[i][j] = obstaclesItem;
			}
		}

		int result = queensAttack(n, k, r_q, c_q, obstacles);
		System.out.println(result);

		bufferedReader.close();
	}

}
