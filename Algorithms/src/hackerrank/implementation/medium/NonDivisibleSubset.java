package hackerrank.implementation.medium;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedHashSet;
import java.util.Scanner;

public class NonDivisibleSubset {

	// Complete the nonDivisibleSubset function below.
	static int nonDivisibleSubset(int k, int[] S) {

		int count = 0;
		int[] counts = new int[k];
		
		for(int i: S) {
			counts[i%k]++;
		}
		
		count = Math.min(counts[0], 1);
		for(int i = 1; i < (int)Math.floor(k/2) + 1; i++) {
			if(i != k-i)
				count += Math.max(counts[i], counts[k-i]);
		}
		
		if(k%2 == 0)
			count++;

		return count;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		String[] nk = scanner.nextLine().split(" ");

		int n = Integer.parseInt(nk[0]);

		int k = Integer.parseInt(nk[1]);

		int[] S = new int[n];

		String[] SItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int SItem = Integer.parseInt(SItems[i]);
			S[i] = SItem;
		}

		int result = nonDivisibleSubset(k, S);
		System.out.println(String.valueOf(result));
//        bufferedWriter.write(String.valueOf(result));
//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

		scanner.close();
	}

	
	/*
	 * 
	 * values.add(new LinkedHashSet<>());
			
			for (int j = i + 1; j < S.length; j++) {
				int s = S[i] + S[j];
				if(s % k != 0) {
					if(!processed.contains(S[i])) {
						values.get(i).add(S[i]);
					}
					
					if(!processed.contains(S[j])) {
						values.get(i).add(S[j]);
					}
					
					
				}
			}
			LinkedHashSet<Integer> current = values.get(i);
			Iterator<Integer> it1 = current.iterator();
			while(it1.hasNext()) {
				Integer v = it1.next();
				Iterator<Integer> it2 = current.iterator();
				while(it2.hasNext()) {
					Integer v2 = it2.next();
					if((v + v2) % k == 0 && v != v2) {
						it1.remove();
						break;
					}
				}
			}
			
			processed.addAll(current);
			
			if(max < current.size()) {
				max = current.size();
			}
			
	 */
}
