package hackerrank.implementation;

import java.math.BigDecimal;
import java.util.Scanner;

public class ExtraLongFactorial {
	
	// Complete the extraLongFactorials function below.
    static void extraLongFactorials(int n) {
    	
    	BigDecimal result = new BigDecimal(0);
    	
    	for(int i = 1; i<=n;i++) {
    		result = result.multiply(new BigDecimal(i));
    	}

    	System.out.println(result.toString());

    }
    /*
     * int main() {
    int val;
    int carry = 0;
    cin >> val;
    vector <int> arr(200, 0);
    arr[0] = 1; //Initial product = 1

    int k = 0; //Current size of the number stored in arr

    for(int i = 1; i <= val; i++) {
        for(int j = 0;j <= k; j++) {
            arr[j] = arr[j] * i + carry;
            carry = arr[j] / 10;
            arr[j] = arr[j] % 10;
        }
        while(carry) { //Propogate the remaining carry to higher order digits
            k++;
            arr[k] = carry % 10;
            carry /= 10;
        }   
    }
    for(int i = k; i >= 0; i--) {
        cout << arr[i];
    }
    cout << endl;
    return 0;
}
     */
    
    static BigDecimal recurisveFactorial(BigDecimal n) {
    	if(n.equals(new BigDecimal(1))) {
    		return new BigDecimal(1).multiply(n);
    	}
    	
    	return recurisveFactorial(n.multiply(n.subtract(new BigDecimal(1))));
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        extraLongFactorials(n);

        scanner.close();
    }

}
