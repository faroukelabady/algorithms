package hackerrank.implementation;

import java.util.Scanner;

public class DrawingBook {

	static int solve(int n, int p) {
		// Complete this function
		int pageTurns = n / 2;
		int index =p / 2;
		
		return index > (pageTurns - index)? pageTurns - index: index;
	}

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
		int n = in.nextInt();
		int p = in.nextInt();
		int result = solve(n, p);
		System.out.println(result);
	}

}
