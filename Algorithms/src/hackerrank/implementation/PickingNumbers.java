package hackerrank.implementation;

import java.util.Arrays;
import java.util.Scanner;

public class PickingNumbers {

	public static void main(String[] args) {
		Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] a = new int[n];
        for(int a_i=0; a_i < n; a_i++){
            a[a_i] = in.nextInt();
        }
        
        Arrays.sort(a);
        
        int max = -1;
        boolean start = false;
        int startIndex = -1;
        for(int i = 1; i < n; i++) {
        	if( a[i] - a[i - 1] <= 1 ) {
        		start = true;
        		if(startIndex != -1 && a[i] - a[startIndex] > 1) {
        		  start = false;
        		  max = Math.max(max, i - startIndex);
        		  startIndex = -1;
        		} else if(startIndex == -1 ) {
        		  startIndex = i - 1;
        		}
        	}
        	
        	if(a[i] - a[i - 1] > 1 && start) {
        		start = false;
        		max = Math.max(max, i - startIndex);
        		startIndex = -1;
        	}
        	
        }
        
        if(max == -1)
        	max = n;
      
        
        System.out.println(max);

	}
	
	/*
	 * 
	 * int main() {
   int n,k,max = 0;
    cin>>n;
    int a[100]={0};
    for(int i = 0;i<n;i++){
        cin>>k;
        a[k]++;
    }
    for(int i = 0;i<n-1;i++){
        if(a[i]+a[i+1]> max) max = a[i]+a[i+1];
    }
    cout<<max;
    return 0;
}
	 */

}
