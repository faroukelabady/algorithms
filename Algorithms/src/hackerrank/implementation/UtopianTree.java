package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class UtopianTree {

	static int utopianTree(int n) {
		int H = 1;
		
		for(int i = 1; i <= n; i++) {
			if((i & 1) == 1) {
				H *= 2;
			} else {
				H += 1;
			}
			
		}
		return H;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//	        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
		
		// (1<<((N>>1)+1))-1
		int n = (10 >> 1);
		System.out.println(Integer.toBinaryString(n));
		int l = ~1;
		System.out.println(Integer.toBinaryString(l));
		int k = ~(~1 << n);
		System.out.println(Integer.toBinaryString(k));
		// ((1<<((N>>1)+1))-1) << n%2
		System.out.println(~(~1<<(10>>1)) << 10%2);

//		int t = scanner.nextInt();
//		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//		for (int tItr = 0; tItr < t; tItr++) {
//			int n = scanner.nextInt();
//			scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");
//
//			int result = utopianTree(n);
//			System.out.println(String.valueOf(result));
//
////	            bufferedWriter.write(String.valueOf(result));
////	            bufferedWriter.newLine();
//		}

//	        bufferedWriter.close();

		scanner.close();
	}

}
