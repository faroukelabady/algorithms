package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class PermutationEquation {

	// Complete the permutationEquation function below.
	static int[] permutationEquation(int[] p) {

		int[] pr = new int[p.length];
		for (int x = 1; x < p.length; x++) {
			pr[p[p[x]]] = x;
		}
		
		
		return pr;

	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		int n = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		int[] p = new int[n + 1];

		String[] pItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 1; i <= n; i++) {
			int pItem = Integer.parseInt(pItems[i - 1]);
			p[i] = pItem;
		}

		int[] result = permutationEquation(p);

		for (int i = 1; i < result.length; i++) {
			System.out.println(String.valueOf(result[i]));

		}

//        bufferedWriter.newLine();
//
//        bufferedWriter.close();

		scanner.close();
	}

}
