package hackerrank.implementation;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class BeautifulDay {

	// Complete the beautifulDays function below.
	static int beautifulDays(int i, int j, int k) {

		int count = 0;
		for (int n = i; n <= j; n++) {
			int r = reverse(n);
			int d = Math.abs(r -n);
			if(d % k == 0) {
				count++;
			}
		}
		return count;
	}

	/*
	 * bool isOk(int x, int mod) {
    int n = x;
    int m = 0;
    while (x > 0) {
        m = m * 10 + x % 10;
        x /= 10;
    }
    int delta = abs(n - m);
    delta %= mod;
    return (delta == 0);
}

	 */
	
	static int reverse(int source) {
		int n = source;
		int r = 0;
		while(n > 0) {
			r = r * 10 + n % 10;
			n /= 10;
		}
		return r;
	}
//	static Integer reverse(Integer source) {
//		String result = "";
//		String sourceStr = Integer.toString(source);
//		for (int i = sourceStr.length() - 1; i >= 0; i--) {
//			result += sourceStr.charAt(i);
//		}
//
//		return Integer.valueOf(result);
//	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
		String[] ijk = scanner.nextLine().split(" ");

		int i = Integer.parseInt(ijk[0]);

		int j = Integer.parseInt(ijk[1]);

		int k = Integer.parseInt(ijk[2]);

		int result = beautifulDays(i, j, k);

		bufferedWriter.write(String.valueOf(result));
		bufferedWriter.newLine();

		bufferedWriter.close();

		scanner.close();
	}
}
