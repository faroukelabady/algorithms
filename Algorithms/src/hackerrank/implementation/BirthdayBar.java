package hackerrank.implementation;

import java.util.Scanner;

public class BirthdayBar {

	static int solve(int n, int[] s, int d, int m){
        // Complete this function
		// possible solution is to get the sum of all the values
		// together then subtract the previous sum 
		if(n < m)
			return 0;
		
		int count = 0;
		int sum = 0;
		
		for(int j = 0; j < m; j++) {
			sum += s[j];
		}
		
		if(sum == d) {
			count++;
		}
			
		for(int i = m; i < n; i++) {
			sum = sum - s[i - m] + s[i];
			if(sum == d) {
				count++;
			}
		}
		
		return count;
		
		
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt();
        int[] s = new int[n];
        for(int s_i=0; s_i < n; s_i++){
            s[s_i] = in.nextInt();
        }
        int d = in.nextInt();
        int m = in.nextInt();
        int result = solve(n, s, d, m);
        System.out.println(result);
    }

}
