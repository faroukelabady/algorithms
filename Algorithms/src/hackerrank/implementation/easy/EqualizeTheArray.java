package hackerrank.implementation.easy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class EqualizeTheArray {
	
    // Complete the equalizeArray function below.
    static int equalizeArray(int[] arr) {
    	int[] counter = new int[101];
    	// can make mostcount here and compare each time we add to the array;
    	for(int i = 0 ; i < arr.length; i++) {
    		counter[arr[i]]++;
    	}
    	int mostEqualNumber = Arrays.stream(counter).sorted().toArray()[100];    	
    	return arr.length - mostEqualNumber;

    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int n = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int result = equalizeArray(arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

}
