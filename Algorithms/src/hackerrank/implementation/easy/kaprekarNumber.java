package hackerrank.implementation.easy;

import java.util.Scanner;

public class kaprekarNumber {
	
	
	 // Complete the kaprekarNumbers function below.
    static void kaprekarNumbers(int p, int q) {
    	
    	String finalResut = "";
    	for( int i = p ; i <= q; i++) {
    		long doubleValue = (long)i * i;
        	StringBuilder value = new StringBuilder();
        	value.append(doubleValue);
        	if(value.length() == 1) {
        		value.insert(0, '0');
        	}
        	int middle = value.length() / 2;
        	String firstHalf = value.substring(0, middle );
        	String secondHalf = value.substring(middle, value.length());
        	int val1 = Integer.valueOf(firstHalf);
        	int val2 = Integer.valueOf(secondHalf);
        	int result = val1 + val2;
        	if (i == result) {
        		finalResut += i + " ";
        	}
    		
    	}
    	if(finalResut.equals("")) {
    		System.out.println("INVALID RANGE");
    	} else {
    		System.out.println(finalResut.trim());
    	}
    	
    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) {
        int p = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        int q = scanner.nextInt();
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        kaprekarNumbers(p, q);

        scanner.close();
    }

}
