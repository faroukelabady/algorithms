package hackerrank.implementation.easy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class JumpingOnClouds {

	static int jumpingOnClouds(int[] c) {
		
		int i = 0;
		int count = 0;
		while(i < c.length - 1) {
			int singleStep = c[i+1];
			int doubleStep = (i+2) >= c.length? 1 : c[i+2];
			if (doubleStep == 0) {
				count++;
				i += 2;
			} else if (singleStep == 0){
				count++;
				i += 1;
			} else {
				i++;
			}
		}
		
//		 int count = -1;
//	        for (int i = 0; i < n; i++, count++) {
//	            if (i<n-2 && c[i+2]==0) i++;
//	        }
		
		return count;

	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		int n = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		int[] c = new int[n];

		String[] cItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int cItem = Integer.parseInt(cItems[i]);
			c[i] = cItem;
		}

		int result = jumpingOnClouds(c);

		bufferedWriter.write(String.valueOf(result));
		bufferedWriter.newLine();

		bufferedWriter.close();

		scanner.close();
	}

}
