package hackerrank.implementation.easy;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.IntStream;

public class ChocolateFeast {
	
	  public static int chocolateFeast(int n, int c, int m) {
	    	int initialBars = n / c;
	    	int reminder = 0;
	    	int freeBars = initialBars;
	    	int sum = initialBars;
	    	do {
	    		reminder = freeBars % m;
	    		freeBars = freeBars / m;
	    		sum += freeBars;
	    		if (freeBars > 0)
	    			freeBars += reminder;	    		
	    	} while(reminder > 0 || freeBars > 1);
	    	return sum;
	    }
	
	
    public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
//        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));
        List<String> results = new ArrayList<>();
        int t = Integer.parseInt(bufferedReader.readLine().trim());

        IntStream.range(0, t).forEach(tItr -> {
            try {
                String[] firstMultipleInput = bufferedReader.readLine().replaceAll("\\s+$", "").split(" ");

                int n = Integer.parseInt(firstMultipleInput[0]);

                int c = Integer.parseInt(firstMultipleInput[1]);

                int m = Integer.parseInt(firstMultipleInput[2]);

                int result = chocolateFeast(n, c, m);
                results.add(String.valueOf(result));
//                bufferedWriter.write(String.valueOf(result));
//                bufferedWriter.newLine();
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        });
        results.stream().forEach(System.out::println);

        bufferedReader.close();
       // bufferedWriter.close();
    }

}
