package hackerrank.implementation.easy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class HowManyGames {

	static int howManyGames(int p, int d, int m, int s) {
		// Return the number of games you can buy
		int budget = s;
		int currentPrice = p;
		int discount = d;
		int minPrice = m;
		int count = 0;
		while( budget > 0) {
			budget  = budget - currentPrice;
			currentPrice = currentPrice - discount;
			if ( currentPrice <= minPrice) {
				currentPrice = minPrice;
			}
			if( budget >= 0) {
				count++;
			} else {
				break;
			}
		}
		
		/*
		 * int games = 0;
		    while (s >= p) {
		        s -= p;
		        games++;
		        p = Math.max(p - d, m);
		    }
		    return games;
		 */
		return count;

	}


	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		String[] pdms = bufferedReader.readLine().split(" ");

		int p = Integer.parseInt(pdms[0]);

		int d = Integer.parseInt(pdms[1]);

		int m = Integer.parseInt(pdms[2]);

		int s = Integer.parseInt(pdms[3]);

		int answer = howManyGames(p, d, m, s);

		System.out.println(answer);

		bufferedReader.close();
	}
}
