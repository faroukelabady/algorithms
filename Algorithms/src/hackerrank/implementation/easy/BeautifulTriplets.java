package hackerrank.implementation.easy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Scanner;

public class BeautifulTriplets {
	
	static int beautifulTriplets(int d, int[] arr) {
		
		int count = 0;
		
		for(int i = 0; i < arr.length; i++) {
			for (int j = i + 1 ; j < arr.length; j ++) {
				int value = arr[j] - arr[i];
				if (value == d) {
					for (int k = j + 1; k < arr.length; k++) {
						int value2 = arr[k] - arr[j];
						if(value2 == d) {
							count++;
						}
						
					}
				}
				
			}
		}
		
		return count;


    }

    private static final Scanner scanner = new Scanner(System.in);

    public static void main(String[] args) throws IOException {
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        String[] nd = scanner.nextLine().split(" ");

        int n = Integer.parseInt(nd[0]);

        int d = Integer.parseInt(nd[1]);

        int[] arr = new int[n];

        String[] arrItems = scanner.nextLine().split(" ");
        scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

        for (int i = 0; i < n; i++) {
            int arrItem = Integer.parseInt(arrItems[i]);
            arr[i] = arrItem;
        }

        int result = beautifulTriplets(d, arr);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedWriter.close();

        scanner.close();
    }

}
