package hackerrank.implementation.easy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.BitSet;
import java.util.Scanner;

public class AcmIcpcTeam {

	static int[] acmTeam(String[] topic) {
		int maxTopics = Integer.MIN_VALUE;
		int count = 0;
		for (int i = 0; i < topic.length; i++) {
			for (int j = i+1; j < topic.length; j++) {
				int sum = sumTopics(topic[i], topic[j]);
				if ( sum > maxTopics) {
					maxTopics = sum;
					count = 1;
				} else if ( sum == maxTopics) {
					count++;
				}
			}
		}
		
		return new int[]{maxTopics, count};

	}

	private static int sumTopics(String topic1, String topic2) {
		int sum = 0;
		for (int i = 0; i < topic1.length(); i++) {
			int bit1 = topic1.charAt(i) - '0';
			int bit2 = topic2.charAt(i) - '0';
			sum += bit1 | bit2;

		}
		return sum;
	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		String[] nm = bufferedReader.readLine().split(" ");

		int n = Integer.parseInt(nm[0]);

		int m = Integer.parseInt(nm[1]);

		String[] topic = new String[n];

		for (int i = 0; i < n; i++) {
			String topicItem = bufferedReader.readLine();
			topic[i] = topicItem;
		}

		int[] result = acmTeam(topic);

		for (int i = 0; i < result.length; i++) {
			System.out.println(String.valueOf(result[i]));
		}

		bufferedReader.close();

		scanner.close();
	}
}
