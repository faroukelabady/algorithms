package hackerrank.implementation.easy;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.Scanner;

public class FlatlandSpaceStations {

	static int flatlandSpaceStations(int n, int[] c) {
		
		int max = Integer.MIN_VALUE;
		Arrays.sort(c);
		for(int j = 0; j < c.length - 1; j++) {
			int value1 = Math.abs(c[j] - c[j+1]) / 2;
			max = Math.max(max, value1);
		}
		int start = 0;
		int end = n - 1;
		max = Math.max(max, c[0] - start);
		max = Math.max(max, end - c[c.length - 1]);
		if (n == c.length) {
			return 0;
		}
		return max;
	}


	public static void main(String[] args) throws IOException {
		BufferedReader bufferedReader = new BufferedReader(new FileReader("/home/farouk/data.txt"));

		String[] nm = bufferedReader.readLine().split(" ");

		int n = Integer.parseInt(nm[0]);

		int m = Integer.parseInt(nm[1]);

		int[] c = new int[m];

		String[] cItems = bufferedReader.readLine().split(" ");

		for (int i = 0; i < m; i++) {
			int cItem = Integer.parseInt(cItems[i]);
			c[i] = cItem;
		}

		int result = flatlandSpaceStations(n, c);
		System.out.println(result);


		bufferedReader.close();

	}

}
