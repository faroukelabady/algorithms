package hackerrank.implementation.easy;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

public class MinimumDistances {

	static int minimumDistances(int[] a) {
		
		Map<Integer, Integer> indexMap = new HashMap<>();
		int min = Integer.MAX_VALUE;
		for(int i = 0; i < a.length; i++) {
			if (!indexMap.containsKey(a[i])) {
				indexMap.put(a[i], i);
			} else {
				int index = indexMap.get(a[i]);
				if (i - index < min) {
					min = i - index;
				}
				indexMap.put(a[i], i);
			}
		}
		if(min == Integer.MAX_VALUE) {
			return -1;
		}
		return min;

	}

	private static final Scanner scanner = new Scanner(System.in);

	public static void main(String[] args) throws IOException {
		BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

		int n = scanner.nextInt();
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		int[] a = new int[n];

		String[] aItems = scanner.nextLine().split(" ");
		scanner.skip("(\r\n|[\n\r\u2028\u2029\u0085])?");

		for (int i = 0; i < n; i++) {
			int aItem = Integer.parseInt(aItems[i]);
			a[i] = aItem;
		}

		int result = minimumDistances(a);

		bufferedWriter.write(String.valueOf(result));
		bufferedWriter.newLine();

		bufferedWriter.close();

		scanner.close();
	}

}
