package hackerrank;

import java.io.BufferedReader;
import java.math.BigDecimal;

public class Factorial {
	
	
	public static void main(String[] args) {
		Integer value = Integer.parseInt(args[0]);
		
		BigDecimal fact = new BigDecimal("1");
		for(int i = value; i > 0; i--) {
			fact.multiply(BigDecimal.valueOf(i));
		}
		
		System.out.println(fact);
		
	}
	
	public BigDecimal factorial(long value) {
		if(value == 1)
			return new BigDecimal("1");
		
		return factorial(value -1).multiply(new BigDecimal(value));
	}

}
