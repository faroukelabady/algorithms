package hackerrank;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.IntStream;


public class CompetitiveProgramming {
	
	
	
	
	
	public static void main(String[] args) throws IOException {
        BufferedReader bufferedReader = new BufferedReader(new InputStreamReader(System.in));
        BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter(System.getenv("OUTPUT_PATH")));

        int k = Integer.parseInt(bufferedReader.readLine().trim());

        int scoresCount = Integer.parseInt(bufferedReader.readLine().trim());

        List<Integer> scores = IntStream.range(0, scoresCount).mapToObj(i -> {
            try {
                return bufferedReader.readLine().replaceAll("\\s+$", "");
            } catch (IOException ex) {
                throw new RuntimeException(ex);
            }
        })
            .map(String::trim)
            .map(Integer::parseInt)
            .collect(Collectors.toList());

        int result = Result.numPlayers(k, scores);

        bufferedWriter.write(String.valueOf(result));
        bufferedWriter.newLine();

        bufferedReader.close();
        bufferedWriter.close();
    }

}

class Result {

    /*
     * Complete the 'numPlayers' function below.
     *
     * The function is expected to return an INTEGER.
     * The function accepts following parameters:
     *  1. INTEGER k
     *  2. INTEGER_ARRAY scores
     */

    public static int numPlayers(int k, List<Integer> scores) {
    	
    	List<Integer> sortedScores = scores.stream().filter(v -> v > 0).sorted(Collections.reverseOrder()).collect(Collectors.toList());
    	
    	if(sortedScores.size() == 0) {
    		return 0;
    	}
    	
    	int currentRank = 0;
    	int currentScore = Integer.MAX_VALUE;
    	int numberOfplayers = 0;
    	
    	for(int i = 0; i < sortedScores.size(); i++) {
    		if(sortedScores.get(i) < currentScore) {
    			currentRank +=1;
    		}
    		
    		if ( k > 100 && k == (i + 1)) {
    			k = currentRank;
    		}
    		
    		if( currentScore <= k) {
    			numberOfplayers++;
    		}
    	}
    	
    	
    	return numberOfplayers;

    }

}
