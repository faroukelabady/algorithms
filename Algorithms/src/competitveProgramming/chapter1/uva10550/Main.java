package competitveProgramming.chapter1.uva10550;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		while (true) {
			StringTokenizer data = new StringTokenizer(scanner.nextLine());
			int a = new Integer(data.nextToken());
			int b = new Integer(data.nextToken());
			int c = new Integer(data.nextToken());
			int d = new Integer(data.nextToken());
			if (a == 0 & b == 0 & c == 0 & d == 0)
				break;
			int temp = b -a;
			int res1 = temp > 0? (40 - (temp)) : -temp ;
			temp = b - c;
			int res2 = temp > 0? (40 - (temp)) : -temp ;
			temp = d - c;
			int res3 = temp > 0? (40 - (temp)) : -temp ;
			int result = 3 * 360 + 9 * res1
					+ 9 * res2 + 9 * res3;
			System.out.println(result);
		}

	}
}
