/**
 * 
 */
package competitveProgramming.chapter1.uva494;

import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String line;
		while (scanner.hasNextLine()) {
			line = scanner.nextLine();
			Pattern p = Pattern.compile("[a-zA-z]+");
			Matcher match = p.matcher(line);
			int counter = 0;
			
			while(match.find())
				counter++;
			
			System.out.println(counter);
		}

	}

}
