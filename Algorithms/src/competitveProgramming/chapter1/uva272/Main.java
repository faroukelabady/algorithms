/**
 * 
 */
package competitveProgramming.chapter1.uva272;

import java.io.IOException;
import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	static String ReadLn(int maxLg) // utility function to read from stdin
	{
		byte lin[] = new byte[maxLg];
		int lg = 0, car = -1;
		String line = "";

		try {
			while (lg < maxLg) {
				car = System.in.read();
				if ((car < 0) || (car == '\n'))
					break;
				lin[lg++] += car;
			}
		} catch (IOException e) {
			return (null);
		}

		if ((car < 0) && (lg == 0))
			return (null); // eof
		return (new String(lin, 0, lg));
	}

	void Begin() {
		String line = Main.ReadLn(255);
		String result = "";
		boolean evenFlag = true;
		while(line != null && !line.isEmpty()) {
			for(int i =0; i < line.length();i++) {
				if(line.charAt(i) == '\u001a') {
					break;
				} 
				 if(line.charAt(i) == '\"') {  
                     if(evenFlag == true) {  
                          System.out.print("``");  
                          evenFlag = false;  
                          continue;  
                     } else {  
                          System.out.print("''");  
                          evenFlag = true;  
                          continue;  
                     }  
                }
				 System.out.print(line.charAt(i));  
			}
		}
		System.out.print("\n");
	}

	public static void main(String[] args) {
        Scanner scanner = new Scanner(System.in);  
        String line;  
        boolean open = true;  
        boolean end = false;  
        while(scanner.hasNextLine()) {  
             line = scanner.nextLine();  
             for(int i = 0; i < line.length(); i++) {  
                  if(line.charAt(i) == '\u001a'){  
                       end = true;  
                       break;  
                  }  
                  if(line.charAt(i) == '\"') {  
                       if(open == true) {  
             
                    	   System.out.print("``");  
                            open = false;  
                            continue;  
                       } else {  
                            System.out.print("''");  
                            open = true;  
                            continue;  
                       }  
                  }  
                  System.out.print(line.charAt(i));  
             }  
             System.out.print("\n");       
        }  
	}

}