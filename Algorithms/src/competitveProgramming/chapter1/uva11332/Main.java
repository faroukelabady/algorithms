package competitveProgramming.chapter1.uva11332;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String[] values = sc.nextLine().split("(?<=.)");
		while(true) {
			if(values.length == 1 && values[0].equals("0"))
				break;
			
			System.out.println(caluclateFun(values));
			values = sc.nextLine().split("(?<=.)");
		}

	}
	
	public static String caluclateFun(String[] values) {
		int result = 0;
		if(values.length == 1)
			return values[0];
		
		for(String value:values) 
			result += new Integer(value);
		
		return caluclateFun((""+result).split("(?<=.)"));
	}

}
