/**
 * 
 */
package competitveProgramming.chapter1.uva11044;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int i = new Integer(scanner.nextLine());
		while (i > 0) {
			StringTokenizer data = new StringTokenizer(scanner.nextLine());
			int w = new Integer(data.nextToken());
			int h = new Integer(data.nextToken());
			int result = (w/3)*(h/3);
			System.out.println(result);
			i--;
		}

	}

}
