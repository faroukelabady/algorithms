package competitveProgramming.chapter1.uva499;

import java.util.Scanner;
import java.util.TreeMap;

public class Main {

	/**
	 * @param args
	 */
	@SuppressWarnings("unchecked")
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		String line;
		while (scanner.hasNextLine()) {
			line = scanner.nextLine().trim();
			if(line.isEmpty()) continue;
			int[] letters = new int[53];
			
			for(int i = 0; i < line.length();i++) {
				if(line.charAt(i) > 64 && line.charAt(i) < 91) letters[line.charAt(i) - 65]++;
				else if(line.charAt(i) > 96 && line.charAt(i) < 123) letters[26+line.charAt(i)-97]++;
			}
			
			int max = 0;
			StringBuilder builder = new StringBuilder();
			for(int value:letters)
				max = Math.max(max, value);
			
			for(int i =0; i < letters.length;i++)
				if(i <26 && letters[i] == max)
					builder.append((char)(i+65));
				else if(i >= 26 && letters[i] == max)
					builder.append((char)(i+71));
			System.out.printf("%s %d\n", builder.toString(), max);
		}

	}
}
