package competitveProgramming.chapter1.uva10300;

import java.util.Scanner;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer testCasesNum =  new Integer(scanner.nextLine());
		for(int i =1; i <= testCasesNum; i++) {
			Integer totalFarmers = new Integer(scanner.nextLine());
			int sum = 0;
			for(int j =1; j <= totalFarmers; j++){
				String[] data = scanner.nextLine().split(" ");
				int area = new Integer(data[0]);
				int animals = new Integer(data[1]);
				int env = new Integer(data[2]);
				int result = area * env;
				sum += result;
			}
			System.out.println(sum);
		}
	}
}
