/**
 * 
 */
package competitveProgramming.chapter1.uva10420;

import java.util.Scanner;
import java.util.SortedMap;
import java.util.TreeMap;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		Integer totalLines =  new Integer(scanner.nextLine());
		SortedMap<String, Integer> countries = new TreeMap<String, Integer>();
		for(int i =1; i <= totalLines; i++) {
			String[] words =  scanner.nextLine().split(" ");
			countries.put(words[0], countries.get(words[0]) == null ? 1 : countries.get(words[0])+1);
		}
		for(String country: countries.keySet())
			System.out.println(country + " " + countries.get(country));
	}
}
