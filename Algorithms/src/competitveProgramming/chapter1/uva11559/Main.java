/**
 * 
 */
package competitveProgramming.chapter1.uva11559;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while(sc.hasNextLine()) {
			String line = sc.nextLine();
			
			StringTokenizer tokens = new StringTokenizer(line);
			
			int customers = new Integer(tokens.nextToken());
			int budget = new Integer(tokens.nextToken());
			int hotelNums = new Integer( tokens.nextToken());
			int umOfWeeks = new Integer(tokens.nextToken());
			int minValue = Integer.MAX_VALUE;
			while(hotelNums > 0) {
				int hotelBudget = new Integer(sc.nextLine());
				String[] roomsPerWeek = sc.nextLine().split(" ");
				for(String avaiRooms : roomsPerWeek) {
					if(new Integer(avaiRooms).compareTo(customers) !=  -1)
						if(budget >= (hotelBudget * customers))
							minValue = Math.min(minValue, hotelBudget * customers);
				}
				hotelNums--;
			}
			if(minValue == Integer.MAX_VALUE)
				System.out.println("stay home");
			else 
				System.out.println(minValue);
		}
	}
}
