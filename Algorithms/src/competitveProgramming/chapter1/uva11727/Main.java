/**
 * 
 */
package competitveProgramming.chapter1.uva11727;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = new Integer(sc.nextLine());
		int count = 1;
		while(testCases > 0) {
			String[] values = sc.nextLine().split(" ");
			int a = new Integer(values[0]);
			int b = new Integer(values[1]);
			int c = new Integer(values[2]);
			
			if(a >= b && a <= c || a <= b && a >= c)
				System.out.println("Case "+ count+": " + a);
			else if(b >= a && b <= c || b <= a && b >= c)
				System.out.println("Case "+ count+": " + b);
			else if(c >=a && c <= b || c <= a && c >= b)
				System.out.println("Case "+ count+": " + c);
			
			count++;
			testCases--;
		}
	}
}
