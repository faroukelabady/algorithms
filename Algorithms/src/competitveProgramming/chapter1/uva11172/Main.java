package competitveProgramming.chapter1.uva11172;

import java.util.Scanner;
import java.util.StringTokenizer;

public class Main {

	public static void main(String[] args) {
		Scanner scanner = new Scanner(System.in);
		int i = new Integer(scanner.nextLine());
		while (i > 0) {
			StringTokenizer data = new StringTokenizer(scanner.nextLine());
			int result = new Integer(data.nextToken()) - new Integer(data.nextToken());
			if(result > 0)
				System.out.println(">");
			else if(result == 0)
				System.out.println("=");
			else
				System.out.println("<");
			i--;
		}
	}
}
