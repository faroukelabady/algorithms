/**
 * 
 */
package competitveProgramming.chapter1.uva11764;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = new Integer(sc.nextLine());
		int count = 1;
		while(testCases > 0) {
			int totalWalls = new Integer(sc.nextLine());
			String[] walls = sc.nextLine().split(" ");
			int previous = new Integer(walls[0]);
			int highJumps = 0;
			int lowJumps = 0;
			for(int i =1; i < walls.length; i++) {
				int val = new Integer(walls[i]);
				if(previous > val) {
					lowJumps++;
				} else if(previous < val) {
					highJumps++;
				}
					
					previous = val;
			}
			System.out.println("Case " + count + ": " + highJumps + " " + lowJumps);
			count++;
			testCases--;
				
		}

	}

}
