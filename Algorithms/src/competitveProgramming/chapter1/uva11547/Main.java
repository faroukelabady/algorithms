/**
 * 
 */
package competitveProgramming.chapter1.uva11547;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		int testCases = new Integer(sc.nextLine());
		
		while(testCases > 0) {
			int value = new Integer(sc.nextLine());
			long result = ((value * 567 / 9) + 7492) * 235 / 47 - 498;
			
			long tensColumn = Math.abs((result / 10) % 10);
			System.out.println(tensColumn);
			
			testCases--;
		}
	}
}
