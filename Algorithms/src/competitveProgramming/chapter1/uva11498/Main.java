/**
 * 
 */
package competitveProgramming.chapter1.uva11498;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		while (true) {
			int testCases = new Integer(sc.nextLine());
			if(testCases ==0 )
				break;
			StringTokenizer data = new StringTokenizer(sc.nextLine()); 
			int x = new Integer(data.nextToken());
			int y = new Integer(data.nextToken());
			while(testCases > 0) {
				data = new StringTokenizer(sc.nextLine()); 
				int xP = new Integer(data.nextToken()) - x;
				int yP = new Integer(data.nextToken()) - y;
				
				if(xP > 0 && yP > 0)
					System.out.println("NE");
				else if(xP < 0 && yP < 0)
					System.out.println("SO");
				else if(xP > 0 && yP < 0)
					System.out.println("SE");
				else if(xP < 0 && yP > 0)
					System.out.println("NO");
				else
					System.out.println("divisa");
				
				testCases--;
			}
		}

	}

}
