package codinggame.tempartures;

import java.util.*;
import java.util.regex.*;
import java.util.stream.*;
import java.io.*;
import java.math.*;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int n = in.nextInt(); // the number of temperatures to analyse

        if (in.hasNextLine()) {
            in.nextLine();
        }
        String temps = in.nextLine(); // the n temperatures expressed as integers ranging from -273 to 5526
        int result = Integer.MAX_VALUE;
        if(temps == null || temps.isEmpty()) {
            result = 0;
        } else {
           Pattern pattern = Pattern.compile(" ");
           result = pattern.splitAsStream(temps)
                            .mapToInt(Integer::valueOf).reduce(Integer.MAX_VALUE, (a , b) -> {
                            	int diff = Math.abs(a) - Math.abs(b);
                            	return diff > 0 ? b : diff < 0 ? a : Math.max(a, b);
                            });
        }
        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");

        System.out.println(result);
    }
}