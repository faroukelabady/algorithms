package codinggame.asc2Art;
import java.util.*;
import java.io.*;
import java.math.*;
import java.nio.CharBuffer;

/**
 * Auto-generated code below aims at helping you parse
 * the standard input according to the problem statement.
 **/
class Solution {

    public static void main(String args[]) {
        Scanner in = new Scanner(System.in);
        int L = in.nextInt();
        int H = in.nextInt();
        if (in.hasNextLine()) {
            in.nextLine();
        }
        String T = in.nextLine().toUpperCase();
        List<StringBuilder> list = new ArrayList<>();
        for (int i = 0; i < H; i++) {
            String ROW = in.nextLine();
            StringBuilder buil = new StringBuilder();
            for(char c: T.toCharArray()) {
            	int start = Character.getNumericValue(c);
            	if(start < 10 || start > 35) {
            		start = 26;
            	} else {
            		start = start - 10;
            	}
            	// I am doing that, so I won't have to create unnecessary
            	// strings objects in the middle
            	for(int j = 0 ; j < L ; j++) {
            		buil.append(ROW.charAt(start * L + j));
            	}

            }
            list.add(buil);
        }

        // Write an action using System.out.println()
        // To debug: System.err.println("Debug messages...");
        list.forEach(System.out::println);
    }
}