package codility;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.DayOfWeek;
import java.time.Month;
import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

public class Test4 {

	public static void main(String[] args) {
		System.out.println(new Test4().solution(2014, "January", "December", "Monday"));
	}

	public int solution(int Y, String A, String B, String W) {
		Month firstMonth = Month.valueOf(A.toUpperCase());
		Month secondMonth = Month.valueOf(B.toUpperCase());
		DayOfWeek firstDay = DayOfWeek.valueOf(W.toUpperCase());
		int weekNum = 0;

		// first Define a date format and parse dates
		SimpleDateFormat sdf = new SimpleDateFormat("dd/MMMMM/yyyy");
		// define calendars for the first and second month and another one for
		// January for comparison
		Calendar firstMonthInput = new GregorianCalendar();
		Calendar secondMonthInput = new GregorianCalendar();
		Calendar janMonth = new GregorianCalendar();

		try {
			// format the dates into the calendars
			Date date = sdf.parse("01/" + firstMonth.name() + "/" + Y);
			firstMonthInput.setTime(date);

			date = sdf.parse("" + secondMonth.length(new GregorianCalendar().isLeapYear(Y)) 
					+ "/" + secondMonth.name() + "/" + Y);
			secondMonthInput.setTime(date);

			date = sdf.parse("01/" + Month.JANUARY.name() + "/" + Y);
			janMonth.setTime(date);

		} catch (ParseException e) {
			e.printStackTrace();
		}

		// find the days between the first and second month
		int monthsDiff = (int) ((secondMonthInput.getTime().getTime() - firstMonthInput.getTime().getTime())
				/ (1000 * 60 * 60 * 24) + 1);

		// find the days between the first month and january
		int dayDiff = (int) ((firstMonthInput.getTime().getTime() - janMonth.getTime().getTime()) / (1000 * 60 * 60 * 24));
		
		// search for the first monday in january
		int firstMonday = 1;
		for (int i = 0; i <= 7; i++) {
			if ("Monday".equalsIgnoreCase(firstDay.name())) {
				break;
			}
			firstMonday++;
			firstDay = firstDay.plus(1);
		}
		
		// go through the day from the day of the first monday to the day of the first month
		// adding 7 days as you go through
		int mondayCounter = firstMonday;
		for (;;) {
			if (mondayCounter > dayDiff) {
				break;
			} 
			mondayCounter += 7;
		}
		// when you reach the first monday subtract the the day diff to get the day of the first month
		mondayCounter = mondayCounter - dayDiff;
		
		// no go through the days from the first month to the last day of the second month 
		// increasing he week num counter as you go through
		for (;;) {
			if (mondayCounter > monthsDiff) {
				mondayCounter -= 7;
				break;
			} 
			mondayCounter += 7;
			weekNum++;
		}
		// if the last monday counter is less than the monthsdiff by 6 days then the last week isn't complete 
		// and we subtract it from the wekNum Counter
		if (monthsDiff - mondayCounter < 6) {
			weekNum--;
		}

		return weekNum;
	}

}
