package codility;

import java.util.Arrays;

public class Test1 {

	public static void main(String args[]) {
		// Test1 t = new Test1();
		// System.out.println(t.solution(new int[] { 7, 21, 3, 42, 3, 7 }));
		System.out.println(Arrays.toString(merge(new int[] {}, new int[] { 2, 4, 7, 12 })));
		System.out.println(Arrays.toString(merge(new int[] { 2 }, new int[] { 2 })));
		System.out.println(Arrays.toString(merge(new int[] { 2, 4, 7, 12, 32 }, new int[] { 2, 5, 7, 8 })));
		System.out.println(Arrays.toString(merge(new int[] { 1, 2, 3 }, new int[] { 4, 5, 6 })));
		System.out.println(Arrays.toString(merge(new int[] { 2, 5, 7, 8 }, new int[] { 2, 4, 7, 12, 32 })));

	}

	public int solution(int[] A) {
		// write your code in Java SE 8
		Arrays.sort(A);
		int n = A.length;
		int min = Integer.MAX_VALUE;
		for (int i = 0; i < n - 1; i++) {
			int result = Math.abs(A[i] - A[i + 1]);
			min = Math.min(min, result);
		}

		return min;

	}

	public static int[] merge(int[] arrLeft, int[] arrRight) {

		int leftSize = arrLeft.length;
		int rightSize = arrRight.length;
		int[] arr = new int[leftSize + rightSize];
		int leftIndex = 0; 
		int rightIndex = 0;
		for (int i = 0; i < arr.length; i++) {
			if (arrLeft.length == 0) {
				arr[i] = arrRight[i];
			} else if (arrRight.length == 0) {
				arr[i] = arrLeft[i];
			} else {
				if(rightIndex >= rightSize) {
					arr[i] = arrLeft[leftIndex];
					leftIndex++;
				} else if(leftIndex >= leftSize) {
					arr[i] = arrRight[rightIndex];
					rightIndex++;
				} else if (arrLeft[leftIndex] < arrRight[rightIndex]) {
					arr[i] = arrLeft[leftIndex];
					leftIndex++;
				} else {
					arr[i] = arrRight[rightIndex];
					rightIndex++;
				}
			}

		}

		return arr;

	}
}
