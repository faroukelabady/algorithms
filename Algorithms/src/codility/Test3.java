package codility;

public class Test3 {

	public static void main(String args[]) {
		Test3 t = new Test3();
		System.out.println(t.solution(new int[] {1,1,1,1,1,0,0,1,0}));
	}

	int solution(int[] A) {
		int n = A.length;
		int result = 0;
		for (int i = 0; i < n - 1; i++) {
			if (A[i] == A[i + 1])
				result = result + 1;
		}
		int r = 0;
		for (int i = 0; i < n; i++) {
			int count = 0;
			if (i > 0) {
				if (A[i - 1] != A[i])
					count = count + 1;
				else
					count = count - 1;
			}
			if (i < n - 1) {
				if (A[i + 1] != A[i])
					count = count + 1;
				else
					count = count - 1;
			}
			r = Math.max(r, count%2 == 0?count: 0);
		}
		return result + r;
	}

}
