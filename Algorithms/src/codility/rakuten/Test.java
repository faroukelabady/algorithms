package codility.rakuten;

public class Test {

	// public static void main(String[] args) {
	// // TODO Auto-generated method stub
	// Test t = new Test();
	// System.out.println(t.solution(3, 4, 0));
	// System.out.println(t.solution(3, 4, 1));
	// System.out.println(t.solution(3, 4, 2));
	// System.out.println(t.solution(3, 4, 3));
	// System.out.println(t.solution(3, 4, 4));
	// System.out.println(t.solution(3, 4, 5));
	//
	// }
	static int divideDenominator = 1_000_000_007;

	public int solution(int A, int B, int N) {
		// write your code in Java SE 8
		int divideDenominator = 1_000_000_007;
		int firstResult = (A % divideDenominator);
		int secondResult = (B % divideDenominator);
		long finalResult = 0;
		if (N == 0) {
			return firstResult;
		} else if (N == 1) {
			return secondResult;
		} else {
			for (int i = 2; i <= N; i++) {
				finalResult = (firstResult + secondResult) % divideDenominator;
				firstResult = secondResult;
				secondResult = (int) finalResult;
			}

		}
		return (int) finalResult;
	}

	static int MAX = 1000;
	// Create an array for memoization
	int[] f = new int[MAX];

	// public int solution2(int A, int B, int N) {
	//
	//// If n is even then k = n/2:
	//// F(n) = [2*F(k-1) + F(k)]*F(k)
	////
	//// If n is odd then k = (n + 1)/2
	//// F(n) = F(k)*F(k) + F(k-1)*F(k-1)
	// }

	static int fib(int A, int B, int N) {
		int F[][] = new int[][] {
				{ (A % divideDenominator + B % divideDenominator) % divideDenominator, B % divideDenominator },
				{ B % divideDenominator, A % divideDenominator } };

		if (N == 0) {
			return A % divideDenominator;
		} else if (N == 1) {
			return B % divideDenominator;
		}
		power(F,A, B, N - 1);

		return F[0][0];
	}

	static void multiply(int F[][], int M[][]) {
		int x = (F[0][0] * M[0][0] + F[0][1] * M[1][0]) % divideDenominator ;
		int y = (F[0][0] * M[0][1] + F[0][1] * M[1][1]) % divideDenominator;
		int z = (F[1][0] * M[0][0] + F[1][1] * M[1][0]) % divideDenominator;
		int w = (F[1][0] * M[0][1] + F[1][1] * M[1][1]) % divideDenominator;

		F[0][0] = x;
		F[0][1] = y;
		F[1][0] = z;
		F[1][1] = w;
	}

	/* Optimized version of power() in method 4 */
	static void power(int F[][],int A, int B, int n) {
		if (n == 0 || n == 1)
			return;
		int M[][] = new int[][] {
			{ (A % divideDenominator + B % divideDenominator) % divideDenominator, B % divideDenominator },
			{ B % divideDenominator, A % divideDenominator } };

		power(F,A, B, n / 2);
		multiply(F, F);

		if (n % 2 != 0)
			multiply(F, M);
	}

	/* Driver program to test above function */
	public static void main(String args[]) {
		int n = 9;
//		System.out.println(fib(n));
		
		System.out.println(fib(3, 4, 0));
		System.out.println(fib(3, 4, 1));
		System.out.println(fib(3, 4, 2));
		System.out.println(fib(3, 4, 3));
		System.out.println(fib(3, 4, 4));
		System.out.println(fib(3, 4, 5));
	}

	// private long recursiveGF(int A, int B, int N) {
	//
	// if (N == 0) {
	// return A % divide;
	// } else if (N == 1) {
	// return B % divide;
	// } else {
	// return (recursiveGF(A, B, N - 1) + recursiveGF(A, B, N - 2)) % divide;
	// }
	// }

}
