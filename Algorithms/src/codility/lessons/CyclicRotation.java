package codility.lessons;

public class CyclicRotation {

	public static void main(String[] args) {

		CyclicRotation cr = new CyclicRotation();
		cr.solution(new int[] { 3,1, 8, 9, 7, 6 }, 3);

	}

	public int[] solution(int[] A, int K) {
		int l = A.length;
		if (l == 0) {
			return new int[0];
		}

		K = K % l;

		reverse(A, l - K, l - 1);
		reverse(A, 0, l - K - 1);
		reverse(A, 0, l - 1);

		return A;
	}

	private void reverse(int[] arr, int i, int j) {
		int constant = (j - i + 1) / 2;
		for (int idx = 0; idx < constant; idx++) {
			int temp = arr[i + idx];
			arr[i + idx] = arr[j - idx];
			arr[j - idx] = temp;
		}
	}

	// public int[] solution(int[] A, int K) {
	// int[] B = new int[A.length];
	// for(int i = 0; i < A.length; i++) {
	// B[(i + K) % A.length] = A[i];
	// }
	// return B;
	// }

	// public int[] solution(int[] A, int K) {
	// // write your code in Java SE 8
	// if(A.length == 0) {
	// return A;
	// }
	// for(int i =0; i < K; i++) {
	// shiftRight(A);
	// }
	// return A;
	// }

	private void shiftRight(int[] A) {
		int lastValue = A[A.length - 1];
		for (int i = A.length - 2; i >= 0; i--) {
			A[i + 1] = A[i];
		}
		A[0] = lastValue;
	}

}
