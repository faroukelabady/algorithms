package codility.lessons;

import javax.swing.plaf.basic.BasicInternalFrameTitlePane.MaximizeAction;

public class BinaryGap {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		BinaryGap bg = new BinaryGap();
		System.out.println(bg.solution(9));
		System.out.println(bg.solution(529));
		System.out.println(bg.solution(20));
		System.out.println(bg.solution(15));
		System.out.println(bg.solution(1041));
		System.out.println(bg.solution(0));
		System.out.println(bg.solution(8));
		System.out.println(bg.solution(0X08));
	}

	public int solution(int N) {
		// write your code in Java SE 8
		int remindar = N / 2 ;
		int previousBitValue = 0;
		int currentBitValue = 0;
		currentBitValue = previousBitValue = N & 1;
		int i = 0;
		int startIndex = 0;
		int endIndex = 0;
		int maxValue = 0;
		boolean startFound = false;
		boolean endFound = false;
		while (remindar > 0) {
			currentBitValue = remindar & 1;
			
			if (currentBitValue != previousBitValue && previousBitValue == 1) {
				startFound = true;
				startIndex = i;
			}
			
			if (currentBitValue != previousBitValue && previousBitValue == 0) {
				endFound = true;
				endIndex = i;
			}
			
			if(endIndex > startIndex && startFound && endFound) {
				maxValue = Math.max(endIndex - startIndex, maxValue);
			}
			previousBitValue = currentBitValue;
			remindar >>= 1;
			i++;
		}

		return maxValue;
	}
	
/*
 * 
 * def solution(N):
    cnt = 0
    result = 0
    found_one = False
 
    i = N    
         
    while i:
        if i & 1 == 1:
            if (found_one == False):
                found_one = True
            else:
                result = max(result,cnt)
            cnt = 0
        else:
            cnt += 1
        i >>= 1
    
    return result
 */
	

}
