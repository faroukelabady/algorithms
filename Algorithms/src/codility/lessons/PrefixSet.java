package codility.lessons;

import java.util.HashSet;
import java.util.Set;

public class PrefixSet {

	public static void main(String[] args) {

	}

	public static int solution(int[] A) {
		// write your code in Java SE 8
		Set<Integer> numbersBag = new HashSet<>();
		int coveringPrefix = 0;
		for(int i = 0; i < A.length; i++) {
			if(!numbersBag.contains(A[i])) {
				coveringPrefix = i;
			}
			numbersBag.add(A[i]);
		}
		
		return coveringPrefix;
		
	}

}
