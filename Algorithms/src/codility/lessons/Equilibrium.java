package codility.lessons;

public class Equilibrium {

	public static void main(String[] args) {
		// TODO Auto-generated method stub

	}
	
	public int solution(int[] A) {
        // write your code in Java SE 8
		int p = -1;
		long leftSum = 0;
		long rightSum = 0;
		for(int i : A) {
			rightSum += i;
		}
		
		for(int i = 0; i < A.length - 1; i++) {
			if(leftSum == rightSum) {
				p = i;
				break;
			} else {
				leftSum += A[i];
				rightSum -= A[i] - A[i + 1];
			}
			
		}
		
		return p;
    }

}
