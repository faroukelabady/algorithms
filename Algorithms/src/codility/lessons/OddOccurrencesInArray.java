package codility.lessons;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class OddOccurrencesInArray {

	public static void main(String[] args) {
		System.out.println(solution(new int[]{9, 3, 9, 3, 9, 7, 9} ));
	}

	/*
	 * def solution(A):
    missing_int = 0
    for value in A:
        missing_int ^= value
    return missing_int
	 */
	public static int solution(int[] A) {
		// write your code in Java SE 8
		return Arrays.stream(A).reduce((l,r) -> l ^ r).getAsInt();
//		Set<Integer> values = new HashSet<>();
//		for(int i: A) {
//			if(values.contains(i)) {
//				values.remove(i);
//			} else {
//				values.add(i);
//			}
//			
//		}
//		
//		return values.iterator().next();
	}
}
