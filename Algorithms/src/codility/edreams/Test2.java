package codility.edreams;


public class Test2 {

	public static void main(String args[]) {
		Test2 t = new Test2();
		System.out.println(t.solution(new int[] {1,3,-3}));
	}

	public int solution(int[] A) {
		// write your code in Java SE 8
		int length = A.length;
		int max = 0;
		for(int i =0; i < length; i++) {
			for(int j = 0; j < length; j++) {
				int result = A[i] + A[j] + (j - i);
				//System.out.println(result);
				max = Math.max(max, result);
			}
		}
		return max;
	}


}
