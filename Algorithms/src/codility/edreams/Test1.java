package codility.edreams;

import java.util.Arrays;


public class Test1 {

	public static void main(String args[]) {
		Test1 t = new Test1();
		System.out.println(t.solution(-4, 17));
	}

	public int solution(int A, int B) {
		// write your code in Java SE 8
		int counter = 0;
		for(int i = 0; i <= B / 3; i++) {
			int multiply = i * i;
			if(multiply >= A && multiply <= B ) {
				counter++;
			}
		}

		return counter;

	}

}
