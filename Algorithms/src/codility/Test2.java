package codility;

import java.util.Arrays;

public class Test2 {

	public static void main(String args[]) {
		Test2 t = new Test2();
		//System.out.println(t.solution2(new int[] { 1, 3, -3 }));
		System.out.println(maxSum(new int[] { 1, 3, -3 }, 0, 3));
	}

	public int solution(int[] A) {
		// write your code in Java SE 8
		int length = A.length;
		int max = 0;
		for (int i = 0; i < length; i++) {
			for (int j = 0; j < length; j++) {
				int result = A[i] + A[j] + (j - i);
				// System.out.println(result);
				max = Math.max(max, result);
			}
		}
		return max;
	}
	public static int maxSum(int a[], int start, int end) {
	    int maxSum = 0;

	    // Trivial cases
	    if (start == end) {
	        return a[start];
	    } else if (start > end) {
	        return 0;
	    } else if (end - start == 1) {
	        return a[start] > a[end] ? a[start] : a[end];
	    } else if (start < 0) {
	        return 0;
	    } else if (end >= a.length) {
	        return 0;
	    }

	    // Subproblem solutions, DP
	    for (int i = start; i <= end; i++) {
	        int possibleMaxSub1 = maxSum(a, i, end);
	        int possibleMaxSub2 = maxSum(a, start, i);

	        int possibleMax = possibleMaxSub1 + possibleMaxSub2 + a[i];
	        if (possibleMax > maxSum) {
	            maxSum = possibleMax;
	        }
	    }

	    return maxSum;
	}
	public int solution3(int[] A) {
		// write your code in Java SE 8
		int length = A.length;
		int dp[] = new int[] {A[0],A[0]};

		for(int i=1;i<A.length;i++)
		{
		    int temp = dp[1];
		    dp[1] = dp[0]+A[i] + (i - 0) ;
		    dp[0] = Math.max(dp[0],temp);
		}
		return  Math.max(dp[0],dp[1]);
	}

	public int solution2(int[] A) {
		// write your code in Java SE 8
		int length = A.length;
		int maxNum = Integer.MIN_VALUE;
		int maxIndex = 0;
		int max = Integer.MIN_VALUE;

		for (int i = 0; i < length; i++) {
			if (A[i] >= maxNum) {
				maxNum = A[i];
				maxIndex = i;
			}
		}

		for (int i = 0; i <= maxIndex; i++) {
			int result = A[i] + maxNum + (maxIndex - i);
			max = Math.max(max, result);
		}

		for (int i = maxIndex; i < length; i++) {
			int result = A[i] + maxNum + (i - maxIndex);
			max = Math.max(max, result);
		}

		return max;
	}
}
