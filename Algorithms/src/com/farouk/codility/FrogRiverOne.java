package com.farouk.codility;

import java.util.HashSet;
import java.util.Set;

public class FrogRiverOne {
	// 5 , [1, 3, 1, 4, 2, 3, 5, 4]
	public int solution(int X, int[] A) {
		Set<Integer> numbers = new HashSet<>();
		int leastTime = -1;
		for(int i = 0; i < A.length; i++) {
			
			numbers.add(A[i]);
			if(numbers.size() >= X) {
				leastTime = i;
				break;
			}
		}
		System.out.println(leastTime);
		
		return leastTime;
		
	}
	
	public static void main(String[] args) {
		new FrogRiverOne().solution(5 , new int[] {1, 3, 1, 4, 2, 3, 5, 4});
	}

}
