package com.farouk.codility;

public class FrogJmp {

	public int solution(int X, int Y, int D) {
		
		int distance = Y - X;
		double val = ((double)distance /(double) D);
		System.out.println(val);
		int jumps = (int)Math.ceil(val);
		return jumps;
	}
	
	public static void main(String[] args) {
		new FrogJmp().solution(10, 85, 30);
	}

}
