package com.farouk.codility;

import java.util.Arrays;

public class PassingCars {

	
	public int solution(int[] A) {
		int result = 0;
		int zeroCars = 0;
		for(int value:A) {
			if(value == 0) {
				zeroCars += 1;
			} else if (value == 1) {
				result += zeroCars;
			}
		}
		
		if(result > 1000000000 || result < 0) {
			result = -1;
		}
		return result;
    }
	
	public static void main(String[] args) {
		int[] A = new int[100000];
		int value=1;
		for(int i =0; i <A.length; i++) {
			if(value == 1) {
				value = 0;
			} else {
				value = 1;
			}
			A[i] =  value;
		}
		System.out.println(Arrays.toString(A));
		System.out.println(new PassingCars().solution(A));

	}

}
