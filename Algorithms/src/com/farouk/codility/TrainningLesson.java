package com.farouk.codility;

public class TrainningLesson {
	
	public static void main(String[] args) {
		solution(new int[]{-1, 3, -4, 5, 1, -6, 2, 1});
	}

	public static int solution(int[] A) {

		int sumRight = 0;
		int sumLeft = A[0];
		int min = Integer.MAX_VALUE;

		for(int i = 1; i < A.length ; i++) {
			sumRight += A[i];
		}
		
		min = Math.min(min, Math.abs((sumLeft-sumRight)));
		
		for(int i = 1; i < A.length-1; i++) {
			sumLeft += A[i];
			sumRight -= A[i];
			min = Math.min(min, Math.abs((sumLeft-sumRight)));
		}
		
		return min;
	}

}
