package com.farouk.codility;

public class MinAvgTwoSlice {
	
	public int solution(int[] A) {
        // write your code in Java SE 8
		double minAve = (double)(A[0] + A[1])/ 2.0;
		int minPos = 0;
		
		for(int i = 0; i < A.length - 2; i++) {
			if((double)(A[i]+A[i+1]) / 2.0 < minAve) {
				minAve = (double)(A[i]+A[i+1]) / 2.0;
				minPos = i;
			} 
			if((double)(A[i]+A[i+1]+A[i+2]) / 3.0 < minAve) {
				minAve = (double)(A[i]+A[i+1]+A[i+2]) / 3.0 ;
				minPos = i;
			}
			
		}
		if((double)(A[A.length-1]+A[A.length-2]) / 2.0 < minAve) {
			minAve = (double)(A[A.length-1]+A[A.length-2]) / 2.0;
			minPos = A.length-2;
		} 
		
		return minPos;
		
    }

	public static void main(String[] args) {
		System.out.println(new MinAvgTwoSlice().solution(new int[] {4,2,2,5,1,5,8}));
 	}

}
