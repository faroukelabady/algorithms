package com.farouk.codility;

import java.util.Arrays;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.TreeSet;

public class GenomicRangeQuery {

	class genoms<T> implements Comparable<T>{
		int start;
		int end;

		public genoms(int start, int end) {
			super();
			this.start = start;
			this.end = end;
		}

		@Override
		public boolean equals(Object obj) {
			if (this.start == ((genoms) obj).start && this.end == ((genoms) obj).end) {
				return true;
			}
			return false;
		}
		
		@Override
		public int hashCode() {
			// TODO Auto-generated method stub
			return super.hashCode();
		}

		@Override
		public int compareTo(T o) {
			if (this.start == ((genoms) o).start && this.end == ((genoms) o).end) {
				return 0;
			}
			return -1;
		}
	}

	public int[] solution(String S, int[] P, int[] Q) {
		char[] genom = S.toCharArray();
		Set<genoms> set = new TreeSet<>();
		int[] j = new int[P.length]; 
		int counter = 0;
		for(int i =0; i < P.length; i++) {
			set.add(new genoms(P[i], Q[i]));
		}
		for(genoms g : set) {
			for(int k = g.start; k <= g.end-1; k++) {
				if(genom[k] < genom[k+1]) {
					j[counter] = k;
				}
			}
			counter++;
		}
		System.out.println(Arrays.toString(j));
		return j;
		
	}
	
	public static void main(String[] args) {
		System.out.println(new GenomicRangeQuery().solution("CAGCCTA", new int[] {2,5,0}, new int[] {4,5,6}));
	}

}
