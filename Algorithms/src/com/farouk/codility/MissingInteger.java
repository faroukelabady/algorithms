package com.farouk.codility;

import java.util.Arrays;

public class MissingInteger {
	public int solution(int[] A) {
		Arrays.sort(A);
		int missingInt = 1;
		for(int i:A) {
			if(i == missingInt) {
				missingInt++;
			}
		}
		
		return missingInt;
		
	}
}
