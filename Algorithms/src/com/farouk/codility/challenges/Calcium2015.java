package com.farouk.codility.challenges;

import java.util.ArrayDeque;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.Map;
import java.util.Queue;
import java.util.Stack;

/*
 * For example, given K = 2 and the following arrays:

  A[0] = 5    B[0] = 1
  A[1] = 1    B[1] = 0
  A[2] = 0    B[2] = 7
  A[3] = 2    B[3] = 4
  A[4] = 7    B[4] = 2
  A[5] = 0    B[5] = 6
  A[6] = 6    B[6] = 8
  A[7] = 6    B[7] = 3
  A[8] = 1    B[8] = 9
 */
public class Calcium2015 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = new int[] { 5, 1, 0, 2, 7, 0, 6, 6, 1 };
		int[] B = new int[] { 1, 0, 7, 4, 2, 6, 8, 3, 9 };
		int k = 2;
		System.out.println(new Calcium2015().solution(A, B, k));
	}

	public int solution(int[] A, int[] B, int K) {

		Graph g = new Graph(A.length);
		for (int i = 0; i < A.length; i++) {
			g.addEdge(A[i], B[i]);
		}

		System.out.println(g.toString());
		
		Object[] roadEnds = (Object[]) g.getRoadEnds().toArray();
		int start = (int) roadEnds[0];
		BreadthFirstPaths bfs = new BreadthFirstPaths(g, start);
		HashMap<Road, Integer> roads = new HashMap<>();
		for(int i = 1 ; i < roadEnds.length; i++) {
			int start2 = (int) roadEnds[i];
			for(Road road : bfs.pathTo(start2)) {
				//Road road = new Road( start2, t);
				if(!roads.containsKey(road)) {
					roads.put(road, 0);
				} else {
					roads.put(road, roads.get(road) + 1);
				}
				//start2 = t;
				
				System.out.print(road);
			}
			System.out.println();
		}
		for(Map.Entry<Road, Integer> entry : roads.entrySet()) {
			System.out.print("Road : " + entry.getKey().start + " : " + entry.getKey().end);
			System.out.print(" : crosses : " + entry.getValue());
			System.out.println();
		}
		
		

		return 0;
	}
	
	class Road {
		private int start;
		private int end;
		private int crosses;
		
		
		
		public Road(int start, int end) {
			super();
			this.start = start;
			this.end = end;
		}
		
		
		
		@Override
		public String toString() {
			return " " + start ;
		}



		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + getOuterType().hashCode();
			result = prime * result + end;
			result = prime * result + start;
			return result;
		}


		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Road other = (Road) obj;
			if (!getOuterType().equals(other.getOuterType()))
				return false;
			if (end != other.end)
				return false;
			if (start != other.start)
				return false;
			return true;
		}


		public int getStart() {
			return start;
		}
		public void setStart(int start) {
			this.start = start;
		}
		public int getEnd() {
			return end;
		}
		public void setEnd(int end) {
			this.end = end;
		}
		public int getCrosses() {
			return crosses;
		}
		public void setCrosses(int crosses) {
			this.crosses = crosses;
		}


		private Calcium2015 getOuterType() {
			return Calcium2015.this;
		}
	}

	class Graph {
		private final int V;
		private int E;
		private LinkedList<Road>[] connections;
		private HashSet<Integer> roadEnds;

		public Graph(int roads) {
			super();
			V = roads + 1;
			connections = (LinkedList<Road>[]) new LinkedList[V];
			roadEnds = new HashSet<>(10);
			for (int v = 0; v < V; v++) {
				connections[v] = new LinkedList<>();
				roadEnds.add(v);
			}

		}

		public void addEdge(int V, int W) {
			connections[V].add(new Road(V , W));
			connections[W].add(new Road(W , V));
			if (connections[V].size() > 1) {
				roadEnds.remove(V);
			}
			if (connections[W].size() > 1) {
				roadEnds.remove(W);
			}
			E++;
		}

		public Iterable<Road> connections(int V) {
			return connections[V];
		}

		@Override
		public String toString() {
			String s = V + " vertices, " + E + " edges\n";
			for (int v = 0; v < V; v++) {
				s += v + ": ";
				for (Road w : this.connections(v))
					s += w.getEnd() + " ";
				s += "\n";
			}
			System.out.println("edge roads");
			for (Integer i : roadEnds) {
				System.out.println(i);
			}
			return s;
		}

		public HashSet<Integer> getRoadEnds() {
			return roadEnds;
		}

		public int getV() {
			return V;
		}

	}

	public class BreadthFirstPaths {
		private boolean[] marked; // Is a shortest path to this vertex known?
		private int[] edgeTo; // last vertex on known path to this vertex
		private final int s; // source

		public BreadthFirstPaths(Graph G, int s) {
			marked = new boolean[G.getV()];
			edgeTo = new int[G.getV()];
			this.s = s;
			bfs(G, s);
		}

		private void bfs(Graph G, int s) {
			Queue<Integer> queue = new ArrayDeque<>();
			marked[s] = true; // Mark the source
			queue.add(s); // and put it on the queue.
			while (!queue.isEmpty()) {
				int v = queue.remove(); // Remove next vertex from the queue.
				for (Road w : G.connections(v))
					if (!marked[w.getEnd()]) // For every unmarked adjacent vertex,
					{
						edgeTo[w.getEnd()] = v; // save last edge on a shortest path,
						marked[w.getEnd()] = true; // mark it because path is known,
						queue.add(w.getEnd()); // and add it to the queue.
					}
			}
		}

		public boolean hasPathTo(int v) {
			return marked[v];
		}

		public Iterable<Road> pathTo(int v) {
			if (!hasPathTo(v)) return null;
			Stack<Road> path = new Stack<Road>();
			int x = v;
			for (; x != s; x = edgeTo[x]) {
				Road road = new Road(x, edgeTo[x]);
				path.push(road);
			}
			//path.push(new Road(x, edgeTo[x]));
			return path;
		}
	}

}
