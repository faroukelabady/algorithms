package com.farouk.codility.challenges;

import java.util.Arrays;

public class Argon2015 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		Argon2015 ar = new Argon2015();
		System.out.println(ar.solution(new int[] {0,1}));
	}
	
	public int solution(int[] A) {
		
		/*
		 * for an input to be valid the tourist must have at least 
		 * one day for a swim (0) in his first part of the vacation, 
		 * and one day for trekking (1) in his second part of the vacation
		 * so if we have an input [0,1] this will be valid but if we have an
		 * input of [1,0] this will be invalid, if we generalize this formula
		 * then we need to find the first occurrence of zero and the last 
		 * occurrence of one and make sure the input is in the valid formula
		 * [......,0,..............,1,....]  
		 */
		int n = A.length - 1;
		int firstZeroIndex = -1;
		int lastOneIndex = -1;
		
		// first loop to find first zero and last one
		for(int i = 0; i <= n ; i++) {
			if(firstZeroIndex == -1 && A[i] == 0) {
				firstZeroIndex = i;
			}
			if(A[i] == 1) {
				lastOneIndex = i;
			}
		}
		//System.out.println("first Zero : " + firstZeroIndex);
		//System.out.println("last One : " + lastOneIndex);
		// now we have the indices  let's make sure the formula we deduced is correct
		// if not return 0 as there is no valid solution
		if(firstZeroIndex == -1 || lastOneIndex == -1 || firstZeroIndex > lastOneIndex) {
			return 0;
		}
		
		/* now the first solution will be the difference between the first zero index and
		 * last one index but of course we want to expand this solution to be optimal
		 * to do this we need to find a good split position two have extra zero's in the
		 * left half and extra one's in the right half so that we can expand the each half
		 * while maintaining the second requirment which is the amount of zero,s will be
		 * more than the half size of the first part and the amount of one's will be more than 
		 * the half size on the second part , to achieve this we need to count how many extra zero's
		 * from left to right that we have and how many extra one's from the right to left and store's
		 * the information in two arrays one for zero's and one for the one's
		 */
		// the size is the calculation from the last one index and the first zero index
		int baseSize = lastOneIndex - firstZeroIndex + 1;
		int[] extraZeros = new int[baseSize];
		int[] extraOnes = new int[baseSize];
		// we start from -1 because we already have zero in our disposal the start of the array
		int extraZero = -1; 
		// we start from -1 because we already have one in our disposal the end of the array
		int extraOne = -1;
		
		int zeroCounter = 0;
		// second loop to fill both arrays
		for(int i = firstZeroIndex, j = lastOneIndex ; i <= lastOneIndex && j >= firstZeroIndex; i++ , j--) {
			if(A[i] == 0) {
				extraZero++;
			} else {
				extraZero--;
			}
			
			if(A[j] == 1) {
				extraOne++;
			} else {
				extraOne--;
			}
			extraZeros[zeroCounter] = extraZero;
			extraOnes[baseSize - 1 - zeroCounter] = extraOne;
			zeroCounter++;
		}
		//System.out.println("extra zeros array : " + Arrays.toString(extraZeros));
		//System.out.println("extra ones  array : " + Arrays.toString(extraOnes));
		/*
		 * first of we need check how many available elements on the left and the right
		 */
		int avaLeft = firstZeroIndex;
		int avaRight = n  - lastOneIndex;
		// then we define maxExtraSize and loop through the extra arrays
		int maxExtraSize = 0;
		for(int index = 0; index < extraOnes.length - 1; index++) {
			// we need to check a condition as follows if there is avaLeft and
			// there is extraZeros then grab another element from the left 
			// and the same for the ones this can be done with if statements
			// or just get the min value comparing the avaLeft and what can 
			// grab from it using the extraZeros
			int grabLeftSize = Math.min(avaLeft, extraZeros[index]);
			// we use index+1 to eliminate the edges from the calculation as
			// we can't add the last one to the left side and vice versa
			int grabRightSize = Math.min(avaRight, extraOnes[index + 1]);
			// no get the max grab from left and right
			int extraLeft = Math.max(0, grabLeftSize);
			int extraRight = Math.max(0, grabRightSize);
			maxExtraSize = Math.max(maxExtraSize, extraLeft + extraRight); 
		}
		
		return baseSize + maxExtraSize;
		
	}

}
