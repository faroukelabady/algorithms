package com.farouk.codility.challenges;

import java.util.LinkedList;

import com.farouk.codility.challenges.Calcium2015.Graph;

public class Calcium2015V2 {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		int[] A = new int[] { 5, 1, 0, 2, 7, 0, 6, 6, 1 };
		int[] B = new int[] { 1, 0, 7, 4, 2, 6, 8, 3, 9 };
		int k = 2;
		System.out.println(new Calcium2015().solution(A, B, k));
	}

	public int solution(int[] A, int[] B, int K) {
		EdgeWeightedGraph g = new EdgeWeightedGraph(A.length);
		for (int i = 0; i < A.length; i++) {
			g.addEdge(new Edge(A[i], B[i],1));
		}

		return 0;
	}

	class Edge implements Comparable<Edge> {
		private final int v; // one vertex
		private final int w; // the other vertex
		private final double weight; // edge weight

		public Edge(int v, int w, double weight) {
			this.v = v;
			this.w = w;
			this.weight = weight;
		}

		public double weight() {
			return weight;
		}

		public int either() {
			return v;
		}

		public int other(int vertex) {
			if (vertex == v)
				return w;
			else if (vertex == w)
				return v;
			else
				throw new RuntimeException("Inconsistent edge");
		}

		public int compareTo(Edge that) {
			if (this.weight() < that.weight())
				return -1;
			else if (this.weight() > that.weight())
				return +1;
			else
				return 0;
		}

		public String toString() {
			return String.format("%d-%d %.2f", v, w, weight);
		}
	}

	class EdgeWeightedGraph {

		private final int V; // number of vertices
		private int E; // number of edges
		private LinkedList<Edge>[] adj; // adjacency lists

		public EdgeWeightedGraph(int V) {
			this.V = V;
			this.E = 0;
			adj = (LinkedList<Edge>[]) new LinkedList[V];
			for (int v = 0; v < V; v++)
				adj[v] = new LinkedList<Edge>();
		}

		public int V() {
			return V;
		}

		public int E() {
			return E;
		}

		public void addEdge(Edge e) {
			int v = e.either(), w = e.other(v);
			adj[v].add(e);
			adj[w].add(e);
			E++;
		}

		public Iterable<Edge> adj(int v) {
			return adj[v];
		}

		public Iterable<Edge> edges() {
			LinkedList<Edge> b = new LinkedList<Edge>();
			for (int v = 0; v < V; v++)
				for (Edge e : adj[v])
					if (e.other(v) > v)
						b.add(e);
			return b;
		}
	}

}
