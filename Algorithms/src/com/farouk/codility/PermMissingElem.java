package com.farouk.codility;

import java.util.Arrays;

public class PermMissingElem {

	public int solution(int[] A) {
		long N = A.length + 1;
		long sum = N * (N + 1) / 2;
		for(int i  :A)  {
				sum -= i;
		}
		System.out.println(sum);
		return  (int)sum;
	}
	
	public static void main(String[] args) {
		new PermMissingElem().solution(new int[] {1,2,4});
	}

}
