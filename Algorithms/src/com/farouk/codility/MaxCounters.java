package com.farouk.codility;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class MaxCounters {
	
    public int[] solution(int N, int[] A) {
       int[] counter = new int[N];
       int condition = N + 1;
       int max = 0;
       int lastUpdat = 0;
       for(int i =0; i < A.length; i++) {
    	   if(A[i] == condition) {
    		   lastUpdat = max;
    	   } else {
    		   int pos = A[i] - 1;
    		   if(counter[pos] < lastUpdat )
    			   counter[pos] = lastUpdat+1;
    		   else 
    			   counter[pos]++;
    		   
    		   if(counter[pos] > max)
    			   max = counter[pos];
    	   }
       }
       for(int j =0; j < counter.length; j++) {
    	   if(counter[j] < lastUpdat)
    		   counter[j] = lastUpdat;
       }
       return counter;

    		   
    }

}
