package com.farouk.codility;

public class testClass {
	
	public int solution(int[] A) {
		
//		long sum = 0;
//		long leftSum = 0;
//
//		for (int i=0;i<A.length;i++){
//		leftSum += A[i];
//		}
//		for (int i=0;i<A.length;i++){
//		if (sum==leftSum-A[i]) return i;
//		sum+=A[i];
//		leftSum-=A[i];
//		}
//		return -1;
		if(A.length == 0) {
			
			return -1;
		}
		long leftSum = 0;
		long rightSum = 0;
		for(int i = 1; i < A.length; i++) {
			rightSum += A[i];
		}
		if(rightSum == leftSum) {
			return 0;
		}
		for(int i = 1; i < A.length; i++) {
			leftSum += A[i-1];
			rightSum -= A[i];
			if(leftSum == rightSum) {
				return i;
			}
		}
		return -1;

    }

	public static void main(String[] args) {
		System.out.println(new testClass().solution(new int[] {})); 

	}

}
