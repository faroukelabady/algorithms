package com.farouk.codility;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

public class PermCheck {

	public int solution(int[] A) {
		Arrays.sort(A);
		int result = 1;
		for(int i = 0 ; i < A.length ; i++) {
			if(i+1 != A[i]) {
				result = 0;
				break;
			}
			
		}
		return result;
	}
	
	public static void main(String[] args) {
		new PermCheck().solution(new int[] {4, 1, 3});
	}

}
