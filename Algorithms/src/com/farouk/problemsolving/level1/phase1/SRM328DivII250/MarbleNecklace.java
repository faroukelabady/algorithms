/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.SRM328DivII250;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;



/**
 * @author Farouk Elabady
 *7
    
Consider a necklace composed of marbles of various colors arranged in a circle. 
The colors are represented by uppercase letters. We can describe a necklace with a string of 
characters as follows: start with any marble and go through all the marbles in either a clockwise or 
counter-clockwise direction, until the starting marble is reached again, meanwhile appending to 
the string the colors of the marbles in the order they are visited. Obviously, there could be many 
different strings describing the same necklace. For example, the necklace described by the string 
"CDAB" can also be described by seven other strings (see example 0).
You are given a String necklace containing the description of a necklace. 
Return the description for that necklace that comes earliest alphabetically.
Definition
    
Class:
MarbleNecklace
Method:normalize
Parameters:String
Returns:String
Method signature:String normalize(String necklace)
(be sure your method is public)
Limits    
Time limit (s):2.000
Memory limit (MB):64
Constraints
-necklace will contain between 1 and 50 characters, inclusive.
-Each character of necklace will be an uppercase letter ('A'-'Z').
Examples
0)    
"CDAB"
Returns: "ABCD"
This necklace can be described by the eight strings "CDAB", "DABC", "ABCD", "BCDA", "CBAD", 
"DCBA", "ADCB", "BADC". "ABCD" comes first lexicographically.
1)    
"RGB"
Returns: "BGR"

2)    
"TOPCODER"
Returns: "CODERTOP"

3)    
"SONBZELGFEQMSULZCNPJODOWPEWLHJFOEW"
Returns: "BNOSWEOFJHLWEPWODOJPNCZLUSMQEFGLEZ"

 */
public class MarbleNecklace {
	
	class RotationStatus{
		int index;
		StringBuilder value;
	}
	
	class CustomComparator implements Comparator<RotationStatus> {
	    @Override
	    public int compare(RotationStatus o1, RotationStatus o2) {
	        return o1.value.toString().compareTo(o2.value.toString());
	    }
	}
	
	public String normalize(String necklace) {
		
		List<RotationStatus> indeces = new ArrayList<>();
		char smallestChar = 'Z';
		for(int i=0; i < necklace.length();i++) {
			if(necklace.charAt(i) < smallestChar) {
				indeces.clear();
				RotationStatus status = new RotationStatus();
				status.index = i;
				status.value = new StringBuilder(necklace);
				indeces.add(status);
				smallestChar = necklace.charAt(i); 
			} else if (necklace.charAt(i) == smallestChar) {
				RotationStatus status = new RotationStatus();
				status.index = i;
				status.value = new StringBuilder(necklace);
				indeces.add(status);
				
			}
		}
		StringBuilder total = new StringBuilder(necklace + necklace + necklace);
		String test = total.toString();
		for(RotationStatus index:indeces)  {
			int internalIndex = index.index + necklace.length();
			int rightvalue = test.charAt(internalIndex);
			int leftvalue = test.charAt(internalIndex);
			for(int i =1;i < necklace.length() ; i++ ) {
				rightvalue += test.charAt(internalIndex + i);
				leftvalue += test.charAt(internalIndex - i);
				if(leftvalue == rightvalue) {
					index.value =  new StringBuilder(total.substring(internalIndex,
							internalIndex + necklace.length()));
					continue;
				} else if(leftvalue < rightvalue) {
					index.value =  new StringBuilder(total.substring(internalIndex+1,
							internalIndex + necklace.length()+1)).reverse();
					break;
				}
				index.value =  new StringBuilder(total.substring(internalIndex,
						internalIndex + necklace.length()));
				break;
			}
			
		}
		Collections.sort(indeces, new CustomComparator());
		
		for(RotationStatus index:indeces) {
			System.out.println(index.value);
		}
		return indeces.get(0).value.toString();
		
	}

}
