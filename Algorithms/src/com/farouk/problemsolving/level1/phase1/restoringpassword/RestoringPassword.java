/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.restoringpassword;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class RestoringPassword {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String password = sc.nextLine();
		Map<String, Integer> digits = new HashMap<>();
		String decryptedPass = "";
		for (int i = 0; i < 10; i++)
			digits.put(sc.nextLine(), i);

		for (int j = 0; j < password.length(); j += 10) {
			String digit = password.substring(j, j + 10);
			decryptedPass += digits.get(digit);
		}
		System.out.println(decryptedPass);
	}
}
