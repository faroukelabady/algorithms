/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.football;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Football {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String teamMembers = sc.nextLine();
		boolean dangerousFlag = false;
		int count = 0;
		for(int i=0; i < teamMembers.length() - 1;i++) {
			if(teamMembers.charAt(i) == teamMembers.charAt(i+1)){
				count += 1;
				if(count >= 6) {
					dangerousFlag = true;
				}
			}
			else {
				count =0 ;
			}
		}
		
		if(dangerousFlag) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}
	}

}
