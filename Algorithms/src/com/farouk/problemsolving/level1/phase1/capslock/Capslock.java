package com.farouk.problemsolving.level1.phase1.capslock;

import java.util.Scanner;



public class Capslock {


	public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in);
		char[] chArray = null;
		String input = sc.nextLine();
		int upper = 0;
		for(Character ch: input.toCharArray()) {
			if(Character.isUpperCase(ch)) upper++;
		}
		
		if(upper == input.length() || (upper == input.length() - 1 && Character.isLowerCase(input.toCharArray()[0]))) {
			chArray = input.toLowerCase().toCharArray();
			if(upper == input.length() - 1)
			chArray[0] -= 0X20;
			System.out.println(new String(chArray));
		} else {
			System.out.println(input);
		}
	}
}
