package com.farouk.problemsolving.level1.phase1.HQ9;

import java.util.Scanner;

public class Joke {


	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String input = sc.nextLine();
		boolean found = false;
		for(char ch: input.toCharArray()) {
			if(ch == 'H' || ch == 'Q' || ch == '9') {
				found = true;
				System.out.println("YES");
				break;
			}
		}
		if(!found) {
			System.out.println("NO");
		}

	}

}
