/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.twins;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Twins {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Integer total = new Integer(sc.nextLine());
		List<Integer> coins = new ArrayList<Integer>();
		while (coins.size() < total)
		coins.add(sc.nextInt());
		Collections.sort(coins);
		int sum =0;
		int mySum = 0;
		int takenCoins = 0;
		for(Integer coin: coins) {
			sum = sum + coin;
		}
		for(int i = total-1; i >= 0 ; i--) {
			mySum += coins.get(i);
			takenCoins++;
			if(mySum > (sum - mySum)) {
				break;
			}
		}
		
		System.out.println(takenCoins);		

	}

}
