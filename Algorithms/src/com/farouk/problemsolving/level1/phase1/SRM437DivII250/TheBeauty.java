package com.farouk.problemsolving.level1.phase1.SRM437DivII250;


import java.util.HashSet;
import java.util.Set;

/**
 * 
 * @author Farouk Elabady
 *
 *Problem Statement
    
There is nothing more beautiful than just an integer number.
The beauty of an integer is the number of distinct digits it contains in decimal notation.
You are given an integer number n. Return the beauty of this number.
Definition
    
Class:TheBeauty
Method:find
Parameters:int
Returns:int
Method signature:int find(int n)
(be sure your method is public)
Limits
    
Time limit (s):2.000
Memory limit (MB):64
Constraints
-n will be between 1 and 1,000,000,000, inclusive.
Examples
0)
   
7
Returns: 1
Just one digit.
1)
  
100
Returns: 2
Two distinct digits - 0 and 1.
2)
   
123456789
Returns: 9
All the digits are different.
3)
    
1000000000
Returns: 2

4)
    
932400154
Returns: 7
 */
public class TheBeauty {

	public int find(int n) {
		Set<Integer> digitNumbers = new HashSet<>();
		int base = n;
		do {
			int val1 = base / 10;
			int val2 = val1 * 10;
			int result = base - val2;
			digitNumbers.add(result);
			
			base = val1;
		} while(base != 0);
		
		return digitNumbers.size();
	}

}
