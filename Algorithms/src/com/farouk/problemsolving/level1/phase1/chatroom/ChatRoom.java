/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.chatroom;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class ChatRoom {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String word = sc.nextLine();
		boolean wordFound = false;
		String hello = "hello";
		String found = "";
		int internalIndex=0;
		for(int i =0; i < hello.length(); i++) {
			for(int j =internalIndex; j < word.length(); j++) {
				if(hello.charAt(i) == word.charAt(j)) {
					found +=word.charAt(j);
					internalIndex = j + 1;
					break;
				}
			}
		}
		if(found.equals(hello)) {
			wordFound = true;
		}
		
		
		if(wordFound) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}

	}

}
