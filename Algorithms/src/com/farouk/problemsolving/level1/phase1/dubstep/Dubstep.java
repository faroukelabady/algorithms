/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.dubstep;

import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Dubstep {
	
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String song = sc.nextLine();
		
		String notmixedSong = song.replaceAll("WUB", " ").trim();
		
		System.out.println(notmixedSong);
	}

}
