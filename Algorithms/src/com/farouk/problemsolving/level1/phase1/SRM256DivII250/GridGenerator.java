/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.SRM256DivII250;

/**
 * @author Farouk Elabady
 *Problem Statement
    
Consider the following grid of numbers:
 1 0  3  4   1
 4 5  8  15  20
 1 10 23 46  81
 0 11 44 113 240
 3 14 69 226 579
Aside from the top row and left column, each number is equal to the sum of the three numbers immediately left, above, 
and above-left of it. You will be given a int[], row, representing the first row of a similar grid,
 and a int[], col, representing the first column of the grid. Your task is to return
  the value of the lower rightmost location when the values are calculated in the same way. Hence, 
  the above example would be represented by the input row = {1,0,3,4,1}, col = {1,4,1,0,3}.
Definition
    
Class:GridGenerator
Method:generate
Parameters:int[], int[]
Returns:int
Method signature:int generate(int[] row, int[] col)
(be sure your method is public)
Limits
Time limit (s):
2.000
Memory limit (MB):
64
Constraints
-row and col will contain the same number of elements.
-row and col will contain between 2 and 10 elements, inclusive.
-Each element of row and col will be between 0 and 9, inclusive.
-The first element of row will be the same as the first element of col.
Examples
0)
{1,0,3,4,1}
{1,4,1,0,3}
Returns: 579
The example above.
1)
{9,9,9,9,9,9,9,9,9,9}
{9,9,9,9,9,9,9,9,9,9}
Returns: 13163067
The largest possible return.
2)
{0,0,0,0,0,0,0,0,0}
{0,0,0,0,0,0,0,0,0}
Returns: 0

 */
public class GridGenerator {

	/**
	 * 
	 */
	public GridGenerator() {
		// TODO Auto-generated constructor stub
	}
	
	public int generate(int[] row, int[] col) {
		
		int[][] grid = new int[row.length][col.length];
		for(int i = 0; i < row.length; i++)
			grid[i][0] = row[i];
		for(int i = 0; i < col.length; i++)
			grid[0][i] = col[i];
		
		for(int i = 1; i < col.length; i++) 
			for(int j = 1; j < row.length; j++)
				grid[j][i] = grid[j-1][i-1] + grid[j-1][i] + grid[j][i-1];
		System.out.println(grid[row.length-1][col.length-1]);
		
		return grid[row.length-1][col.length-1];
		
	}

}
