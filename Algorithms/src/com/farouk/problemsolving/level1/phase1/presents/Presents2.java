/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.presents;

import java.util.HashMap;
import java.util.Map;
import java.util.Scanner;

/**
 * @author Farouk Elabady
 *
 */
public class Presents2 {

	/**
	 * 
	 */
	public Presents2() {
		// TODO Auto-generated constructor stub
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		Integer numOfFriends = new Integer(sc.nextLine());
		int[] fToF = new int[numOfFriends];
		for(int i=0;i<numOfFriends;++i) {
			int x = sc.nextInt();
			fToF[--x] = i + 1;
		}
		
		for(int i = 0; i < numOfFriends; i++) {
			System.out.print(fToF[i] + " ");
		}

	}

}
