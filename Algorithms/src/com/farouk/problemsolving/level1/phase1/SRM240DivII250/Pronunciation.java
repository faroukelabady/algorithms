/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.SRM240DivII250;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Farouk Elabady
 *Problem Statement
    
Peter has problems with pronouncing difficult words. In particular he can't pronounce words that
 contain three or more consecutive consonants (such as "street" or "first"). Furthermore he can't
  pronounce words that contain two or more consecutive vowels that are different (such as "goal" or "beauty").
   He can pronounce words with two consecutive equal vowels though (such as "need").  Is this problem we consider
    the 'y' to be always a consonant, even in words like "any". So the vowels are 'a', 'e', 'i', 'o' and 'u'.
     You are given a String[] words. If Peter can pronounce all the words, return an empty String; otherwise
      return the first word he can't pronounce.
Definition
    
Class:
Pronunciation
Method:
canPronounce
Parameters:
String[]
Returns:
String
Method signature:
String canPronounce(String[] words)
(be sure your method is public)
Limits
    
Time limit (s):
2.000
Memory limit (MB):
64
Constraints
-
words contains between 1 and 50 elements, inclusive.
-
Each element of words has length between 1 and 50, inclusive.
-
Each element of words consists of upper- and lowercase letters.
Examples
0)

    
{"All","of","these","are","not","difficult"}
Returns: ""

1)

    
{"The","word","REALLY","is","really","hard"}
Returns: "REALLY"

2)

    
{"TRiCKy"}
Returns: "TRiCKy"
Since the 'y' is a consonant, this word contain three consecutive consonants.
3)

    
{"irresistable","prerogative","uttermost","importance"}
Returns: ""
Peter can pronounce these words.
4)

    
{"Aa"}
Returns: ""

This problem statement is the exclusive and proprietary property of TopCoder, 
Inc. Any unauthorized use or reproduction of this information without the prior written consent of
 TopCoder, Inc. is strictly prohibited. (c)2003, TopCoder, Inc. All rights reserved.
 */
public class Pronunciation {
	
	public String canPronounce(String[] words) {
		
		String[] vowels = {"ae","ai","ao","au", "ea","ia","oa","ua"
				,"ei","eo","eu","ie","oe","ue","io","iu","oi","ui","ou","uo"};
		String firstWord = "";
		for(String word:words) {
			String temp = word.toLowerCase();
			int consonantCount = 0;
			int vowelsCount = 0;
			Matcher m = Pattern.compile("[b-df-hj-npr-tvx-z][b-df-hj-npr-tvx-z][b-df-hj-npr-tvx-z]").matcher(temp);
			while (m.find()) {
				consonantCount++;
			}
			
			if(consonantCount >= 1) {
				firstWord = word;
				break;
			}
			
			for(String vowel: vowels) {
				if(temp.indexOf(vowel) > -1)
					vowelsCount++;
			}
			
			if(vowelsCount >= 1) {
				firstWord = word;
				break;
			}
			
		}
		return firstWord;
		
	}

}
