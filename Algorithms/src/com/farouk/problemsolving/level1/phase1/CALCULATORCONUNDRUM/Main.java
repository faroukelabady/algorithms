/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.CALCULATORCONUNDRUM;

import java.io.IOException;
import java.util.Set;
import java.util.TreeSet;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	  static String ReadLn (int maxLg)  // utility function to read from stdin
	    {
	        byte lin[] = new byte [maxLg];
	        int lg = 0, car = -1;
	        String line = "";

	        try
	        {
	            while (lg < maxLg)
	            {
	                car = System.in.read();
	                if ((car < 0) || (car == '\n')) break;
	                lin [lg++] += car;
	            }
	        }
	        catch (IOException e)
	        {
	            return (null);
	        }

	        if ((car < 0) && (lg == 0)) return (null);  // eof
	        return (new String (lin, 0, lg));
	    }
	    
	    void Begin()
	    {
	    	Integer t = new Integer(Main.ReadLn (255));
			
			StringBuilder out = new StringBuilder("");
	    	for(int i = 0 ; i < t;i++) {
	    		String data = Main.ReadLn (255);
	    		int n = new Integer(data.split(" ")[0]);
	    		Long k = new Long(data.split(" ")[1]);
	    		Set<Long> values = new TreeSet<>();
	    		long max = Long.MIN_VALUE;
	    		do {
	    			values.add(k);
	    			max = Math.max(max, k);
	    			k = k * k;
	    			//k = new Long(k.toString().substring(0, n < k.toString().length()? n : k.toString().length() ));
	    			 int tempLen = (k + "").length();
	                 if (tempLen > n)
	                     k /= (int) (Math.pow(10, tempLen - n));
	                 
	    		}while(!values.contains(k));
	    		out.append("" + max + "\n");
	    	}
	    	
    		System.out.println(out);
	    	
			
	    }
		public static void main(String[] args) {
			try {
			Main game  = new Main();
			game.Begin();
			} catch (Exception e) {
				e.printStackTrace();
			}
		}

}
