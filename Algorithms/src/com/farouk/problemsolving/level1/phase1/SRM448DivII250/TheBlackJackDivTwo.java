/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.SRM448DivII250;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * @author Farouk Elabady
 *
 */
public class TheBlackJackDivTwo {
	
	public int score(String[] cards) {
		int score = 0;
		for(String card: cards) {
			Matcher m = Pattern.compile("[1-9]").matcher(card);
			if(m.find()) {
				score += new Integer(m.group());
			}
			m = Pattern.compile("[JKQT]").matcher(card);
			if(m.find()) {
				score += 10;
			}
			m = Pattern.compile("[A]").matcher(card);
			if(m.find()) {
				score += 11;
			}
		}
		System.out.println(score);
		return score;
 	}

}
