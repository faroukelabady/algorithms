/**
 * 
 */
package com.farouk.problemsolving.level1.phase1.pennygame;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.Map;

/**
 * @author Farouk Elabady
 *
 */
public class Main {

	
    static String ReadLn (int maxLg)  // utility function to read from stdin
    {
        byte lin[] = new byte [maxLg];
        int lg = 0, car = -1;
        String line = "";

        try
        {
            while (lg < maxLg)
            {
                car = System.in.read();
                if ((car < 0) || (car == '\n')) break;
                lin [lg++] += car;
            }
        }
        catch (IOException e)
        {
            return (null);
        }

        if ((car < 0) && (lg == 0)) return (null);  // eof
        return (new String (lin, 0, lg));
    }
    
    void Begin()
    {
        
    	Integer p = new Integer(Main.ReadLn (255));
    	String[] datasets = new String[p];
		
		for(int i = 0; i < p; i++) {
			Integer index = new Integer(Main.ReadLn (255));
			datasets[index-1] = Main.ReadLn (255);
		}
		
		for(int j = 0;j < datasets.length; j++){
			Map<String, Integer> values =  new LinkedHashMap<String, Integer>();
			for(int i = 0;i < datasets[j].length() - 2;i++){
				String data = datasets[j].substring(i, i + 3);
				values.put(data, values.get(data)==null? 1:values.get(data)+1) ;
			}
			System.out.print("" + (j+1));
			for(String key: values.keySet()) {
				System.out.print(" " + values.get(key));
			}
			System.out.println();
		}
    }
	public static void main(String[] args) {
		try {
		Main game  = new Main();
		game.Begin();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

}
