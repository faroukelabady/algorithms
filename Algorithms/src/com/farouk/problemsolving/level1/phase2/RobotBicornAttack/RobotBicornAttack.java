package com.farouk.problemsolving.level1.phase2.RobotBicornAttack;

import java.math.BigDecimal;
import java.util.Scanner;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class RobotBicornAttack {

	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		String amount = sc.nextLine();

		long max = -1;
		BigDecimal limit = new BigDecimal("1000000");
		Pattern p = Pattern.compile("[1-9]+");
		Matcher m = p.matcher(amount);


		for (int i = 1; i < amount.length(); i++) {
			for (int j = i; j < amount.length(); j++) {
				BigDecimal first = new BigDecimal(score(amount.substring(0, j)));
					if(first.compareTo(new BigDecimal("-1")) == 0)
						continue;
				for(int k = j+1; k < amount.length(); k++) {
					BigDecimal second = new BigDecimal(score(amount.substring(j,k)));
					BigDecimal third = new BigDecimal(score(amount.substring(k)));
					if(second.longValue() >= 0 && third.longValue() >=0) {
						BigDecimal val = first.add(second).add(third);
							long t = val.longValue();
							max = Math.max(max, t);
					}
					
				}
			}
		}
		System.out.println(max);
	}

	public static long score(String s) {

		if (s.length() > 1 && s.startsWith("0"))
			return -1;
		if (s.length() > 7)
			return -1;
		long val = new Long(s);
		return val > 1000000 ? -1 : val;
	}

}
