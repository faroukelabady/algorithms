/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.SRM330DivII250;

/**
 * @author Farouk Elabady
 *
 */
public class Sortness {
	public double getSortness(int[] a) {
		double sortness = 0.0;
		boolean swapped = true;
		int[] results = new int[a.length];
		int result = 0;
		do {
			swapped= false;
			for(int i = 1; i < a.length; i++) {
				if(a[i-1] > a[i]) {
					//results[a[i-1]-1]++;
					//results[a[i]-1]++;
					result+=2;
					int temp = a[i-1];
					a[i-1] = a[i];
					a[i] = temp;
					swapped = true;
				}
			}
		}while(swapped);
		
//		double sum = 0;
//		for(int o:results) {
//			sum+= o;
//		}
		
		sortness = (double)result/results.length;
		System.out.println(sortness);
		return sortness;
		
	}
	
	
}
