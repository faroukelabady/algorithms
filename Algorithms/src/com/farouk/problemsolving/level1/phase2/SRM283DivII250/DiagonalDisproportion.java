/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.SRM283DivII250;

/**
 * @author Farouk Elabady
 *
 */
public class DiagonalDisproportion {
	
	public int getDisproportion(String[] matrix) {
		int diagSum = 0;
		
		for(int i = 0; i < matrix.length; i++) {
			diagSum += (matrix[i].charAt(i) - matrix[i].charAt(matrix.length - 1 -i));
			
		}
		System.out.println(diagSum);
		
		return diagSum;
		
	}

}
