/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.SRM457DivII250;

import java.util.Arrays;

/**
 * @author Farouk Elabady
 *
 */
public class TheSquareDivTwo {

	public String[] solve(String[] board) {

		String[] result = new String[board.length];

		for (String row : board) {
			char[] chars = row.toCharArray();
			Arrays.sort(chars);
			String sortedRow = new String(chars);
			for (int i = 0; i < result.length; i++) {
				if (result[i] == null) {
					result[i] = "";
				}

				result[i] += (sortedRow.charAt(i));

			}
		}

//		Arrays.sort(result);

		for (String test : result)
			System.out.println(test);
		return result;

	}

}
