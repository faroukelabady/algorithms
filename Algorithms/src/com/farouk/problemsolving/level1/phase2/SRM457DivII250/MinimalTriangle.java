/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.SRM457DivII250;


/**
 * @author Farouk Elabady
 *
 */
public class MinimalTriangle {
	
	public double maximalArea(int length) {
		
		double hexagonArea = (3 * Math.sqrt(3)/2)* length * length;
		double missingTriaSide = (length * Math.sin(Math.toRadians( 120 ))/Math.sin(Math.toRadians( 30 )));
		double s = (2 *length + missingTriaSide) / 2;
		double value = s *(s-length) * (s- length) * (s-missingTriaSide);
		double triangleArea = Math.sqrt(value);
		double firstMin = Math.min(triangleArea, (hexagonArea - 3 * triangleArea));
		double secondMin = Math.min(triangleArea, (hexagonArea - 2 * triangleArea)/2);
		double maximalArea = Math.max(firstMin, secondMin);
		return maximalArea;
	}

}
