/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.Letter;

import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class Letter {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringTokenizer tokenizer = new StringTokenizer(sc.nextLine());
		int lines = new Integer(tokenizer.nextToken());
		int columns = new Integer(tokenizer.nextToken());
		
		List<String> data = new ArrayList<>();
		for(int i =0; i < lines ; i++) {
			String input = sc.nextLine();
			data.add(input);
		}
		
		for(int i =0;;) {
			String row = data.get(i);
			if(row.indexOf('*') == -1) {
				data.remove(i);
			} else {
				break;
			}
		}
		
		for(int i =data.size()-1; i > 0; i--) {
			String row = data.get(i);
			if(row.indexOf('*') == -1) {
				data.remove(i);
			} else {
				break;
			}
		}
		
		for(; ; ) {
			boolean flag1 = true;
			boolean flag2 = true;
			for(int i =0; i < data.size(); i++) {
				if(data.get(i).charAt(0) == '*')
					flag1 = false;
				
				if(data.get(i).charAt(data.get(i).length()-1) == '*')
					flag2 = false;
			}
			if(flag1) {
				for(int i =0; i < data.size(); i++) {
					data.set(i, data.get(i).substring(1));
				}
			} 
			if(flag2) {
				for(int i =0; i < data.size(); i++) {
					data.set(i, data.get(i).substring(0,data.get(i).length()-1));
				}
			} 
			if(!flag2 && !flag1)
				break;
		}
		
		for(String d : data) {
			System.out.println(d);
		}
		
		

	}

}
