/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.dominopiling;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class DominoPilling {
	
	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringTokenizer tokenizer = new StringTokenizer(sc.nextLine());
		Integer rows = new Integer(tokenizer.nextToken());
		Integer columns = new Integer(tokenizer.nextToken());
		int area = rows* columns;
		int totalDominos = 0;
		if(area%2 == 0 ){
			totalDominos = area /2;
		} else {
			totalDominos = (area-1)/2;
		}
		System.out.println(totalDominos);

	}

}
