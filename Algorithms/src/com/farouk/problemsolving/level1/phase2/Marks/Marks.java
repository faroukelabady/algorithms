/**
 * 
 */
package com.farouk.problemsolving.level1.phase2.Marks;

import java.util.Scanner;
import java.util.StringTokenizer;

/**
 * @author Farouk Elabady
 *
 */
public class Marks {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringTokenizer tokenizer = new StringTokenizer(sc.nextLine());
		Integer students = new Integer(tokenizer.nextToken());
		Integer subjects = new Integer(tokenizer.nextToken());
		int[][] marks = new int[students][subjects];
		int[] maxvalues =  new int[subjects];
		for(int i =0; i < students; i++) {
			String[] values = sc.nextLine().split("(?<=.)");
			for(int j =0; j < subjects; j++) {
				marks[i][j] = new Integer(values[j]);
					maxvalues[j] = Math.max(marks[i][j],maxvalues[j]);
			}
		}
		
		int c=0;
		for(int i =0; i < students; i++) {
			int suc = 0;
			for(int j =0; j < subjects; j++) {
				if(marks[i][j] == maxvalues[j]) {
					suc++;
				}
			}
			if(suc>0) {
				c++;
			}
			
		}
		
		System.out.println(c);

	}

}
