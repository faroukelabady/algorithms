package com.farouk.problemsolving.level1.phase2.luckydivision;

import java.util.Scanner;
import java.util.StringTokenizer;

public class LuckyDivision {
	
	public static void main(String[] args) {
		Scanner sc = new Scanner(System.in);
		StringTokenizer tokenizer = new StringTokenizer(sc.nextLine());
		Integer number = new Integer(tokenizer.nextToken());
		String four = "4";
		String seven = "7";
		boolean lucky = false;
		for(int i = 0 ; i < 3 ; i++) {
			Integer value1 = new Integer(four);
			Integer value2 = new Integer(seven);
			
			if(number%value1 == 0 || number%value2 ==0) {
				lucky =true;
				break;
			}
			four += "7";
			seven += "4";
		}
		
		if(lucky) {
			System.out.println("YES");
		} else {
			System.out.println("NO");
		}

	}


}
