/**
 * 
 */
package com.farouk.problemsolving;

import com.farouk.problemsolving.level1.phase1.SRM240DivII250.Pronunciation;
import com.farouk.problemsolving.level1.phase1.SRM288DivII250.AccountBalance;
import com.farouk.problemsolving.level1.phase1.SRM328DivII250.MarbleNecklace;
import com.farouk.problemsolving.level1.phase1.SRM332DivII250.TextStatistics;
import com.farouk.problemsolving.level1.phase1.SRM350DivII250.DistanceBetweenStrings;
import com.farouk.problemsolving.level1.phase1.SRM437DivII250.TheBeauty;
import com.farouk.problemsolving.level1.phase1.SRM443DivII250.SoccerLeagues;
import com.farouk.problemsolving.level1.phase1.SRM448DivII250.TheBlackJackDivTwo;
import com.farouk.problemsolving.level1.phase2.SRM283DivII250.DiagonalDisproportion;
import com.farouk.problemsolving.level1.phase2.SRM330DivII250.Sortness;
import com.farouk.problemsolving.level1.phase2.SRM457DivII250.MinimalTriangle;
import com.farouk.problemsolving.level1.phase2.SRM457DivII250.TheSquareDivTwo;

/**
 * @author Farouk Elabady
 *
 *
 */
public class Tester {


	public Tester() {
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		TheSquareDivTwo insider = new TheSquareDivTwo();
		insider.solve(new String[]{"C.C..C.C..C..C.",
				 "CCC...C..CCC.C.",
				 "......CC...CCCC",
				 ".C..CC.C.C.C.C.",
				 "C....C.C......C",
				 ".....C..CCCCC.C",
				 "CCC.......CCCCC",
				 "..C.C..C.C...C.",
				 "CCC....CCC.CC..",
				 "CC.CCCC.CCCC...",
				 ".C..C.CC.C.CC.C",
				 "C.CCCC..CC..C.C",
				 ".CCCC.CCCCCC...",
				 "..C...C.CCC.CC.",
				 "CCCC..CCC.C...."});
	}

}
