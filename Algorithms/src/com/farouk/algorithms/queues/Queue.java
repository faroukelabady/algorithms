/**
 * 
 */
package com.farouk.algorithms.queues;

import java.util.Iterator;

/**
 * @author faroukElabady
 *	FIFO queue
 */
public class Queue<Item> implements Iterable<Item> {
	
	private Node first; 
	private Node last;
	private int N; 
	
	private class Node {
		Item item; 
		Node next;
	}
	
	public boolean isEmpty() {return first == null;}

	public int size() {return N;}
	
	
	public void enqueue(Item item) {
		
		Node oldLast = last;
		last = new Node();
		last.item = item;
		last.next = null;
		if(isEmpty()) first = last;
		else oldLast.next = last;
		N++;
	}
	
	public Item dequeue() {
		
		Item item  = first.item;
		first = first.next;
		if(isEmpty()) last = null;
		N--;
		return item;
		
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item> {

		private Node current = first;
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public Item next() {
			Item item = current.item;
			current = current.next;
			return item;
		}
		
		 
	}

}
