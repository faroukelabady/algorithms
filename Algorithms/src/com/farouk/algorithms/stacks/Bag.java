/**
 * 
 */
package com.farouk.algorithms.stacks;

import java.util.Iterator;

/**
 * @author faroukElabady
 *
 */
public class Bag<Item> implements Iterable<Item> {

	public Node first;

	private class Node {

		Item item;
		Node next;
	}

	public void add(Item item) {

		Node oldFirst = first;
		first = new Node();
		first.item = item;
		first.next = oldFirst;
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}

	private class ListIterator implements Iterator<Item> {

		private Node current = first;

		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public Item next() {
			Item item = current.item;
			current = current.next;
			return item;
		}

	}

}
