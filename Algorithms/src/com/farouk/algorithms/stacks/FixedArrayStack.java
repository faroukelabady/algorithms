/**
 * 
 */
package com.farouk.algorithms.stacks;

import java.util.Iterator;

/**
 * @author faroukElabady
 *
 */
public class FixedArrayStack<Item> implements Iterable<Item> {
	
	private Item[] a;
	private int N;
	
	public FixedArrayStack(int cap) {
		
		a = (Item[]) new Object[cap];
	}
	
	public boolean isEmpty(){ return N ==00 ;}
	
	public boolean isFull() { return N == a.length;}
	
	public void push(Item item) {
		
		a[N++] = item;
	}
	
	public Item pop() {
		Item item  = a[--N];
		a[N] = null;
		return item;
	}

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return null;
	}

}
