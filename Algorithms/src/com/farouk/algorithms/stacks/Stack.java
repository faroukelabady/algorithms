/**
 * 
 */
package com.farouk.algorithms.stacks;

import java.util.Iterator;

/**
 * @author faroukElabady
 *
 */
public class Stack<Item> implements Iterable<Item> {
	
	public Node first;
	public int N;
	
	private class Node {
		
		Item item;
		Node next;
	}
	
	public boolean isEmpty() { return first == null;}
	
	public int size() {return N;}
	
	public void push(Item item) {
		
		Node oldfirst  = first; 
		first = new Node();
		first.item = item;
		first.next = oldfirst;
		N++;
	}
	
	public Item pop() {
		
		Item item = first.item;
		first = first.next;
		N--;
		return item;
		
	}
	
	
	
	

	@Override
	public Iterator<Item> iterator() {
		// TODO Auto-generated method stub
		return new ListIterator();
	}
	
	private class ListIterator implements Iterator<Item> {

		private Node current = first;
		@Override
		public boolean hasNext() {
			// TODO Auto-generated method stub
			return current != null;
		}

		@Override
		public Item next() {
			Item item = current.item;
			current = current.next;
			return item;
		}
		
		 
	}

}
