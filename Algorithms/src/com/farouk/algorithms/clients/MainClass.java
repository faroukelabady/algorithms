/**
 * 
 */
package com.farouk.algorithms.clients;

import com.farouk.algorithms.stacks.Stack;
import com.library.StdOut;

/**
 * @author faroukElabady
 *
 */
public class MainClass {

	/**
	 * 
	 */
	public MainClass() {
		// TODO Auto-generated constructor stub
	}
	
	private class Node {
		
		int num;
		Node next;
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		permutation("", "ABCD");
//		Node first = null;
//		MainClass test = new MainClass();
//		for(int i = 0; i < 10; i++) {
//			Node oldfirst  = first; 
//			first = test.new Node();
//			int val  = 0 + (int)(Math.random() * ((10 - 0) + 1));
//			System.out.print(" - "  + val);
//			first.num = val;
//			first.next = oldfirst;
//		}
//		System.out.println();
//		System.out.println("Max number");
//		System.out.println(max(first));

	}
	
    private static void permutation(String prefix, String str){
        int n = str.length();
        if (n == 0) 
            System.out.println(prefix);
        else {
            for (int i = 0; i < n; i++)
                permutation(prefix + str.charAt(i), 
            str.substring(0, i) + str.substring(i+1));
        }
    }
	
	public static int max(Node first) {
		if(first == null) return 0;
		int max = first.num;
		if(first==null || first.next == null) return max;
		if(max >= first.next.num) {
			first.next = first.next.next;
			max = max(first);
		} else {
			max = max(first.next);
		}
		return max;
		
	}
	
	public static void Parenthess() {
		String test = "1+2)*3-4)*5-6)))";
		Stack<String> psStack = new Stack<>(); 
		boolean order  = true;
		for (char st : test.toCharArray()) {
			
			if(st == ')') {
				String result = "";
				for(int i =0; i < psStack.size(); i++){
					String out = psStack.pop();
					if(out.equals("+") || out.equals("-") || out.equals("*") || out.equals("/")){
						String otherNum = psStack.pop();
						psStack.push("(" + otherNum + out + result + ")");
						break;
					} else {
						result += out;
					}
				}
			} else {
				psStack.push("" + st);
			}
				

		}
		for(int i =0; i < psStack.size(); i++)
		StdOut.println("order perserved " + psStack.pop());
	}

}
