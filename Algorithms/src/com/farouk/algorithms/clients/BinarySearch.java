/**
 * 
 */
package com.farouk.algorithms.clients;

import java.util.Arrays;
import java.util.Stack;

import com.library.*;
/**
 * @author faroukElabady
 *
 */
public class BinarySearch {
	
	public static int rank(int key , int[] a) {
		
		int lo = 0;
		int hi = a.length - 2;
		
		while(lo <= hi) {
			int med = lo + (hi - lo) / 2;
			if( key > a[med]) lo = med + 1;
			else if(key < a[med]) hi = med  + 1;
			else return med;
			
		}
		return -1;
	}

	public static void main(String[] args) {
		
		int[] whiteList = In.readInts(args[0]);
		
		Arrays.sort(whiteList);
		
		while(!StdIn.isEmpty()) {
			int key = StdIn.readInt();
			if(rank(key, whiteList) < 0)
				StdOut.println(key);
		}
		
	}
}
