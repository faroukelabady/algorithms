package binarytree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Deque;
import java.util.HashSet;
import java.util.List;
import java.util.Queue;
import java.util.Set;
import java.util.Stack;

class Node {
	int data;
	Node left, right;

	Node(int item) {
		data = item;
		left = right = null;
	}

	@Override
	public String toString() {
		return "Node [data=" + data + ", left=" + left + "]";
	}

}

public class GfG {

	void printSpiral(Node node) {
		int h = height(node);
		int i;

		/*
		 * ltr -> left to right. If this variable is set then the given label is
		 * transversed from left to right
		 */
		boolean ltr = false;
		for (i = 1; i <= h; i++) {
			printGivenLevel(node, i, ltr);

			/* Revert ltr to traverse next level in opposite order */
			ltr = !ltr;
		}

	}

	/*
	 * Compute the "height" of a tree -- the number of nodes along the longest path
	 * from the root node down to the farthest leaf node.
	 */
	int height(Node node) {
		if (node == null)
			return 0;
		else {

			/* compute the height of each subtree */
			int lheight = height(node.left);
			int rheight = height(node.right);

			/* use the larger one */
			if (lheight > rheight)
				return (lheight + 1);
			else
				return (rheight + 1);
		}
	}

	/* Print nodes at a given level */
	void printGivenLevel(Node node, int level, boolean ltr) {
		if (node == null)
			return;
		if (level == 1)
			System.out.print(node.data + " ");
		else if (level > 1) {
			if (ltr != false) {
				printGivenLevel(node.left, level - 1, ltr);
				printGivenLevel(node.right, level - 1, ltr);
			} else {
				printGivenLevel(node.right, level - 1, ltr);
				printGivenLevel(node.left, level - 1, ltr);
			}
		}
	}

	void printSpiralStack(Node node) {

		if (node == null)
			return;

		Stack<Node> ltr = new Stack<>();
		Stack<Node> rtl = new Stack<>();

		rtl.add(node);

		while (!ltr.isEmpty() || !rtl.isEmpty()) {

			while (!rtl.isEmpty()) {
				Node temp = rtl.pop();

				System.out.println(temp.data);
				if (temp.right != null)
					ltr.add(temp.right);
				if (temp.left != null)
					ltr.add(temp.left);
			}

			while (!ltr.isEmpty()) {
				Node temp = ltr.pop();

				if (temp == null) {
					continue;
				}
				System.out.println(temp.data);

				if (temp.left != null)
					rtl.add(temp.left);
				if (temp.right != null)
					rtl.add(temp.right);
			}

		}

	}

	// void printSpiral(Node node) {
	//
	// List<Node> s = new ArrayList<>();
	// Queue<Node> q = new ArrayDeque<>();
	// s.add(node);
	// q.add(node);
	// Node current = null;
	// int size = s.size();
	// System.out.print(s.get(0).data);
	// boolean reverse = false;
	// while (!q.isEmpty()) {
	// if(s.size() == size * 2 + 1) {
	// if(reverse) {
	// for(int i = s.size() - 1; i >= size ; i--) {
	// System.out.print(" " + s.get(i).data);
	// }
	// } else {
	// for(int i = size; i <= s.size() - 1; i++) {
	// System.out.print(" " + s.get(i).data );
	// }
	// }
	// size = s.size();
	// reverse = !reverse;
	// }
	//
	// current = q.poll();
	//
	//
	// if (current.left == null && current.right == null) {
	// continue;
	// }
	//
	// if (!s.contains(current.left)) {
	// s.add(current.left);
	// q.add(current.left);
	// }
	//
	// if (!s.contains(current.right)) {
	// s.add(current.right);
	// q.add(current.right);
	// }
	// }
	//
	// if(size < s.size()) {
	// if(!reverse) {
	// for(int i = s.size() - 1; i >= size ; i--) {
	// System.out.print(" "+ s.get(i).data);
	// }
	// } else {
	// for(int i = size; i <= s.size() - 1; i++) {
	// System.out.print(" " + s.get(i).data);
	// }
	// }
	// }
	//
	//
	// }

	// int getTreeHeight(Node node) {
	//
	// if (node == null) {
	// return 0;
	// }
	//
	// return 1 + getTreeHeight(node.left);
	// }

	// void printSpiral(Node node)
	// {
	// if (node == null)
	// return; // NULL check
	//
	// // Create two stacks to store alternate levels
	// Stack<Node> s1 = new Stack<Node>();// For levels to be printed from right to
	// left
	// Stack<Node> s2 = new Stack<Node>();// For levels to be printed from left to
	// right
	//
	// // Push first level to first stack 's1'
	// s1.push(node);
	//
	// // Keep ptinting while any of the stacks has some nodes
	// while (!s1.empty() || !s2.empty())
	// {
	// // Print nodes of current level from s1 and push nodes of
	// // next level to s2
	// while (!s1.empty())
	// {
	// Node temp = s1.peek();
	// s1.pop();
	// System.out.print(temp.data + " ");
	//
	// // Note that is right is pushed before left
	// if (temp.right != null)
	// s2.push(temp.right);
	//
	// if (temp.left != null)
	// s2.push(temp.left);
	//
	// }
	//
	// // Print nodes of current level from s2 and push nodes of
	// // next level to s1
	// while (!s2.empty())
	// {
	// Node temp = s2.peek();
	// s2.pop();
	// System.out.print(temp.data + " ");
	//
	// // Note that is left is pushed before right
	// if (temp.left != null)
	// s1.push(temp.left);
	// if (temp.right != null)
	// s1.push(temp.right);
	// }
	// }
	// }

	public static void main(String[] args) {
		Node root = new Node(1);

		// root.left = new Node(20);
		// root.right = new Node(30);
		//
		// root.left.left = new Node(40);
		// root.left.right = new Node(60);

		root.left = new Node(2);
		root.right = new Node(3);

		root.left.left = new Node(7);
		root.left.right = new Node(6);
		root.right.left = new Node(5);
		root.right.right = new Node(4);

		// root.left.left.left = new Node(8);
		// root.left.left.right = new Node(9);
		//
		// root.left.right.left = new Node(10);
		// root.left.right.right = new Node(11);
		//
		// root.right.left.left = new Node(12);
		// root.right.left.right = new Node(13);
		//
		// root.right.right.left = new Node(14);
		// root.right.right.right = new Node(15);

		GfG f = new GfG();
		f.printSpiralStack(root);
	}

}
