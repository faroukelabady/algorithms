package binarytree;

public class TreeToDll {

	Node head;

//	Node bintree2listUtil(Node node) {
//		// Base case
//		if (node == null)
//			return node;
//
//		// Convert the left subtree and link to root
//		if (node.left != null) {
//			// Convert the left subtree
//			Node left = bintree2listUtil(node.left);
//
//			// Find inorder predecessor. After this loop, left
//			// will point to the inorder predecessor
//			for (; left.right != null; left = left.right)
//				;
//
//			// Make root as next of the predecessor
//			left.right = node;
//
//			// Make predecssor as previous of root
//			node.left = left;
//		}
//
//		// Convert the right subtree and link to root
//		if (node.right != null) {
//			// Convert the right subtree
//			Node right = bintree2listUtil(node.right);
//
//			// Find inorder successor. After this loop, right
//			// will point to the inorder successor
//			for (; right.left != null; right = right.left)
//				;
//
//			// Make root as previous of successor
//			right.left = node;
//
//			// Make successor as next of root
//			node.right = right;
//		}
//
//		return node;
//	}

	Node BuiltList(Node root) {
		if (root == null)
			return null;

		BuiltList(root.left);
		///
		Node leaf = new Node(root.data);
		if (head == null) {
			head = root;
		} else {
			head.right = leaf;
			leaf.left = head;
			head = leaf;
		}
		///
		BuiltList(root.right);
		return leaf;
	}

	Node BToDLL(Node root) {
		// Your code here
		BuiltList(root);

		Node last = getFirst(head);
//		last.left = head;
//		head.right = last;
		return last;
	}

	Node getFirst(Node root) {
		if (root.left == null) {
			return root;
		}
		return getFirst(root.left);
	}

	static void printList(Node print) {
		if (print == null)
			return;
		System.out.print(print.data + " ");
		printList(print.left);
	}

	public static void main(String[] args) {
		TreeToDll tree = new TreeToDll();

		// Let us create the tree shown in above diagram
		Node root = new Node(10);
		root.left = new Node(12);
		root.right = new Node(15);
		root.left.left = new Node(25);
		root.left.right = new Node(30);
		root.right.left = new Node(36);
		// tree.head = tree.BToDLL(root);
		// Convert to DLL
		tree.BToDLL(root);

		System.err.println(root);

		// Print the converted list
		tree.printList(tree.head);
	}

}
