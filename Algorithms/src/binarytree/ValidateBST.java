package binarytree;

import java.util.ArrayList;
import java.util.List;

class TreeNode {
	int data;
	TreeNode left;
	TreeNode right;

	TreeNode(int data) {
		this.data = data;
	}
}

public class ValidateBST {

	public static void main(String[] args) {
		TreeNode root = new TreeNode(20);
		root.left = new TreeNode(15);
		root.right = new TreeNode(40);
		root.left.left = new TreeNode(10);
		root.left.right = new TreeNode(30);
		validateBST(root);
	}

	public static boolean validateBST(TreeNode root) {
		//List<TreeNode> list = new ArrayList<>();
		//inOrder(root, list);
		return validateBST(root, Integer.MIN_VALUE,Integer.MAX_VALUE);
//		for(int i = 1; i < list.size(); i++) {
//			if(list.get(i-1).data > list.get(i).data) {
//				return false;
//			}
//		}
//		
//		return true;
		

	}

	public static void inOrder(TreeNode root, List<TreeNode> list) {
		if (root == null)
			return;

		inOrder(root.left, list);
		list.add(root);
		inOrder(root.right, list);

	}
	
	
	public static boolean validateBST(TreeNode root, int min, int max) {
	    if (root == null) {
	            return true;
	    }
	    if (root.data <= min || root.data >= max) {
	            return false;
	    }
	    // left subtree must be < root.val && right subtree must be > root.val
	    return validateBST(root.left, min, root.data) && validateBST(root.right, root.data, max);
	}
}
