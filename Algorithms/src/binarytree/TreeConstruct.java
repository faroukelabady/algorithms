package binarytree;


public class TreeConstruct {

	static int preIndex = 0;

	static Node buildTree(int in[], int pre[], int inStrt, int inEnd) {
		// add code here.
		if(inStrt > inEnd)
			return null;
		
		Node node = new Node(pre[preIndex++]);
		
		if(inStrt == inEnd)
			return node;
		
		int inIndex = search(in, inStrt, inEnd, node.data);
		
		node.left = buildTree(in, pre, inStrt, inIndex - 1);
		node.right = buildTree(in, pre, inIndex + 1, inEnd);
		
		return node;

	}
	
	static int search(int in[], int inStrt, int inEnd, int data ) {
		int i;
		for(i = inStrt; i <= inEnd; i++) {
			if(in[i] == data) {
				return i;
			}
		}
		
		return i;
	}
	
	/* This funtcion is here just to test buildTree() */
    void printInorder(Node node) 
    {
        if (node == null)
            return;
  
        /* first recur on left child */
        printInorder(node.left);
  
        /* then print the data of node */
        System.out.print(node.data + " ");
  
        /* now recur on right child */
        printInorder(node.right);
    }
    
    
    static Node buildTree2(int[] inOrder, int[] preOrder, int inStrt, int inEnd) {
    	
    	if(inStrt > inEnd)
    		return null;
    	
    	Node start = new Node(preOrder[preIndex++]);
    	
    	if(inStrt == inEnd)
    		return start;
    	
    	int inIndex = search2(inOrder, inStrt, inEnd, start.data);
    	
    	start.left = buildTree2(inOrder, preOrder, inStrt, inIndex - 1);
    	start.right = buildTree2(inOrder, preOrder, inIndex + 1, inEnd);
    	
    	return start;
    }
    
    static int search2(int[] inOrder, int start, int finish, int data ) {
    	int i= start;
    	for(i = start; i < finish; i++) {
    		if(inOrder[i] == data) {
    			return i;
    		}
    	}
    	
    	return i;
    }
  
    // driver program to test above functions
    public static void main(String args[]) 
    {
    	TreeConstruct tree = new TreeConstruct();
    	int[] in = new int[] { 3, 1, 4, 0, 5, 2, 6 };
		int[] pre = new int[] { 0, 1, 3, 4, 2, 5, 6 };
        int len = in.length;
        Node root = tree.buildTree(in, pre, 0, len - 1);
  
        // building the tree by printing inorder traversal
        System.out.println("Inorder traversal of constructed tree is : ");
        tree.printInorder(root);
    }

}
