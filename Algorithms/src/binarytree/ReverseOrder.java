package binarytree;

import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.List;
import java.util.Queue;

public class ReverseOrder {

	List<Node> s = new ArrayList<>();

	public static void main(String[] args) {
		ReverseOrder ro = new ReverseOrder();
		Node root = new Node(1);
		root.left = new Node(2);
		root.right = new Node(3);
		root.left.left = new Node(4);
		root.left.right = new Node(5);

		ro.reversePrint(root);
	}

	void reversePrint(Node node) {
		bfs(node);
		for (int i = s.size() - 1; i >= 0; i--) {
			System.out.print(s.get(i).data + (i == 0 ? "" : " "));
		}
	}

	void bfs(Node node) {

		Queue<Node> q = new ArrayDeque<>();

		s.add(node);
		q.add(node);

		while (!q.isEmpty()) {
			Node n = q.poll();

			if (n.right != null) {
				s.add(n.right);
				q.add(n.right);
			}

			if (n.left != null) {
				s.add(n.left);
				q.add(n.left);
			}

		}

	}

}
