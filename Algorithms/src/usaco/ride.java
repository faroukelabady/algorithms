package usaco;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.StringTokenizer;

/*
ID: Eclipse
LANG: JAVA
TASK: ride
*/

public class ride {

	public static void main(String[] args) throws IOException {
		 // Use BufferedReader rather than RandomAccessFile; it's much faster
	    BufferedReader f = new BufferedReader(new FileReader("ride.in"));
	    // input file name goes above
	    PrintWriter out = new PrintWriter(new BufferedWriter(new FileWriter("ride.out")));
	    // Use StringTokenizer vs. readLine/split -- lots faster
	    StringTokenizer st = new StringTokenizer(f.readLine());
							  // Get line, break into tokens
	    String comet = st.nextToken();    // first integer
	    st = new StringTokenizer(f.readLine());
	    String group = st.nextToken();    // second integer
	    int cometval = 1;
	    int groupval = 1;
	    for(char c: comet.toCharArray()) {
	    	cometval *= ( Character.getNumericValue(c) - 9);
	    }

	    for(char c: group.toCharArray()) {
	    	groupval *= ( Character.getNumericValue(c) - 9);
	    }

	    if((cometval % 47) == groupval % 47) {
	    	out.println("GO");                           // output result
	    } else {
	    	out.println("STAY");                           // output result
	    }


	    out.close();                                  // close the output file

	}

}
