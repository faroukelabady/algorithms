package amazon;

import java.util.Set;
import java.util.Stack;

public class AmazonClass {


	public static void main(String[] args) {
		//System.out.println(totalScore(new String[]{"Z", "1", "2", "+", "Z"}, 5));
		Movie A = new Movie(1, 6.2F);
		Movie B = new Movie(2, 3.6F);
		Movie C = new Movie(3, 2.4F);
		Movie D = new Movie(4, 9.8F);
		Movie E = new Movie(5, 5.1F);
		Movie F = new Movie(6, 8.4F);
		Movie G = new Movie(7, 8.4F);
		Movie H = new Movie(8, 8.0F);
		A.addSimilarMovie(B);
		A.addSimilarMovie(C);
		A.addSimilarMovie(H);
		A.addSimilarMovie(G);
		A.addSimilarMovie(F);
		B.addSimilarMovie(D);
		C.addSimilarMovie(D);

		Solution sol = new Solution();

		Set<Movie> m = sol.getMovieRecommendations(A, 4);

		m.forEach((e) -> System.out.println(e.getRating()));

	}


	// METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
	public static int totalScore(String[] blocks, int n)
	{
		int totalScore = 0;
		Stack<Integer> scores = new Stack<>();
		for(int i = 0; i < n; i++) {
			String scoreStr =  blocks[i];
			if(isInteger(scoreStr)) {
				Integer score = Integer.parseInt(scoreStr);
				scores.push(score);
				totalScore = totalScore + score;
			} else if(scoreStr.equalsIgnoreCase("Z")) {
				Integer lastscore = !scores.isEmpty() ? scores.pop() : 0;
				totalScore = totalScore - lastscore;
			} else if(scoreStr.equalsIgnoreCase("X")) {
				Integer lastscore = !scores.isEmpty() ? scores.peek() : 0;
				scores.push(lastscore * 2);
				totalScore = totalScore + lastscore * 2;
			} else if(scoreStr.equalsIgnoreCase("+")) {
				Integer lastscore = !scores.isEmpty() ? scores.peek() : 0;
				Integer lastscore2 = !scores.isEmpty() && scores.size() > 1  ? scores.get(scores.size() - 2) : 0;
				scores.push(lastscore + lastscore2);
				totalScore = totalScore + lastscore + lastscore2;
			}

		}

		return totalScore;
	}


	public static boolean isInteger(String s) {
	    return isInteger(s,10);
	}

	public static boolean isInteger(String s, int radix) {
	    if(s.isEmpty()) return false;
	    for(int i = 0; i < s.length(); i++) {
	        if(i == 0 && s.charAt(i) == '-') {
	            if(s.length() == 1) return false;
	            else continue;
	        }
	        if(Character.digit(s.charAt(i),radix) < 0) return false;
	    }
	    return true;
	}
}
