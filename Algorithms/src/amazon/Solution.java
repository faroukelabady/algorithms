package amazon;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashSet;
import java.util.Iterator;
import java.util.Set;
import java.util.TreeSet;

public class Solution {

	Set<Movie> movies = new TreeSet<>(new Comparator<Movie>() {

		@Override
		public int compare(Movie o1, Movie o2) {
		return Integer.compare(o2.getMovieId(), o1.getMovieId());
		}

	});

	// METHOD SIGNATURE BEGINS, THIS METHOD IS REQUIRED
	// RETURN AN EMPTY SET IF NO SIMILAR MOVIE TO THE GIVEN MOVIE IS FOUND
	public Set<Movie> getMovieRecommendations (Movie movie, int N)
	{
		traverseMovie(movie);
		ArrayList<Movie> orderedMovies = new ArrayList<>(movies);
		Collections.sort(orderedMovies, new Comparator<Movie>() {
			@Override
			public int compare(Movie o1, Movie o2) {
				return Float.compare(o2.getRating(), o1.getRating());
			}
		});
		if(movies.size() <= N ) {
			movies.remove(movie);
			return movies;
		}
		Set<Movie> highestRated = new HashSet<>();

		// WRITE YOUR CODE HERE
		Iterator<Movie> it = orderedMovies.iterator();
		while(it.hasNext()) {
			Movie current = it.next();
			if(N == 0) {
				break;
			}
			if(current.getMovieId() == movie.getMovieId())
				continue;

			highestRated.add(current);
			N--;
		}

		return highestRated;
	}

	public void traverseMovie(Movie movie) {
		movies.add(movie);
		if(movie.getSimilarMovies() == null ||movie.getSimilarMovies().size() == 0 )
			return;
		for(Movie similar: movie.getSimilarMovies()) {
			traverseMovie(similar);
		}
	}

}
