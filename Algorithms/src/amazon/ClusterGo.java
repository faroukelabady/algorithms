package amazon;

import java.util.ArrayList;
import java.util.List;

public class ClusterGo {

	int rows;
	int columns;

	public static void main(String[] args) {
		List<List<Integer>> grid = new ArrayList<>();
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());

		grid.get(0).add(1);
		grid.get(0).add(1);
		grid.get(0).add(0);
		grid.get(0).add(0);
		grid.get(1).add(0);
		grid.get(1).add(0);
		grid.get(1).add(0);
		grid.get(1).add(0);
		grid.get(2).add(0);
		grid.get(2).add(0);
		grid.get(2).add(1);
		grid.get(2).add(1);
		grid.get(3).add(0);
		grid.get(3).add(0);
		grid.get(3).add(0);
		grid.get(3).add(0);

		ClusterGo go = new ClusterGo();
		int counter = go.numberAmazonGoStores(4, 4, grid);

		System.out.println(counter);

	}

	int numberAmazonGoStores(int rows, int column, List<List<Integer>> grid) {
		boolean visited[][] = new boolean[rows][column];
		Integer[][] array = grid.stream().map(l -> l.stream().toArray(Integer[]::new)).toArray(Integer[][]::new);
		this.rows = rows;
		this.columns = column;
		int count = 0;
		for (int i = 0; i < rows; ++i)
			for (int j = 0; j < column; ++j)
				if (array[i][j] == 1 && !visited[i][j]) {
					DFS(array, i, j, visited);
					++count;
				}

		return count;
	}

	void DFS(Integer M[][], int row, int col, boolean visited[][]) {
		int rowNbr[] = new int[] { -1, -1, -1, 0, 0, 1, 1, 1 };
		int colNbr[] = new int[] { -1, 0, 1, -1, 1, -1, 0, 1 };
		visited[row][col] = true;
		for (int k = 0; k < 8; ++k)
			if (isSafe(M, row + rowNbr[k], col + colNbr[k], visited, row, col ))
				DFS(M, row + rowNbr[k], col + colNbr[k], visited);
	}

	boolean isSafe(Integer M[][], int row, int col, boolean visited[][], int currentrow, int currentColumn) {
		return (row >= 0) && (row < rows) && (col >= 0) && (col < columns) && (M[row][col] == 1 && !visited[row][col]) 
				&& ( (row != currentrow -1 || row != currentrow + 1) && (col != currentColumn -1 || col != currentColumn + 1));
	}

}
