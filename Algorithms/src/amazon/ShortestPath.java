package amazon;

import java.util.Arrays;
import java.util.LinkedList;
import java.util.Queue;
import java.util.Stack;
import java.util.function.Predicate;
import java.util.stream.IntStream;

public class ShortestPath {

	private static class Position {
		public int x;
		public int y;
		public Position predecessor;

		public Position(int x, int y) {
			this.x = x;
			this.y = y;
		}

		public Position(int x, int y, Position predecessor) {
			this(x, y);
			this.predecessor = predecessor;
		}

		@Override
		public int hashCode() {
			final int prime = 31;
			int result = 1;
			result = prime * result + x;
			result = prime * result + y;
			return result;
		}

		@Override
		public boolean equals(Object obj) {
			if (this == obj)
				return true;
			if (obj == null)
				return false;
			if (getClass() != obj.getClass())
				return false;
			Position other = (Position) obj;
			if (x != other.x)
				return false;
			if (y != other.y)
				return false;
			return true;
		}

		@Override
		public String toString() {
			return "[" + x + "," + y + "]";
		}
	}

	private int[][] matrix;

	private Position[] shortestPath;

	private Stack<Position> path;

	private Position start;

	private int end = -1;

	public ShortestPath(int[][] matrix) {
		this.matrix = matrix;
	}

	public ShortestPath(int[][] matrix, Position start) {
		this.matrix = matrix;
		this.start = start;
	}

	public ShortestPath(int[][] matrix, Position start, int end) {
		this.matrix = matrix;
		this.start = start;
		this.end = end;
	}

	public Position[] getPathDFS() {
		path = new Stack<Position>();
		shortestPath = null;

		if (start != null) {
			next(start);
		}

		return shortestPath;
	}

	public Position[] getPathBFS() {
		path = new Stack<Position>();
		shortestPath = null;

		LinkedList<Position> predecessors = new LinkedList<Position>();
		Queue<Position> queue = new LinkedList<Position>();
		queue.offer(start);
		visit(start);

		if (start == null) {
			return null;
		}

		while (!queue.isEmpty()) {
			Position position = queue.poll();
			predecessors.add(position);

			if (!endFound(position)) {
				Position nextPosition = new Position(position.x + 1, position.y, position);
				if (isVisitable(nextPosition)) {
					queue.offer(nextPosition);
					visit(nextPosition);
				}

				nextPosition = new Position(position.x, position.y + 1, position);
				if (isVisitable(nextPosition)) {
					queue.offer(nextPosition);
					visit(nextPosition);
				}

				nextPosition = new Position(position.x - 1, position.y, position);
				if (isVisitable(nextPosition)) {
					queue.offer(nextPosition);
					visit(nextPosition);
				}

				nextPosition = new Position(position.x, position.y - 1, position);
				if (isVisitable(nextPosition)) {
					queue.offer(nextPosition);
					visit(nextPosition);
				}
			} else {
				break;
			}
		}

		Position position = predecessors.getLast();

		if (position != null) {
			do {
				path.push(position);
				position = position.predecessor;
			} while (position != null);

			shortestPath = new Position[path.size()];
			int i = 0;
			while (!path.isEmpty()) {
				shortestPath[i++] = path.pop();
			}
		}

		cleanUp();
		return shortestPath;
	}

	private void next(Position position) {
		stepForward(position);

		if (shortestPath == null || path.size() < shortestPath.length) {
			if (!endFound(position)) {
				Position nextPosition = new Position(position.x + 1, position.y);
				if (isVisitable(nextPosition)) {
					next(nextPosition);
				}

				nextPosition = new Position(position.x, position.y + 1);
				if (isVisitable(nextPosition)) {
					next(nextPosition);
				}

				nextPosition = new Position(position.x - 1, position.y);
				if (isVisitable(nextPosition)) {
					next(nextPosition);
				}

				nextPosition = new Position(position.x, position.y - 1);
				if (isVisitable(nextPosition)) {
					next(nextPosition);
				}
			} else {
				shortestPath = path.toArray(new Position[0]);
			}
		}

		stepBack();
	}

	private boolean isVisitable(Position position) {
		return position.y >= 0 && position.x >= 0 && position.y < matrix.length
				&& position.x < matrix[position.y].length
				&& (matrix[position.y][position.x] == 1 || endFound(position));
	}

	private boolean endFound(Position position) {
		return matrix[position.y][position.x] == end;
	}

	private void stepForward(Position position) {
		path.push(position);
		if (matrix[position.y][position.x] == 1) {
			matrix[position.y][position.x] = -1;
		}
	}

	private void stepBack() {
		Position position = path.pop();
		if (matrix[position.y][position.x] == -1) {
			matrix[position.y][position.x] = 1;
		}
	}

	private void visit(Position position) {
		if (matrix[position.y][position.x] == 1) {
			matrix[position.y][position.x] = -1;
		}
	}

	// private void findStart() {
	// start = new Position(0, 0);
	// if (start != null) {
	// return;
	// }
	//
	// for (int i = 0; i < matrix.length; i++) {
	// for (int j = 0; j < matrix[i].length; j++) {
	// if (matrix[i][j] == 'S') {
	// start = new Position(j, i);
	// }
	// }
	// }
	// }

	private void cleanUp() {
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				if (matrix[i][j] == -1) {
					matrix[i][j] = 1;
				}
			}
		}
	}

	public static void main(String[] args) {

		int[][] field = { { 1, 1, 0, 2 }, { 3, 1, 1, 1 } };

		IntStream stream = Arrays.stream(field).flatMapToInt(Arrays::stream).filter(t -> t > 1);
		
		int max = stream.sorted().min().getAsInt();
		int min = stream.min().getAsInt();
		System.out.println(max);
		System.out.println(min);

//		int maximum = Integer.MIN_VALUE;
//		int minimum = Integer.MAX_VALUE;
//
//		for (int i = 0; i < field.length; i++) {
//			Arrays.sort(field[i]);
//
//			if (field[i][0] < minimum && field[i][0] > 1)
//				minimum = field[i][0];
//			if (field[i][field[i].length - 1] > maximum && field[i][0] > 1)
//				maximum = field[i][field[i].length - 1];
//		}
//		
//		System.out.println(maximum);
//		System.out.println(minimum);

		ShortestPath sp = new ShortestPath(field, new Position(0, 0), 2);
		Position[] path = sp.getPathDFS();
		System.out.println(path.length);
		if (path != null) {
			System.out.println("DFS: " + Arrays.toString(path));
		} else {
			System.out.println("No path found!");
		}

		sp = new ShortestPath(field, new Position(3, 0), 3);
		path = sp.getPathDFS();
		System.out.println(path.length);
		if (path != null) {
			System.out.println("DFS: " + Arrays.toString(path));
		} else {
			System.out.println("No path found!");
		}

		// path = sp.getPathBFS();
		// if (path != null) {
		// System.out.println("BFS: " + Arrays.toString(path));
		// } else {
		// System.out.println("No path found!");
		// }
	}

}
