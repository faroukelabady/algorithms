package amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;

public class Router {

	static int time = 0;
	
	public static void main(String[] args) {
		ArrayList<ArrayList<Integer>> grid = new ArrayList<>();
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());
		grid.add(new ArrayList<>());

		grid.get(0).add(1);
		grid.get(0).add(2);
		grid.get(1).add(2);
		grid.get(1).add(3);
		grid.get(2).add(3);
		grid.get(2).add(4);
		grid.get(3).add(4);
		grid.get(3).add(5);
		grid.get(4).add(6);
		grid.get(4).add(3);
		

		Router go = new Router();
		List<Integer> counter = go.criticalRouters(6,5, grid);
		
		System.out.println(counter.size());
	}

	List<Integer> criticalRouters(int numRouters, int numLinks, ArrayList<ArrayList<Integer>> links) {
		
		time = 0;
		Map<Integer, Set<Integer>> map = new HashMap<>();
		for(int i=0;i<numRouters;i++) {
			map.put(i, new HashSet<>());
		}
		for(ArrayList<Integer> link : links) {
			map.get(link.get(0)).add(link.get(1));
			map.get(link.get(1)).add(link.get(0));
		}
		Set<Integer> set = new HashSet<>();
		int[] low = new int[numRouters];
		int[] ids = new int[numRouters];
		int parent[] = new int[numRouters]; 
		Arrays.fill(ids, -1);
		Arrays.fill(parent, -1);
		for(int i=0;i<numRouters;i++) {
			if(ids[i] == -1)
				dfs(map, low, ids, parent, i, set);
		}
		return new ArrayList<>(set);
	}
	
	private static void dfs(Map<Integer, Set<Integer>> map, int[] low, int[] ids, int[] parent, int cur, Set<Integer> res) {
		int children = 0; 
		ids[cur] = low[cur]= ++time;
		for(int nei : map.get(cur)) {
			if(ids[nei] == -1) {
				children++;
				parent[nei] = cur;
				dfs(map, low, ids, parent,nei, res);
				low[cur] = Math.min(low[cur], low[nei]);
				if((parent[cur] == -1 && children > 1) || (parent[cur] != -1 && low[nei] >= ids[cur]))
					res.add(cur);
			}
			else if(nei != parent[cur])
				low[cur] = Math.min(low[cur], ids[nei]);
		}
	}

}
