package amazon;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.NoSuchElementException;

class Node {
	int x;
	int y;

	Node(int x, int y) {
		this.x = x;
		this.y = y;
	}
}

public class Graph {

	public static boolean pathExists(int[][] matrix) {
		int N = matrix.length;
		List<Node> queue = new ArrayList<Node>();
		queue.add(new Node(0, 0));
		boolean pathExists = false;

		while (!queue.isEmpty()) {
			Node current = queue.remove(0);
			if (matrix[current.x][current.y] == 2) {
				pathExists = true;
				break;
			}

			matrix[current.x][current.y] = 0; // mark as visited

			List<Node> neighbors = getNeighbors(matrix, current);
			queue.addAll(neighbors);
		}

		return pathExists;
	}

	public static List<Node> getNeighbors(int[][] matrix, Node node) {
		List<Node> neighbors = new ArrayList<Node>();

		if (isValidPoint(matrix, node.x - 1, node.y)) {
			neighbors.add(new Node(node.x - 1, node.y));
		}

		if (isValidPoint(matrix, node.x + 1, node.y)) {
			neighbors.add(new Node(node.x + 1, node.y));
		}

		if (isValidPoint(matrix, node.x, node.y - 1)) {
			neighbors.add(new Node(node.x, node.y - 1));
		}

		if (isValidPoint(matrix, node.x, node.y + 1)) {
			neighbors.add(new Node(node.x, node.y + 1));
		}

		return neighbors;
	}

	public static boolean isValidPoint(int[][] matrix, int x, int y) {
		return !(x < 0 || x >= matrix.length || y < 0 || y >= matrix.length) && (matrix[x][y] == 1);
	}

	// test client
	public static void main(String[] args) {
		int row = 2;
		int col = 4;
		int[][] field = { { 1, 1, 0, 2 }, { 3, 1, 1, 1 } };

		boolean pathExists = pathExists(field);
        System.out.println(pathExists ? "YES" : "NO");

	}

}
