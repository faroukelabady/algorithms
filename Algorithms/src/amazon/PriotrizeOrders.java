package amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.function.Function;
import java.util.stream.Collector;
import java.util.stream.Collectors;

public class PriotrizeOrders {

	public static List<String> prioritizedOrders(int numOrders, List<String> orderList) {
		
		List<String> nonPrime = new ArrayList<>();
		SortedMap<String, String> map = new TreeMap<>();
		for(String order: orderList) {
			String[] keyValue = order.split(" ", 2);
			if(!keyValue[1].matches("[a-z\\s]+$")) {
				nonPrime.add(order);
			} else {
				map.put(keyValue[0], keyValue[1]);
			}
			
		}
		
		List<String> result = map.entrySet().stream().sorted(Map.Entry.<String, String>comparingByValue()
				.thenComparing(Map.Entry.comparingByKey())).map(e -> e.getKey() + " " + e.getValue()).collect(Collectors.toList());
		result.addAll(nonPrime);
		
		return result;
		// WRITE YOUR CODE HERE
	}
	
	 List<Integer> lengthSubsequenceShoppers(List<Character> inputList) {
		 int[][] letters = new int[27][2];
		 int val = 96; 
		 letters[inputList.get(0).charValue() - val][0] = 0;
		 for(int i =1; i < inputList.size(); i++) {
			 
			int index =  (int)inputList.get(i).charValue() - val;
			if(letters[index][0] == 0) {
				letters[index][0] = index;
			} else {
				letters[index][1] = index;
			}
		 }
		 int[] result = new int[27];
		 for(int i = 0 ; i < letters.length; i++) {
			 for(int j = i + 1; j < letters.length; j++) {
				 int lastIndex = letters[i][1];
				 if(letters[j][0] > lastIndex) {
					 
				 }
			 }
		 }
		 
		return  Arrays.stream(result).boxed().collect(Collectors.toList());
	        // WRITE YOUR CODE HERE
	 }
	
	
	public static void main(String[] args) {
		List<String> orderList = new ArrayList<>();
		orderList.add("mi2 jog mid pet");
		orderList.add("wz3 34 54 398");
		orderList.add("a1 alps cow bar");
		orderList.add("x4 45 21 7");
		 prioritizedOrders(4, orderList);
	}
}
