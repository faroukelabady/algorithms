package amazon;

import java.util.ArrayList;
import java.util.List;

public class FruitWinnerChooser {

	public static void main(String[] args) {
		List<String> group1 = new ArrayList<>();
		group1.add("apple");
		group1.add("apple");
		
		List<String> group2 = new ArrayList<>();
		group2.add("banana");
		group2.add("anything");
		group2.add("banana");
		
		List<List<String>> order = new ArrayList<>();
		order.add(group1);
		order.add(group2);
		
		List<String> customerOrder = new ArrayList<>();
//		customerOrder.add("orange");
//		customerOrder.add("apple");
//		customerOrder.add("apple");
//		customerOrder.add("orange");
		customerOrder.add("apple");
		customerOrder.add("apple");
		customerOrder.add("orange");
//		customerOrder.add("banana");
		customerOrder.add("anything");
		customerOrder.add("banana");
//		customerOrder.add("apple");
//		customerOrder.add("apple");
//		customerOrder.add("banana");
//		customerOrder.add("anything");
//		customerOrder.add("banana");
//		
		System.out.println(checkWinner(order, customerOrder));

	}

	public static int checkWinner(List<List<String>> codeList, List<String> shoppingCart) {
		// WRITE YOUR CODE HERE
		int count = 0;
		int codeListSize = codeList.size();
		int[] indexChecker = new int[codeListSize];
		int currentList = 0;
		int i = 0;
		for(;i < shoppingCart.size();) {
			
			int j = 0;
			if(currentList >= codeListSize) {
				break;
			}
			List<String> currentGroup = codeList.get(currentList);
			for(; j < currentGroup.size(); ) {
				if(i >= shoppingCart.size()) {
					break;
				}
				if(currentGroup.get(j).equals(shoppingCart.get(i)) || currentGroup.get(j).equals("anything") ) {
					count++;
					i++;
					j++;
				} else {
					i++;
					break;
				}
			}
			
			if(count == currentGroup.size()) {
				indexChecker[currentList] = i - j;
				count = 0;
				if(currentList < codeListSize - 1) {
					currentList++;
				} else {
					break;
				}
				
			}
		}
		
		int result = 1;
		for(int k = 1; k < indexChecker.length; k++) {
			int lastIndex =indexChecker[k -1] + codeList.get(k-1).size() - 1;
			if(lastIndex < indexChecker[k]) {
				result = 1;
			} else {
				result = 0;
				break;
			}
		}
		
		return result;
	}

}
