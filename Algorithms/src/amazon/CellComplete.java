package amazon;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.stream.Collectors;

public class CellComplete {

	public static List<Integer> cellCompete(int[] states, int days) {
		
		while(days > 0) {
			int[] current = new int[states.length];
			for(int i = 0; i < states.length; i++) {
				if(i == 0 && states[i+1] == 0) {
					current[i] = 0;
				} else if(i == states.length - 1 && states[i-1] == 0) {
					current[i] = 0;
				} else if( i > 0 && i < states.length - 1 && states[i+1] == states[i-1]) {
					current[i] = 0;
				} else {
					current[i] = 1;
				} 
			}
			states = current;
			days--;
		}
		
		return Arrays.stream(states).boxed().collect(Collectors.toList());
		// WRITE YOUR CODE HERE
	}
	
	public int generalizedGCD(int num, int[] arr)
    {
		int result = arr[0]; 
        for (int i = 1; i < num; i++) 
            result = gcd(arr[i], result); 
  
        return result; 
        // WRITE YOUR CODE HERE
    }
	
    public int gcd(int a, int b) 
    { 
        if (a == 0) 
            return b; 
        return gcd(b % a, a); 
    } 
	
	public static void main(String[] args) {
		List<Integer> s =  cellCompete(new int[] {1,1,1,0,1,1,1,1}, 2);
		System.out.println(s);
	}

}
