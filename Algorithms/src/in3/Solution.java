package in3;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;
import java.util.Scanner;
import java.util.stream.Stream;

public class Solution {

	private static final Scanner scan = new Scanner(System.in);

	public static void main(String[] args) throws IOException {

		// String str = "unicomp6.unicomp.net - - [01/Jul/1995:00:00:06 -0400] \"GET
		// /shuttle/countdown/ HTTP/1.0\" 200 3985";
		System.out.println("this is a test");

		String filename;
		filename = "input000.txt"; //scan.nextLine();
		Map<String, Integer> counterMap = new LinkedHashMap<>();
		try (Stream<String> stream = Files.lines(Paths.get("/home/farouk/git/algorithms/Algorithms/src/in3/" + filename))) {
			stream.forEach((line) -> {
				int startIndx = line.indexOf("[");
				int endIndx = line.indexOf(" -", startIndx);
				String value = line.substring(startIndx + 1, endIndx);
				Integer counter = counterMap.get(value);
				if(counter == null) {
					counterMap.put(value, 1);
				} else {
					counterMap.put(value,  counterMap.get(value) + 1);
				}
				
			});
		
//			PrintWriter writer = new PrintWriter("req_" + filename, "UTF-8");
//			
//			writer.close();
		} 
		
		File fileName = new File( "req_" + filename );
        if( !fileName.exists() )
        {
            System.out.println( "this file doesn't exist " );
            try
            {
                fileName.createNewFile();
                FileWriter fileWrite = new FileWriter( fileName );
                BufferedWriter bufferedWriter = new BufferedWriter( fileWrite );
                //bufferedWriter.write( "write something here " );
                counterMap.forEach((k, v) -> {
    				if (v != null && v > 1) {
    					try {
							bufferedWriter.write(k);
							bufferedWriter.newLine();
						} catch (IOException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
    				}
    			});
                bufferedWriter.close();
            } catch ( IOException e )
            {
                //catch exception
            }
        }

	}

}
