package in3;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

public class Solution2 {

	public static void main(String[] args) {
		List<String> sent = new ArrayList<>();
		sent.add("it go will away");
		sent.add("go do art");
		sent.add("what to will east");
		
		List<String> q = new ArrayList<>();
		q.add("it will");
		q.add("go east will");
		q.add("will");
		
		textQueries(sent, q);

	}

	public static void textQueries(List<String> sentences, List<String> queries) {
		// Write your code here
		Map<Integer, List<Integer>> result = new LinkedHashMap<>();
		for (int j = 0; j < queries.size(); j++) {
			result.put(j, new ArrayList<>());
			for (int i = 0; i < sentences.size(); i++) {
				String sentence = sentences.get(i);
				int total = 0;
				String[] queryArr = queries.get(j).split(" ");
				for(int k =0 ; k < queryArr.length; k++) {
					String current = queryArr[k];
					int queryCount = sentence.split("\\b" + current + "\\b", -1).length - 1;
					if (queryCount == 0 && k == 0) {
						total = 0;
						break;
					} else if (queryCount == 0 && total != 0) {
						total = 0;
						break;
					} else if ((queryCount + total) % 2 != 0 && k != 0) {
						total = 0;
						break;
					} else {
						total = queryCount;
					}
				}
				if(total != 0) {
					if(result.get(j).contains(-1)) {
						result.put(j, new ArrayList<>());
					}
					for(int q = 1 ; q <= total; q++) {
						result.get(j).add(i);
					}
				} else if( total == 0 && result.get(j).isEmpty() ){
					result.get(j).add(-1);
				}
				
			}
			
		}
		
		result.forEach((k,v) -> {
			v.forEach(r -> System.out.print(r + " "));
			System.out.println();
		});

	}

}
